import React from "react";
import { Button } from "reactstrap";
import Modal from "components/Modal";
import Input from "components/Input";
import useIsMounted from "react-is-mounted-hook";
import Axios from "axios";
import { MAP_BOX_TOKEN } from "constants/config";
import useToast from "components/Toast/Toast";
import { FiArrowUpLeft, FiMapPin } from "react-icons/fi";
import Spinner from "components/Spinner";
import { formatPlaceName } from "utils/typeTools";
import Typography from "components/Typography";

const LocationInput = ({
  name,
  className,
  disabled,
  error,
  label,
  placeholder,
  value,
  onChange,
}) => {
  const isMounted = useIsMounted();
  const toast = useToast();

  const [IsSearching, setIsSearching] = React.useState(false);
  const [Open, setOpen] = React.useState(false);
  const [TimeoutId, setTimeoutId] = React.useState();

  const [SearchResults, setSearchResults] = React.useState([]);
  const [SearchQuery, setSearchQuery] = React.useState("");

  const [Selected, setSelected] = React.useState(value);

  const getLocations = () => {
    if (IsSearching || !SearchQuery) return undefined;

    setIsSearching(true);

    let url = `https://api.mapbox.com/geocoding/v5/mapbox.places/${SearchQuery}.json?access_token=${MAP_BOX_TOKEN}&limit=10&country=AU&types=locality%2Cplace&autocomplete=true`;

    Axios.get(url)
      .then((payload) => {
        if (isMounted && payload.data.features)
          setSearchResults(
            payload.data.features.filter(
              (item, index, self) =>
                self.findIndex((i) => i.text === item.text) === index
            )
          );
      })
      .catch((reason) => {
        if (isMounted && reason.error) {
          toast.showMessage("danger", "Error on fetch data! Report us");
        }
      })
      .finally(() => {
        if (isMounted) setIsSearching(false);
      });
  };

  React.useEffect(() => {
    if (TimeoutId) clearTimeout(TimeoutId);
    setTimeoutId(setTimeout(getLocations, 500));
    // eslint-disable-next-line
  }, [SearchQuery]);

  const handleFocus = (e) => {
    if (SearchQuery) setOpen(true);
  };
  const handleChange = (e) => {
    const { value } = e.target;
    setSearchQuery(value);
    setOpen(value ? true : false);
  };

  const handleBlur = (e) => {
    setTimeout(() => setOpen(false), 300);

    if (value) setSearchQuery(value);
    else setSearchQuery("");
  };

  const handleSelect = (location) => {
    setSelected(location);
    setOpen(false);
  };
  React.useEffect(() => {
    if (onChange && Selected) onChange(Selected);
    // eslint-disable-next-line
  }, [Selected]);

  React.useEffect(() => {
    if (value) setSearchQuery(value);
    // eslint-disable-next-line
  }, [value]);

  return (
    <div className={`location-input${className ? " " + className : ""}`}>
      <Input
        autoComplete="nope"
        name={name}
        afterIcon={<FiMapPin />}
        disabled={disabled}
        error={error}
        label={label}
        placeholder={placeholder}
        onFocus={handleFocus}
        onChange={handleChange}
        onBlur={handleBlur}
        value={SearchQuery || ""}
      />
      {Open && (
        <div className="dropdown">
          <ul>
            {IsSearching && <Spinner type="box" />}
            {SearchResults.map((location, index) => (
              <li
                key={location.text + index}
                className={location.text === Selected?.text ? "selected" : ""}
                onClick={() => handleSelect(location)}
              >
                <Typography variant="sub-heading3">
                  {formatPlaceName(location?.place_name)}
                </Typography>
                <FiArrowUpLeft />
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
};
export default LocationInput;
