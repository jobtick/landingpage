import Typography from 'components/Typography'
import React from 'react'
import Lottie from 'react-lottie';
import { Container } from 'reactstrap';
import classes from './style.module.scss'
export default function UnderMaintenance() {
    return (
        <div className={classes['under-maintenance']}>
            <Container>
                <Lottie
                    className="animation"
                    width="689px"
                    height="689px"
                    options={
                        {
                            loop: true,
                            autoplay: true,
                            animationData: require('assets/vector/animations/maintenance2.json')
                        }
                    }
                    isClickToPauseDisabled={true}
                />
                <Typography component='h2' variant='heading0'>Error 404</Typography>
                <Typography component='p' variant='heading1'>Oops.. The page you’re looking for doesn’t seem to exist</Typography>
            </Container>
        </div>
    );
}
export default UnderMaintenance;
