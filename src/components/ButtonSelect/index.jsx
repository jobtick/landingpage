import React from 'react'
import { Button, Col } from 'reactstrap'

export default function ButtonSelect({ value, items, onChange ,block ,size }) {

    return (
        <div className={`button-select${block?' block':''}`}>
            {items.map(item => (
                <Col key={item.value}>
                    <Button
                        color={value === item.value ? "primary" : "light"}
                        onClick={() => onChange(item.value)}
                        size={size}
                        block
                    >
                        {item.label}
                    </Button>
                </Col>
            ))}
        </div>
    )
}
