import React from "react";
import Typography from "components/Typography";
import dateFormat from "dateformat";
import { range } from "utils/typeTools";
import { FiChevronLeft, FiChevronRight } from "react-icons/fi";
import classes from "./style.module.scss";
/*
    min: 2021-05-03
    max : 2021-06-07
*/
export default function Calendar({ value, onChange, min, max }) {
  const now = new Date();
  const [Year, setYear] = React.useState(now.getFullYear());
  const [Month, setMonth] = React.useState(now.getMonth() + 1);
  const [Selected, setSelected] = React.useState(
    value ? new Date(value) : new Date()
  );

  const lastMonthDays = new Date(
    Month === 1 ? Year - 1 : Year,
    Month === 1 ? 12 : Month - 1,
    0
  ).getDate();
  const monthDays = new Date(Year, Month, 0).getDate();
  const firstWeekDay = new Date(Year, Month - 1, 1).getDay();
  const lastWeekDay = new Date(Year, Month - 1, monthDays).getDay();

  const lastMonthDaysArr = range(
    firstWeekDay,
    lastMonthDays - firstWeekDay + 1
  );
  const monthDaysArr = range(monthDays, 1);
  const nextMonthDaysArr = range(6 - lastWeekDay, 1);

  const handlePrev = () => {
    setMonth((month) => {
      if (month === 1) {
        setYear((year) => year - 1);
        return 12;
      } else return month - 1;
    });
  };

  const handleNext = () => {
    setMonth((month) => {
      if (month === 12) {
        setYear((year) => year + 1);
        return 1;
      } else return month + 1;
    });
  };
  const handleSelect = (day) => {
    setSelected(new Date(Year, Month - 1, day));
  };

  React.useEffect(() => {
    if (Selected && onChange) onChange(Selected);
    // eslint-disable-next-line
  }, [Selected]);
  return (
    <div className={classes.calendar}>
      <div className={classes["calendar-header"]}>
        <FiChevronLeft onClick={handlePrev} />
        <Typography variant="heading4">
          {dateFormat(new Date(Year, Month - 1), "mmmm yyyy")}
        </Typography>
        <FiChevronRight onClick={handleNext} />
      </div>
      <div className={classes.weekdays}>
        {["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"].map(
          (item, index) => (
            <Typography variant="sub-heading2" key={index}>
              {item}
            </Typography>
          )
        )}
      </div>
      <div className={classes.days}>
        {[...lastMonthDaysArr, ...monthDaysArr, ...nextMonthDaysArr].map(
          (item, index) => {
            if (
              index < lastMonthDaysArr.length ||
              index >= lastMonthDaysArr.length + monthDaysArr.length ||
              (min && new Date(Year, Month - 1, item + 1) < new Date(min)) ||
              (max && new Date(Year, Month - 1, item) > new Date(max))
            )
              return (
                <Typography
                  variant="sub-heading2"
                  className={`${classes.day} ${classes.disabled}`}
                  key={index}
                >
                  {item}
                </Typography>
              );
            return (
              <div
                className={[
                  classes.day,
                  dateFormat(now, "yyyy-m-d") ===
                  `${Year}-${Month}-${item <= 9 ? `0${item}` : item}`
                    ? classes.today
                    : "",
                  dateFormat(Selected, "yyyy-m-d") ===
                  `${Year}-${Month}-${item}`
                    ? classes.selected
                    : "",
                ]
                  .join(" ")
                  .trim()}
                key={index}
                onClick={() => handleSelect(item)}
              >
                <Typography variant="sub-heading2">{item}</Typography>
              </div>
            );
          }
        )}
      </div>
    </div>
  );
}
