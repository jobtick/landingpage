import React from 'react'

export default function Insurance() {

    return (
        <div id="insurance">
            <h4>Insurance</h4>
            <ul>
                <li>Jobtick may offer a third-party insurance policy to use from time to time; it is the User's
                    responsibility to ensure that the third-party insurance policy is adequate for its intended use. The
                    insurance policies are available on the Jobtick website, or you can request to receive a copy by
                    email.
                </li>
            </ul>
        </div>
    )
}