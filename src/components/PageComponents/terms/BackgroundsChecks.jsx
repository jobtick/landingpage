import React from 'react'

export default function BackgroundsChecks() {

    return (
        <div id="backgrounds-checks">
            <h4>Backgrounds checks</h4>
            <ul>
                <li>The Jobtick Pty Ltd requires all service provider to provide a Government-issued photo ID, a profile picture and that by installing the Jobtick App or the use of the Jobtick website, giving consent to</li>
                <li>checks the backgrounds of the service provider using third-party services, including but not limited to local and Federal Government sex offender's registry record and theFair-trading Act record</li>
                <li>obtick is not liable for the third-party background check information or any information provided through Government services.</li>
                <li>Jobtick may modify, suspend, or terminate the service provider user account following the third-party verification services' information.</li>
            </ul>
        </div>
    )
}