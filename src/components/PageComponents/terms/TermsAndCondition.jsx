import React from 'react'

export default function TermsAndCondition() {

    return (
        <div id="terms">
            <h4>Terms and Condition</h4>
            <p>This Services Agreement constitutes a legal agreement between Poster/Ticker, and the Jobtick Pty Ltd, an Australian Company registered in Australia with ABN no: 68 639 155 767</p>
            <p>you accept that you have read, understood, and agree to be bound by the Jobtick Terms of Use; if you do not agree to be bound by these terms of service agreement, you must not use the Jobtick Platform or the services as defined on this service agreement.</p>
        </div>
    )
}