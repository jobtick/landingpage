import React from 'react'

export default function JobtickPtyLtdObligation() {

    return (
        <div className="contact">
            <h4>Jobtick Pty Ltd Obligation</h4>
            <ul>
                <li>Jobtick is a mobile app platform that provides a link and access to Jobtick affiliates and third-party service providers," The Ticker."</li>
                <li>Jobtick mobile application and Jobtick website is owned and operated by Jobtick Pty Ltd.</li>
                <li>Jobtick shall invite the individual over the legal age of 18 to become a Poster or a Ticker.</li>
                <li>Jobtick Pty Ltd reserves the right to amend the terms and conditions at any time, and users agree to be bound by the amendments.</li>
                <li>Jobtick reserve the right to suspend, modify an existing user account, or refuse a new account registration for any person or business</li>
                <li>Jobtick shall not be responsible for the accuracy of information provided by the Poster or the service provider.</li>
                <li>Jobtick shall not be responsible for the qualification, skills, or performance of the service provider.</li>
                <li>You agree that Jobtick can collect your personal information when you use Jobtick App or Website; we may use your information to verify your ID documents.</li>
                <li>The information and content stored on Jobtick App or Website may not be used by thethird-party without the written permission from Jobtick.</li>
                <li>You may not use the Jobtick platform for any immoral or illegal activities.</li>
                <li>You acknowledge that any information or documents provided to Jobtick platform is original, accurate, and up to date; you also agree that Jobtick may verify all documents with the issuing authority,</li>
                <li>Jobtick reserve the right to remove any documents that could not be verified with the issuing authority</li>
                <li>The Ticker and the Poster agree that you are responsible for your messages' content when using the Jobtick messing system and that you must not post messages that are unlawful, defamatory, or threatening.</li>
            </ul>
        </div>
    )
}