import React from 'react'

export default function Definitions() {

    return (
        <div id="definitions">
            <h4>Definitions</h4>
            <ul>
                <li>Jobtick "means" Jobtick Pty Ltd "the Company."</li>
                <li>Website "means" Jobetick Website that is owned and operated by Jobtick Pty Ltd.</li>
                <li>Mobile App "means" Jobtick mobile application that is owned and operated by Jobtick Pty Ltd.</li>
                <li>Fee "means" all payments payable to Jobtick Pty Ltd</li>
                <li>A Poster "means" the user/person who is posting a job on the Jobtick platform/Website.</li>
                <li>A Ticker "means" the service provider who offers to do the job for a set fee.</li>
                <li>Job "means" a job that the Poster is posting on the Jobtick platform/website.</li>
            </ul>
        </div>
    )
}