import React from 'react'

export default function TheTickerObligation() {

    return (
        <div id="ticker-obligation">
            <h4>The Ticker Obligation</h4>
            <ul>
                <li>The Ticker shall be responsible for the job performed under the agreed job description. The Ticker must not give any part of the job to a subcontractor or the third party who do not have a user account on the Jobtick platform</li>
                <li>The Ticker accepts and agrees to provide all the essential equipment, tools, and materials at his own expense to perform each job.</li>
                <li>You agree that Jobtick can collect your personal and business information when you use Jobtick App or Website; this information is to verify your user account or ID documents.</li>
                <li>The Ticker shall arrive at the job location on time and agree to perform the job professionally.</li>
                <li>If the Ticker performance is not satisfactory, the Ticker shall rectify the issue as soon as possible.</li>
                <li>The Ticker may perform additional service and request an adjustment of fees. The Ticker acknowledges and agrees that they shall not receive any cash payment and thatall additional services and payments shall be through the Jobtick platform.</li>
                <li>The Ticker shall confirm the completion of the job on the Jobtick Platform.</li>
                <li>The Ticker shall not use the Poster information for any reason other than fulfilling the agreed services.</li>
                <li>Upon your satisfactory performance of the job, the job fee is payable within 24 hours of completing the job</li>
                <li>Jobtick reserve the right to suspend or remove the Ticker account should the Ticker fail to meet his contractual obligation or fails to uphold the terms of his agreement or perform an unacceptable and poor-quality job.</li>
            </ul>
        </div>
    )
}