import React from 'react'

export default function ThePosterObligation() {

    return (
        <div id="poster-obligation">
            <h4>The Poster Obligation</h4>
            <ul>
                <li>Accessing the use of the Jobtick Mobile App or Jobtick website is confirmation of accepting the terms and conditions as set in this agreement.</li>
                <li>Accessing the use of a jobtick mobile App or Jobtick website is confirmation that you are 18 years of age.</li>
                <li>When you accept an offer from the service provider, you are entering into a contract with the service provider; the contract can be written or by way of communicating an intention to accept the offer.</li>
                <li>You must not use The Jobtick platform for any immoral or illegal activities.</li>
                <li>You agree that Jobtick can collect your personal information when you use Jobtick App or Website; we may use it to verify your user account.</li>
                <li>You agree that Jobtick may use your post's content on the Jobtick platform for promotional purposes on the internet or social media worldwide.</li>
                <li>You agree that you are liable for any harmful, fraudulent, misleading, or false information or unlawful images posted to the Jobtick platform using your account.</li>
                <li>Accepting an offer by Poster is a confirmation of an agreement between the Poster and the Ticker; accordingly, the Poster and the Ticker agree to be bound by Jobtick terms and conditions also by Jobtick Privacy Policy.</li>
                <li>Upon accepting the offer, the agreed service fee is due and payable.</li>
                <li>The Poster must provide a safe working environment for the Ticker to perform the job.</li>
                <li>The Poster shall confirm the completion of the job on the Jobtick Platform.</li>
                <li>A Poster reserve the right to nominate the Ticker to perform the job; Jobtick shall notify the Ticker accordingly</li>
                <li>Do not post your exact location, full names, Phone Numbers, or Email Addresses when posting a job.</li>
                <li>The posters may cancel or modify a posted job at any time before accepting an offer from the Ticker</li>
                <li>if the dispute arises from the scheduled job, payment for that particular job shall remain</li>
                <li>Upon accepting an offer from the service provided, the Poster and the Ticker may use Jobtick private messaging facilities to communicate and make changes to the job, including the agreed service fee,</li>
            </ul>
        </div>
    )
}