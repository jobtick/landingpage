import React from 'react'

export default function CancellationAndDisputes() {

    return (
        <div id="Cancellation">
            <h4>Cancellation and Disputes</h4>
            <ul>
                <li>Jobtick Pty Ltd creates a direct business relationship between the Poster and the Ticker, Jobtick Pty Ltd, and their affiliates company is not responsible or liable for disputes arising from matters relating to the direct or indirect business relationship between the Poster and the Ticker,</li>
                <li>In the event of a dispute between the Poster and the Ticker, both parties shall attempt to reach an agreement within 14 days by friendly and informal negotiation; should both parties fail to resolve the issue, the dispute shall be referred to Jobtick to resolve the disputes promptly. However, Jobtick has no obligation to assist in resolving any disputes between the Poster and the Ticker.</li>
                <li>There are circumstances where a poster may cancel the scheduled job offer, and these include when the service provider has misrepresented the service or has a significant problem that cannot provide the service within a reasonable time frame.</li>
                <li>When the Poster and the service provider reach an agreement to cancel the scheduled job, if the Poster initiates the cancelation, Jobtick shall refund the service fee paid by the Poster less the service fee payable to Jobtick Pty Ltd.</li>
                <li>If the Ticker stops performing after the job commenced. Payment shall remain in Jobtick account until such time that the Poster and the Ticker come to a consensus on their own or through other methods of resolving their issues.</li>
                <li>As a Poster using the Jobtick platform, it is your responsibility to evaluate the service provider qualifications, and as the service provider, it is your responsibility to evaluate the job posted by the Poster.</li>
            </ul>
        </div>
    )
}