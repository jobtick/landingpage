import React from 'react'

export default function Fee() {

    return (
        <div id="fee">
            <h4>Fee</h4>
            <ul>
                <li>In respect of services provided by the Jobtick platform, a percentage of the transactionvalue between the Poster and the Ticker is payable to Jobtick Pty Ltd; this service fee is non-refundable and automatically is deducted from the payment</li>
                <li>After the job is scheduled, the service provider may need to perform additional service; the Poster may agree and accept to pay for additional service while the service provider is performing the job.</li>
                <li>The Poster shall pay any agreed additional service fee through the Jobtick platform before the service provider performs the additional service.</li>
                <li>Any additional services performed must be confirmed and paid for through the Jobtick platform.</li>
                <li>Any additional services that are performed but not paid for through the Jobtick platform shall not be covered by the Jobtick Guarantee and will violate the terms of the user agreement, resulting in the deactivation of the service provider user account</li>
                <li>The Poster shall pay for the scheduled service by credit/debit bank card at the time of accepting an offer,</li>
                <li>As a service provider, you acknowledge and agree that Jobtick process the due payment to your registered business bank account within 24 hours of satisfactory performance of service,</li>
                <li>The Jobtick reserves the right to withhold all or portion of the service fee should the Poster complain about unsatisfactory performance, or there may be a reason to believethat you have violated the terms of this agreement</li>
            </ul>
        </div>
    )
}