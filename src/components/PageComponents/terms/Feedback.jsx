import React from 'react'

export default function Feedback() {

    return (
        <div id="feedback">
            <h4>Feedback</h4>
            <ul>
                <li>We invite the users to send their feedback on the Jobtick platform; Jobtick reserves the right to remove your feedback should it be considered unfair or unlawful to other Jobtick Users.</li>
            </ul>
        </div>
    )
}