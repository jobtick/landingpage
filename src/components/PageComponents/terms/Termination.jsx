import React from 'react'

export default function Termination() {

    return (
        <div id="termination">
            <h4>Termination</h4>
            <ul>
                <li>Jobtick or the Users may terminate this agreement at any time; shall give termination notice via email or Jobtick platform. The cancellation effective immediately.</li>
                <li>The termination notice shall not affect any job that has been scheduled with the Poster</li>
            </ul>
        </div>
    )
}