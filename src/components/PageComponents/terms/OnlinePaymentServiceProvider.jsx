import React from 'react'

export default function OnlinePaymentServiceProvider() {

    return (
        <div id="payment">
            <h4>Online Payment Service Provider</h4>
            <ul>
                <li>Jobtick uses a payment service provider, "PSP" to facilitate online payment by credit card, debit card, or any other payment method. The PSP terms and conditions atwww.stripe.comare incorporated into this agreement.</li>
            </ul>
        </div>
    )
}