import React from 'react'
import ProfileAvatar from 'components/ProfileAvatar'
import { FiStar, FiFlag, FiClock } from 'react-icons/fi'
import { TaskCommentView } from './TaskComment'
import { AttachmentPreview } from 'components/AttachmentsView'
import moment from 'moment'
import Typography from 'components/Typography'
import NoContent from 'components/NoContent'
import { Status } from 'constants/status'
import { round10 } from 'utils/typeTools'
import Link from 'next/link'

export default function TaskOfferContainer({ task, onAction }) {

    const renderOffers = (showOnlyAssigned) => {
        const offers = showOnlyAssigned ? task.offers.filter(item => item.accepted) : task.offers
        return (
            offers.length > 0 ?
                offers.map((offer, index) => {
                    return (
                        <TaskOffer key={index}
                            task={task}
                            offer={offer}
                            showOnlyAssigned={showOnlyAssigned}
                            onAction={onAction}
                        />
                    )
                })
                :
                <NoContent icon={<FiClock />} label="Waiting for offers" desc={<>Be the first to <b>make an offer</b> on this job!</>} />
        )
    }

    const renderView = () => {
        if (task.status === Status.open) {
            return renderOffers()
        } else {
            return renderOffers(true)
        }
    }
    return (

        <div className={`task-offers-container task-block`}>
            <Typography component="h4" variant="body1" className="block-title">
                {task.status === Status.open ?
                    `Offers ${task.offers.length > 0 ? `(${task.offers.length})` : ''}`
                    :
                    "Assigned to"
                }
            </Typography>
            <div className="task-offers-list">
                {renderView()}
            </div>
        </div>
    )
}

export const TaskOffer = ({ task, offer, onAction }) => {
    const { worker } = offer
    return (
        <div className="task-offer">
            <div className="user-profile-summary">
                <Link href={"/profile/" + worker.id}>
                    <a target="_blank">
                        <ProfileAvatar profile={worker} />
                    </a>
                </Link>
                <div className="user-details">
                    <Link href={"/profile/" + worker.id}>
                        <a className="user-name" target="_blank">
                            <Typography component="span" variant="body3">{`${worker.name}`}</Typography>
                        </a>
                    </Link>
                    <Typography component="span" variant="caption1" className="user-completion-rate"><span>{`${worker.work_task_statistics.completion_rate}%`}</span> job success</Typography>
                    <div className="user-rating app-flex-center">
                        <FiStar />
                        <Typography component="span" variant="caption1" className="rating">
                            {worker.worker_ratings !== null &&
                                <>
                                    {round10(worker.worker_ratings.avg_rating, -2)} ({worker.worker_ratings.received_reviews})
                            </>
                            }
                        </Typography>
                    </div>
                </div>

                {task.status !== Status.assigned &&
                    <div className="offer-actions">
                        <span className="report-icon absolute-label" data-label="Report" onAction={onAction}><FiFlag /></span>

                        {offer.accepted &&
                            <Typography component="span" variant="caption1" className="status-assigned">
                                Assigned
                            </Typography>
                        }
                    </div>
                }
            </div>


            <div className="task-offer-content">
                <Typography component="p" variant="body3">{offer.message}</Typography>
                {offer.attachments && offer.attachments.length > 0 &&
                    <AttachmentPreview
                        attachments={offer.attachments}
                        preview={true}
                        downloadable={true}
                        containerStyle={{ marginLeft: "auto" }}
                        itemStyle={{ padding: "0px" }}
                    />
                }
                <Typography component="span" variant="caption1" className="offer-time">{moment(offer.created_at).fromNow()}</Typography>
            </div>
            {offer.comments?.length > 0 &&
                <TaskCommentView task={task} comments={offer.comments} onAction={onAction} />
            }
        </div>
    )
}