import React from 'react'
import Modal from 'components/Modal'
import { FiLink, FiShare2 } from 'react-icons/fi'
import { EmailIcon, EmailShareButton, FacebookIcon, FacebookShareButton, TwitterIcon, TwitterShareButton, WhatsappIcon, WhatsappShareButton } from 'react-share'
import ClipboardButton from 'react-clipboard.js'
import { BASE_URL } from 'constants/config'
import useToast from 'components/Toast/Toast'

export default function ShareTask({ task }) {
    const [Open, setOpen] = React.useState(false)
    const toast = useToast()
    return (
        <>
            <span className="task-action-icon absolute-label share-button"
                data-label="Share"
                onClick={() => setOpen(true)}
            >
                <FiShare2 />
            </span>

            <Modal
                className="share-task-modal"
                open={Open}
                onClose={() => setOpen(false)}
                title="Send This Job"
            >
                <div>
                    <FacebookShareButton url={BASE_URL + 'explore-jobs/' + task.slug}>
                        <FacebookIcon />
                    </FacebookShareButton>
                    Facebook
                </div>
                <div>
                    <TwitterShareButton url={BASE_URL + 'explore-jobs/' + task.slug}>
                        <TwitterIcon />
                    </TwitterShareButton>
                    Twitter
                </div>
                <div>
                    <WhatsappShareButton url={BASE_URL + 'explore-jobs/' + task.slug}>
                        <WhatsappIcon />
                    </WhatsappShareButton>
                    Whatsapp
                </div>
                <div>
                    <EmailShareButton url={BASE_URL + 'explore-jobs/' + task.slug} >
                        <EmailIcon bgStyle={{ fill: '#8F39CE' }} />
                    </EmailShareButton>
                    Email
                </div>
                <div>
                    <ClipboardButton
                        className="copy"
                        option-text={() => BASE_URL + 'explore-jobs/' + task.slug}
                        onSuccess={() => toast.showMessage("success", "Share URL copied")}
                    >
                        <FiLink size="36" />
                    </ClipboardButton>
                    Copy
                </div>
            </Modal>
        </>
    )
}