import React from "react";
import Input from "../../../Input";
import { FiMic, FiSearch, FiX } from "react-icons/fi";
import classes from "./style.module.scss";

export default function TaskSearch({ value, onChange, styles, mobile }) {
  const [SearchQuery, setSearchQuery] = React.useState(value);

  const handleClear = () => {
    setSearchQuery("");
    onChange("");
  };
  const handleSearch = (e) => {
    e.preventDefault();
    onChange(SearchQuery);
  };

  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      handleSearch(e);
    }
  };

  return (
    <div className={classes["task-search"]}>
      <div className={`task-search-box`} style={styles ? styles : {}}>
        <Input
          name="search"
          onChange={(e) => setSearchQuery(e.target.value)}
          onKeyPress={handleKeyPress}
          value={SearchQuery}
          placeholder={mobile ? "Find Jobs" : "Search for a job"}
          beforeIcon={
            <div>
              <FiSearch className="icon" onClick={handleSearch} />
            </div>
          }
          afterIcon={
            mobile ? (
              <FiMic className="icon" />
            ) : (
              value && <FiX className="icon" onClick={handleClear} />
            )
          }
        />
      </div>
    </div>
  );
}
