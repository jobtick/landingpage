import React from 'react'
import { FiCalendar, FiMapPin } from 'react-icons/fi'
import Link from 'next/link';
import moment from 'moment'
import Typography from 'components/Typography'
import classes from './style.module.scss'
import { Row } from 'reactstrap'


export default function TaskItem({ task, selected, onSelect, newTab }) {

    return (
        <Link href={`/explore-jobs/${task.slug}`}>
            <a target={newTab ? "_blank" : undefined}
                className={`jt-job ${classes['task-item']} ${selected ? classes.active : ''} ${task.status}`}
                onClick={onSelect}
            >
                <Typography variant="caption1" className={`status-${task.status}`}>{task.status[0].toUpperCase() + task.status.substr(1)}</Typography>
                <Row>
                    <Typography variant="body3" className="title">{task.title}</Typography>
                    {task.budget &&
                        <Typography variant="body1" className="budget">${task.amount}</Typography>
                    }
                </Row>
                <Row>
                    <div className="meta">
                        <FiMapPin />
                        <Typography variant="caption1">{task.location || "Remote"}</Typography>
                    </div>
                </Row>
                <Row>
                    <div className="meta">
                        <FiCalendar />
                        <Typography variant="caption1">
                            {task.due_date ?
                                task.due_date && moment(task.due_date).format('dddd, MMMM DD')
                                : 'No date set'
                            }
                        </Typography>
                    </div>
                    {task.offers?.length > 0
                        ?
                        <Row className="offers" >
                            <Row className="avatars">
                                {task.offers.slice(0, 4).map((offer, index) =>
                                    <img src={offer.avatar || '/vector/default-user.svg'} alt={"User " + offer.user_id} key={index} />
                                )}
                            </Row>
                            {task.offers?.length > 4 &&
                                <Typography variant="caption1" className="offers_count">+4</Typography>
                            }
                        </Row>
                        :
                        <Typography variant="caption1">No offer yet</Typography>
                    }
                </Row>
            </a>
        </Link >
    )
}