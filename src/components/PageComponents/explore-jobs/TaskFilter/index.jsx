import React from "react";
import { BsFilter } from "react-icons/bs";
import { FiFilter } from "react-icons/fi";
import { Range, getTrackBackground } from "react-range";
import { Button, FormGroup } from "reactstrap";
import Modal from "components/Modal";
import LocationInput from "components/LocationInput";
import Typography from "components/Typography";
import { formatPlaceName } from "utils/typeTools";
import classes from "./style.module.scss";
import Input from "components/Input";

export default function FilterMenu({ data, onUpdate }) {
  const [Open, setOpen] = React.useState(false);
  /*
        {
            "min_price":30,
            "max_price":870,
            "distance":95,
            "task_type":"physical",
            "hide_assigned":true,
            "location":"Adelaide, South Australia, Australia"
        }
    */
  const [Filters, setFilters] = React.useState({
    min_price: undefined,
    max_price: undefined,
    distance: undefined,
    task_type: undefined,
    hide_assigned: undefined,
    ...data,
  });

  const handleSwitch = (e) => {
    e.persist();
    setFilters((filters) => {
      return {
        ...filters,
        hide_assigned: e.target.checked || undefined,
      };
    });
  };

  const handleTaskType = (name, value) => {
    setFilters((filters) => {
      const { task_type } = filters;
      switch (task_type) {
        case "physical":
          if (name === "remote" && value)
            return {
              ...filters,
              task_type: "all",
            };
          if (name === "physical" && !value)
            return {
              ...filters,
              task_type: "remote",
            };
          break;
        case "remote":
          if (name === "physical" && value)
            return {
              ...filters,
              task_type: "all",
            };
          if (name === "remote" && !value)
            return {
              ...filters,
              task_type: "physical",
            };
          break;
        default:
          if (name === "physical" && !value)
            return {
              ...filters,
              task_type: "remote",
            };
          if (name === "remote" && !value)
            return {
              ...filters,
              task_type: "physical",
            };
      }
    });
  };

  const handleLocation = (location) => {
    if (!location.id) return undefined;
    const { coordinates } = location.geometry;
    setFilters((filters) => {
      return {
        ...filters,
        current_lng: coordinates[0],
        current_lat: coordinates[1],
        location: formatPlaceName(location.place_name),
      };
    });
  };

  const handleSubmit = (e) => {
    if (e) e.preventDefault();
    onUpdate(Filters);
    setOpen(false);
  };

  const handleFilter = (field, data) => {
    if (field === "price")
      setFilters((filters) => {
        return {
          ...filters,
          min_price: data.min,
          max_price: data.max,
        };
      });
    else
      setFilters((filters) => {
        return {
          ...filters,
          [field]: data,
        };
      });
  };
  const getFiltersCount = () => {
    let count = 0;
    if (data.task_type === "remote") count++;
    else count += 3;

    if (data.min_price || data.max_price) count++;

    if (data.hide_assigned) count++;

    return count;
  };
  React.useEffect(() => {
    setFilters(data);
  }, [data]);

  return (
    <>
      <div className={classes["filter-box"]}>
        <Button className="filter-btn" onClick={() => setOpen(true)}>
          <BsFilter />
        </Button>

        <div className="filters">
          <span className="filter">
            {data.task_type && data.task_type !== "all"
              ? data.task_type === "physical"
                ? "In person"
                : "Remotely"
              : "In person & Remotely"}
          </span>
          {data.task_type !== "remote" && data.location && (
            <span className="filter">
              {data.location} - {data.distance || "100+"} km
            </span>
          )}
          {(data.min_price || data.max_price) && (
            <span className="filter">
              ${data.min_price || 5} - ${data.max_price || 9999}
            </span>
          )}
          {data.hide_assigned && <span className="filter">Open jobs only</span>}
        </div>
      </div>
      <Modal
        className={classes["filter-modal"]}
        title="Filters"
        open={Open}
        onClose={() => setOpen(false)}
      >
        <FormGroup className="box">
          <Input
            label="Remotely"
            type="switch"
            name="remotely"
            onChange={(e) => handleTaskType("remote", e.target.checked)}
            checked={["remote", "all"].includes(Filters.task_type)}
          />
        </FormGroup>

        <FormGroup className="box">
          <Input
            label="In person"
            type="switch"
            name="in-person"
            onChange={(e) => handleTaskType("physical", e.target.checked)}
            checked={["physical", "all"].includes(Filters.task_type)}
          />
          {["physical", "all"].includes(Filters.task_type) && (
            <>
              <div className="location">
                <LocationInput
                  label="Suburb"
                  value={Filters.location}
                  onChange={handleLocation}
                />
              </div>
            </>
          )}
        </FormGroup>

        {["physical", "all"].includes(Filters.task_type) && (
          <FormGroup className="box">
            <div className="row-title flex-row">
              <Typography className="title">Distance</Typography>
              <Typography className="values flex-right">
                {`${
                  !Filters.distance || Filters.distance > 100
                    ? "100+"
                    : Filters.distance
                } km`}
              </Typography>
            </div>
            <DistanceFilter
              data={Filters.distance || 105}
              onUpdate={(data) => {
                handleFilter("distance", data <= 100 ? data : undefined);
              }}
            />
          </FormGroup>
        )}

        <FormGroup>
          <div className="row-title flex-row">
            <Typography className="title">Price</Typography>
            <Typography className="values flex-right">
              ${Filters.min_price || 5} - ${Filters.max_price || 9999}
            </Typography>
          </div>
          <PriceFilter
            data={{
              min: Filters.min_price || 5,
              max: Filters.max_price || 9999,
            }}
            onUpdate={(data) => {
              handleFilter("price", data);
            }}
          />
        </FormGroup>

        <FormGroup>
          <Input
            label="Show open jobs only"
            type="switch"
            name="hide-assigned"
            onChange={handleSwitch}
            checked={Filters.hide_assigned}
          />
        </FormGroup>

        <Button color="primary" size="lg" block onClick={handleSubmit}>
          Apply
        </Button>
      </Modal>
    </>
  );
}

export const PriceFilter = ({ data, onUpdate }) => {
  const initData = [50, 200];
  const [price, setPrice] = React.useState(initData);

  if (data.min !== undefined) initData[0] = data.min;
  if (data.max !== undefined) initData[1] = data.max;

  const handleChange = (data) => {
    const newPrice = {
      min: data[0],
      max: data[1],
    };
    setPrice([data[0], data[1]]);
    onUpdate(newPrice);
  };

  return (
    <Range
      step={10}
      min={0}
      max={9999}
      values={price}
      onChange={handleChange}
      renderTrack={({ props, children }) => (
        <div
          onMouseDown={props.onMouseDown}
          onTouchStart={props.onTouchStart}
          className="jb-range-track"
          style={props.style}
        >
          <div
            ref={props.ref}
            className="jb-range-fill-track"
            style={{
              background: getTrackBackground({
                values: price,
                colors: ["#ccc", "#5468fd", "#ccc"],
                min: 5,
                max: 9999,
              }),
            }}
          >
            {" "}
            {children}{" "}
          </div>
        </div>
      )}
      renderThumb={({ index, props, isDragged }) => (
        <div
          {...props}
          className={`jb-range-thumb${isDragged ? " dragging" : ""}`}
          style={{ ...props.style }}
        ></div>
      )}
    />
  );
};

export const DistanceFilter = ({ data, onUpdate }) => {
  const handleDistance = (values) => {
    onUpdate(values[0]);
  };
  return (
    <>
      <Range
        step={5}
        min={5}
        max={105}
        values={[data || 25]}
        onChange={handleDistance}
        renderTrack={({ props, children }) => (
          <div
            onMouseDown={props.onMouseDown}
            onTouchStart={props.onTouchStart}
            className="jb-range-track"
            style={props.style}
          >
            <div
              ref={props.ref}
              className="jb-range-fill-track"
              style={{
                background: getTrackBackground({
                  values: [data || 25],
                  colors: ["#5468fd", "#ccc"],
                  min: 5,
                  max: 105,
                }),
              }}
            >
              {children}
            </div>
          </div>
        )}
        renderThumb={({ index, props, isDragged }) => (
          <div
            {...props}
            className={`jb-range-thumb${isDragged ? " dragging" : ""}`}
            style={{ ...props.style }}
          ></div>
        )}
      />
    </>
  );
};
