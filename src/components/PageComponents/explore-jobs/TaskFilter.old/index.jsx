import React from 'react'
import { FiFilter } from 'react-icons/fi'
import { Range, getTrackBackground } from 'react-range';
import { Button, FormGroup } from 'reactstrap'
import Modal from 'components/Modal';
import ButtonSelect from 'components/ButtonSelect';
import LocationInput from 'components/LocationInput';
import Typography from 'components/Typography';
import { formatPlaceName } from 'utils/typeTools';

export default function FilterMenu({ data, onUpdate, searchToggle }) {

    const [Open, setOpen] = React.useState(false)
    /*
        {
            "min_price":30,
            "max_price":870,
            "distance":95,
            "task_type":"physical",
            "hide_assigned":true,
            "location":"Adelaide, South Australia, Australia"
        }
    */
    const [Filters, setFilters] = React.useState({
        min_price: undefined,
        max_price: undefined,
        distance: undefined,
        task_type: undefined,
        hide_assigned: undefined,
        ...data
    })

    const handleSwitch = (e) => {
        e.persist()
        setFilters(filters => {
            return {
                ...filters,
                hide_assigned: e.target.checked || undefined
            }
        })
    }

    const handleLocation = (geoData) => {
        if (!geoData.id) return undefined
        const { coordinates } = geoData.geometry;
        setFilters(filters => {
            return {
                ...filters,
                current_lng: coordinates[0],
                current_lat: coordinates[1],
                location: formatPlaceName(geoData.place_name)
            }
        })
    }

    const handleSubmit = (e) => {
        if (e)
            e.preventDefault();
        onUpdate(Filters)
        setOpen(false)
    }

    const handleFilter = (field, data) => {
        if (field === 'price')
            setFilters(filters => {
                return {
                    ...filters,
                    min_price: data.min,
                    max_price: data.max
                }
            })
        else
            setFilters(filters => {
                return {
                    ...filters,
                    [field]: data
                }
            })
    }
    React.useEffect(
        () => {
            setFilters(data);
        }, [data])

    return (
        <>
            <div className={`filter-box${searchToggle ? ' toggled' : ''}`}>
                <Button color="primary" onClick={() => setOpen(true)}>
                    <FiFilter />
                </Button>
                <div className="filters">
                    <span className="filter">
                        {data.task_type && data.task_type !== 'all' ?
                            data.task_type === 'physical' ? 'In person' : 'Remotely'
                            :
                            'In person & Remotely'
                        }
                    </span>
                    {data.task_type !== 'remotly' && data.location &&
                        <span className="filter">
                            {data.location.split(',')[0]} - {data.distance || '100+'} km
                        </span>
                    }
                    {(data.min_price || data.max_price) &&
                        <span className="filter">
                            ${data.min_price || 5} -
                        ${data.max_price ||9999}
                        </span>
                    }
                    {data.hide_assigned &&
                        <span className="filter">
                            Open jobs only
                        </span>
                    }
                </div>
            </div>
            <Modal
                className="filter-modal"
                title="Filters"
                open={Open}
                onClose={() => setOpen(false)}
            >
                <FormGroup className="tabs">
                    <ButtonSelect
                        size="lg"
                        value={Filters.task_type || 'all'}
                        onChange={(value) => handleFilter("task_type", value)}
                        items={
                            [
                                {
                                    label: 'Remotely',
                                    value: 'remote'
                                },
                                {
                                    label: 'In person',
                                    value: 'physical'
                                },
                                {
                                    label: 'All',
                                    value: 'all'
                                }
                            ]
                        } />
                </FormGroup>

                {Filters.task_type !== 'remote' &&
                    <>
                        <FormGroup className="location">
                            <LocationInput
                                label="Suburb"
                                value={Filters.location}
                                onChange={handleLocation}

                            />
                        </FormGroup>

                        <FormGroup>
                            <div className="row-title flex-row">
                                <Typography className="title">Distance</Typography>
                                <Typography className="values app-flex-right">
                                    {`${!Filters.distance || Filters.distance > 100 ? '100+' : Filters.distance} km`}
                                </Typography>
                            </div>
                            <DistanceFilter
                                data={Filters.distance || 105}
                                onUpdate={(data) => {
                                    handleFilter("distance", data <= 100 ? data : undefined);
                                }}
                            />
                        </FormGroup>
                    </>
                }

                <FormGroup>
                    <div className="row-title flex-row">
                        <Typography className="title">Price</Typography>
                        <Typography className="values app-flex-right">
                            ${Filters.min_price || 5} -
                            ${Filters.max_price || 9999}
                        </Typography>
                    </div>
                    <PriceFilter
                        data={{
                            min: Filters.min_price || 5,
                            max: Filters.max_price || 9999
                        }}
                        onUpdate={(data) => {
                            handleFilter("price", data);
                        }}
                    />
                </FormGroup>

                <FormGroup className="hide-jobs-toggle">

                    <input
                        type="checkbox"
                        id="hideAssigned"
                        name="hideAssigned"
                        onChange={handleSwitch}
                        checked={Filters.hide_assigned}
                    />
                    <label htmlFor="hideAssigned">
                        <span>Show open jobs only</span>
                        <div className={`handle${Filters.hide_assigned ? ' active' : ''}`}></div>
                    </label>
                </FormGroup>

                <Button color="primary" size="lg" block onClick={handleSubmit}>
                    Apply
                </Button>
            </Modal>
        </>
    )
}

export const PriceFilter = ({ data, onUpdate }) => {
    const initData = [50, 200];
    const [price, setPrice] = React.useState(initData);

    if (data.min !== undefined)
        initData[0] = data.min;
    if (data.max !== undefined)
        initData[1] = data.max;

    const handleChange = (data) => {
        const newPrice = {
            min: data[0],
            max: data[1]
        }
        setPrice([data[0], data[1]]);
        onUpdate(newPrice);
    }

    return (
        <Range step={10} min={0} max={9999} values={price} onChange={handleChange}
            renderTrack={({ props, children }) => (
                <div onMouseDown={props.onMouseDown} onTouchStart={props.onTouchStart} className="jb-range-track" style={props.style}>
                    <div ref={props.ref} className="jb-range-fill-track"
                        style={{
                            background: getTrackBackground({
                                values: price,
                                colors: ['#ccc', '#5468fd', '#ccc'],
                                min: 5, max: 9999
                            })
                        }}
                    > {children} </div>
                </div>
            )}
            renderThumb={({ index, props, isDragged }) => (
                <div {...props} className={`jb-range-thumb${isDragged ? ' dragging' : ''}`} style={{ ...props.style }}></div>
            )}
        />
    )
}


export const DistanceFilter = ({ data, onUpdate }) => {

    const handleDistance = (values) => {
        onUpdate(values[0]);
    }
    return (
        <>
            <Range step={5} min={5} max={105} values={[data || 25]} onChange={handleDistance}
                renderTrack={({ props, children }) => (
                    <div onMouseDown={props.onMouseDown} onTouchStart={props.onTouchStart} className="jb-range-track" style={props.style}>
                        <div ref={props.ref} className="jb-range-fill-track"
                            style={{
                                background: getTrackBackground({
                                    values: [data || 25],
                                    colors: ['#5468fd', '#ccc'],
                                    min: 5, max: 105
                                })
                            }}
                        >{children}</div>
                    </div>
                )}
                renderThumb={({ index, props, isDragged }) => (
                    <div {...props} className={`jb-range-thumb${isDragged ? ' dragging' : ''}`} style={{ ...props.style }}></div>
                )}
            />
        </>
    )
}