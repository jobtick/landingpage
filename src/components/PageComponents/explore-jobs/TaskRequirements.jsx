import Typography from 'components/Typography'
import React from 'react'
import { FiAlertCircle } from 'react-icons/fi'

export default function TaskRequirements({ task }) {
    return (
        <>
            {task.musthave?.length > 0 &&
                <div className="job-requirements">
                    {task.musthave.map((item, index) => (
                        <div className="item" key={index}>
                            <FiAlertCircle />
                            <Typography component="span" variant="body3">{item}</Typography>
                        </div>
                    ))}
                </div>
            }
        </>
    )
}