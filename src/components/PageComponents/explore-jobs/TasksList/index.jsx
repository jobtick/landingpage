import React from 'react'
import Spinner from 'components/Spinner'
import TaskItem from '../TaskItem'

function TasksList ({ tasks, selected = [''], isLoading, forwardRef, onSelect }) {
    const spinnerRef = React.createRef()

    React.useEffect(() => {
        if (spinnerRef.current) {
            let parent = spinnerRef.current.parentElement.getBoundingClientRect()
            spinnerRef.current.style.position = 'fixed'
            spinnerRef.current.style.width = parent.width + 'px'
            spinnerRef.current.style.height = parent.height + 'px'
            spinnerRef.current.style.top = parent.top + 'px'
            spinnerRef.current.style.left = parent.left + 'px'
        }
    }, [spinnerRef])
    return (
        <div ref={forwardRef} className="tasks-list">
            {isLoading && <Spinner type="box" forwardRef={spinnerRef} />}
            {tasks.length > 0 &&
                tasks.map((item, index) => {
                    return (
                        <TaskItem
                            key={index}
                            task={item}
                            taskPath="/explore-jobs"
                            selected={item.slug === selected[0]}
                            onSelect={onSelect}
                        />
                    )
                })
            }
        </div>
    )
}

export default TasksList;
