import React from 'react'
import ProfileAvatar from 'components/ProfileAvatar'
import { FiCalendar, FiMapPin } from 'react-icons/fi'
import moment from 'moment'
import { Button } from 'reactstrap'
import Typography from 'components/Typography'
import { FaCircle } from 'react-icons/fa'
import { Status } from 'constants/status'
import { ViewOnMapModal } from '../MapView'
import Link from 'next/link'

export const TaskViewStatus = ({ className, task }) => {
    return (
        <div className={`task-status-box${className ? ` ${className}` : ``}`}>
            <Typography component="span" variant="caption1" className={`task-status-${task.status === 'open' ? 'open' : 'default'}`}>Open</Typography>

            {task.status === 'cancelled' ?
                <Typography component="span" variant="caption1" className="task-status-cancelled">Cancelled</Typography> :
                <Typography component="span" variant="caption1" className={`task-status-${task.status === 'assigned' ? 'assigned' : 'default'}`}>Assigned</Typography>
            }

            {task.status === 'Overdue' ?
                <Typography component="span" variant="caption1" className="task-status-overdue">Overdue</Typography> :
                task.status === 'closed' ?
                    <Typography component="span" variant="caption1" className="task-status-closed">Closed</Typography> :
                    <Typography component="span" variant="caption1" className={`task-status-${task.status === 'completed' ? 'completed' : 'default'}`}>Completed</Typography>
            }
        </div>
    )
}
export const TaskViewDetails = ({ className, task }) => {
    const [MapViewOpen, setMapViewOpen] = React.useState(false)

    let due_time = task.due_time && Object.keys(task.due_time).find(key => task.due_time[key] === true)
    due_time = due_time && due_time[0].toUpperCase() + due_time.substr(1)
    return (
        <div className={`task-details-container${className ? ` ${className}` : ``}`}>
            <Typography component="h2" variant="heading2" className="task-title">{task.title}</Typography>
            <Typography component="span" variant="caption1" className="task-posted-at">Posted {moment(task.created_at).fromNow()}</Typography>

            <div className="task-detail-box">
                <FiMapPin />
                <div>
                    <Typography component="span" variant="caption1" className="label">Location</Typography>
                    <Typography component="span" variant="body3" className="value">{task.task_type === "physical" ? task.location : "Remote"}</Typography>
                </div>
                {task.task_type === "physical" &&
                    <>
                        <Button color="link" onClick={() => setMapViewOpen(true)}>
                            <Typography component="span" variant="caption1">View Map</Typography>
                        </Button>
                        <ViewOnMapModal
                            open={MapViewOpen}
                            onClose={() => setMapViewOpen(false)}
                            task={task}
                        />
                    </>
                }
            </div>

            <div className="task-detail-box">
                <FiCalendar />
                <div>
                    <Typography component="span" variant="caption1" className="label">Due date</Typography>
                    <Typography component="span" variant="body3" className="value">
                        {task.due_date && moment(task.due_date).format('dddd, MMMM DD')} {due_time && <><FaCircle /> {due_time}</>}
                    </Typography>
                </div>
            </div>
        </div>
    )
}

export default function TaskViewPoster({ task, className, onAction }) {

    return (
        <div className={`task-poster-container${className ? ' ' + className : ''}`}>
            <div className="poster-details">
                <Link href={"/profile/" + task.poster.id}>
                    <a target="_blank">
                        <ProfileAvatar
                            profile={task.poster}
                            style={{ width: "50px", height: "50px" }}
                        />
                    </a>
                </Link>
                <div className="poster-name">
                    <Typography component="span" variant="caption1" className="label">Posted by</Typography>
                    <Link href={"/profile/" + task.poster.id}>
                        <a className="value" target="_blank">
                            <Typography component="span" variant="caption1">{task.poster.name}</Typography>
                        </a>
                    </Link>
                </div>
            </div>

            <div className="budget">
                <Typography component="span" variant="caption1" className="label">Job Budget</Typography>
                <Typography component="span" variant="heading4" className="value">${task.amount}</Typography>
            </div>
            {task.status === Status.open &&
                <Button
                    color="primary"
                    size="lg"
                    block
                    className="jt-tryforoffer"
                    onClick={onAction}
                >
                    <Typography component="span" variant="body3">Make an offer</Typography>
                </Button>
            }
        </div>
    )
}




