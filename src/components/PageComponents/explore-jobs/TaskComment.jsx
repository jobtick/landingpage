import React from 'react'
import ProfileAvatar from 'components/ProfileAvatar'
import { FiFlag } from 'react-icons/fi'
import { AttachmentPreview } from 'components/AttachmentsView'
import moment from 'moment'
import Typography from 'components/Typography'
import Link from 'next/link'


export const TaskCommentView = ({ comments, task, onAction }) => {
    const [Count, setCount] = React.useState(1)
    return (
        <div className="task-comments-container">
            {Count < comments.length &&
                <Typography
                    component="span"
                    variant="caption1"
                    className="task-comment-count"
                    onClick={() => setCount(comments.length)}
                >
                    View {comments.length - 1} replies
                </Typography>
            }
            {comments.reverse().slice(comments.length - Count).map((item, index) => (
                <TaskComment
                    key={index}
                    comment={item}
                    task={task}
                    onAction={onAction}
                />
            ))}
        </div>
    )
}

export const TaskComment = ({ comment, task, onAction }) => {
    const { user } = comment;
    return (
        <div className="task-comment-item app-flex-column ">
            <div className="task-comment-details">
                <Link href={"/profile/" + user.id}>
                    <a target="_blank">
                        <ProfileAvatar
                            profile={user}
                        />
                    </a>
                </Link>
                <div className="user-details">
                    <Link href={"/profile/" + user.id}>
                        <a className="user-name" target="_blank">
                            <Typography component="span" variant="body3">{`${user.name}`}</Typography>
                        </a>
                    </Link>
                    {comment.user.id === task.poster.id &&
                        <Typography component="span" variant="caption1" className="user-role">Poster</Typography>
                    }
                </div>
                <div className="comment-action">
                    <span className="report-icon absolute-label" data-label="Report" onClick={onAction}><FiFlag /></span>
                </div>
            </div>
            <div className="task-comment-content">
                <Typography component="p" variant="body3" className="task-comment-message">{comment.comment_text}</Typography>
                <AttachmentPreview
                    attachments={comment.attachments}
                    preview={true}
                    downloadable={true}
                    containerStyle={{ marginLeft: "auto" }}
                    itemStyle={{ padding: "0px", width: "50px", height: "50px" }}
                />
                <Typography component="span" variant="caption1" className="task-comment-time">{moment(comment.created_at).fromNow()}</Typography>
            </div>
        </div>
    )
}
export default TaskCommentView
