import React from 'react';
import { FiCopy, FiFlag } from 'react-icons/fi';
import TaskEditor from './TaskEditor';
import { Status } from 'constants/status';
import Spinner from 'components/Spinner';
import ShareTask from './Share';

export const TaskActions = ({ task, onAction }) => {

    return (
        <div className="task-actions">
            {![Status.cancelled, Status.draft].includes(task.status) &&
                <ShareTask task={task} />
            }

            <TaskEditor
                renderTrigger={(onClick, isLoading) => (
                    <span className="task-action-icon absolute-label" data-label="copy" onClick={onClick}>
                        {isLoading ? <Spinner type="button" style={{ borderTopColor: "#333" }} /> : <FiCopy />}
                    </span>
                )}
                mode="duplicate"
                task={task}
            />
            <span className="task-action-icon absolute-label" data-label="Report" onClick={onAction}><FiFlag /></span>

        </div>
    );
};