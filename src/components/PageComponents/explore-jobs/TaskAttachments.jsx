import React, { useState } from 'react'
import dynamic from 'next/dynamic'
const Viewer = dynamic(() => import('react-viewer'), {
    ssr: false
})

const TaskAttachments = ({ task }) => {
    const [visible, setVisible] = useState(false);

    var gridClass = "task-attachments ";
    const { attachments } = task;
    if (attachments.length >= 3) gridClass += "grid-tre";
    else if (attachments.length === 2) gridClass += "grid-sec";
    else gridClass += "grid-one";

    const viewerItem = [];
    Object.keys(attachments).forEach(function (key) {
        viewerItem.push({
            src: attachments[key].modal_url,
            downloadUrl: attachments[key].url,
            alt: attachments[key].name
        })
    });

    var countAttachment = 0;
    const renderAttachment = () => {
        const gridItems = [];

        Object.keys(attachments).forEach(function (key) {
            if (countAttachment >= 3) return gridItems;
            countAttachment++;
            var item = attachments[key];
            gridItems.push(
                <TaskAttachmentItem
                    key={key}
                    order={countAttachment}
                    item={item}
                    renderMoreBtn={(countAttachment >= 3) ? true : false}
                    onClick={() => {
                        setVisible(true);
                    }}
                />
            );
        });

        return gridItems;
    }

    return (
        <div className={gridClass}>
            {attachments.length > 0 ?
                renderAttachment()
                :
                task.task_type === "physical" ?
                    <div order={1} className="grid-item default-image" style={{ backgroundImage: `url('/images/Offline.png')` }} />
                    :
                    <div order={1} className="grid-item default-image" style={{ backgroundImage: `url('/images/Online.png')` }} />

            }
            <Viewer
                visible={visible}
                onClose={() => { setVisible(false); }}
                images={viewerItem}
                drag={false}
                rotatable={false}
                scalable={false}
                attribute={false}
                downloadable={true}
                noNavbar={true}
                noToolbar={false}
                zoomable={true}
                zoomSpeed={0.6}
            />
        </div>
    )
}

export default TaskAttachments;

export const TaskAttachmentItem = ({ order, item, className, renderMoreBtn, onClick }) => {
    return (
        <div
            onClick={onClick}
            order={order}
            className={`grid-item${className ? ` ${className}` : ``}`}
            style={{ backgroundImage: `url('${item.modal_url}')` }}
        >
            {renderMoreBtn && <button className="jb-btn app-flex-both-center">View all</button>}
        </div>
    )
}
