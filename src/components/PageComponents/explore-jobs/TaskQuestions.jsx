import React from 'react'
import ProfileAvatar from 'components/ProfileAvatar'
import { FiFlag, FiMessageSquare } from 'react-icons/fi'
import { AttachmentPreview } from 'components/AttachmentsView'
import { TaskCommentView } from './TaskComment'
import moment from 'moment'
import Typography from 'components/Typography'
import NoContent from 'components/NoContent'
import Link from 'next/link'

const TaskViewQuestion = ({ task, onAction }) => {
    return (
        <div className="task-question-container task-block">
            <Typography component="h4" variant="body1" className="block-title">Questions</Typography>
            <div className="task-questions-list">
                {task.questions?.length > 0 ?
                    task.questions.reverse().map((item, index) => (
                        <TaskQuestion
                            key={index}
                            task={task}
                            question={item}
                            onAction={onAction}
                        />
                    ))
                    :
                    <NoContent icon={<FiMessageSquare />} label="No questions" />
                }
            </div>
        </div>
    )
}

export default TaskViewQuestion;

export const TaskQuestion = ({ task, question, onAction }) => {
    var { user } = question;

    return (
        <div className="task-question">
            <div className="task-question-details">
                <Link href={"/profile/" + user.id}>
                    <a target="_blank">
                        <ProfileAvatar profile={user} />
                    </a>
                </Link>
                <div className="user-details">
                    <Link href={"/profile/" + user.id}>
                        <a className="user-name" target="_blank">
                            <Typography component="span" variant="body3">{`${user.name}`}</Typography>
                        </a>
                    </Link>
                </div>
                <div className="question-action">
                    <span className="report-icon absolute-label" data-label="Report" onClick={onAction}><FiFlag /></span>
                </div>
            </div>
            <div className="task-question-content">
                <Typography component="p" variant="body3" className="task-comment-message">{question.question_text}</Typography>
                {question.attachments && question.attachments.length > 0 &&
                    <AttachmentPreview
                        attachments={question.attachments}
                        preview={true}
                        downloadable={true}
                        itemStyle={{ padding: "0px" }}
                    />
                }
                <Typography component="span" variant="caption1" className="task-question-time">{moment(question.created_at).fromNow()}</Typography>
            </div>
            {question.comments?.length > 0 &&
                <TaskCommentView task={task} comments={question.comments} container className={"task-question-details"} onAction={onAction} />
            }
        </div>
    )
}