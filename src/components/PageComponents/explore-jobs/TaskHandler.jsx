import React from "react";
import { TaskActions } from "./TaskAction";
import TaskViewPoster, {
  TaskViewStatus,
  TaskViewDetails,
} from "./TaskViewBlock";
import TaskAttachments from "./TaskAttachments";
import TaskOfferContainer from "./TaskOffers";
import TaskViewQuestion from "./TaskQuestions";
import { Col } from "reactstrap";
import { FiBriefcase } from "react-icons/fi";
import Typography from "components/Typography";
import TaskRequirements from "./TaskRequirements";
import AuthModal from "components/AuthModal";
import { APP_BASE_URL } from "constants/config";

export default function TaskHandler({ TaskData }) {
  const InvalidTask = !TaskData?.data?.id;
  const task = TaskData?.data;

  const [ShowSignIn, setShowSignIn] = React.useState(false);

  if (InvalidTask) {
    return (
      <div className="no-content">
        <div className="icon">
          <FiBriefcase />
        </div>
        <span className="label">We couldn't find this task</span>
      </div>
    );
  }

  const handleUserAction = () => {
    setShowSignIn(true);
  };

  return (
    <div className="task-view">
      <div className="task-view-container">
        <div className="task-header-container flex-row mb-3">
          <TaskAttachments task={task} />
          <TaskActions task={task} onAction={handleUserAction} />
        </div>
        <div className="task-info-container">
          <Col xl={8}>
            <TaskViewStatus task={task} />
            <TaskViewDetails task={task} />
          </Col>
          <Col xl={4}>
            <TaskViewPoster task={task} onAction={handleUserAction} />
          </Col>
        </div>

        <div className="task-description-container">
          <Typography component="span" variant="caption1" className="label">
            Details
          </Typography>
          <Typography component="p" variant="body3" className="content">
            {task.description}
          </Typography>
        </div>

        <TaskRequirements task={task} />
      </div>

      <TaskOfferContainer task={task} onAction={handleUserAction} />
      <TaskViewQuestion task={task} onAction={handleUserAction} />

      {ShowSignIn && (
        <AuthModal
          open={ShowSignIn}
          onClose={() => setShowSignIn(false)}
          redirectAfter={APP_BASE_URL + "explore-jobs/" + task?.slug}
        />
      )}
    </div>
  );
}
