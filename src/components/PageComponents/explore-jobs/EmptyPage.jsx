import React from 'react';
import Lottie from "react-lottie";
import vector from "../../../assets/vector/animations/No Post.json";


const EmptyPage=()=>{

    return(
        <div className="emptyPage">
            <Lottie className="animation"
                    width="auto"
                    height="auto"
                    options={
                        {
                            loop: true,
                            autoplay: true,
                            animationData: vector,
                            rendererSettings: {
                                preserveAspectRatio: 'xMidYMid slice'
                            }
                        }
                    }
                    isClickToPauseDisabled={true}
            />
            <span>No job posted or completed</span>
        </div>
    )
}
export default EmptyPage
