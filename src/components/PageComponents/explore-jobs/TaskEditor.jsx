import React from 'react'
import PostTaskModal from 'components/PostTaskModal'
import useIsMounted from 'react-is-mounted-hook';


export default function TaskEditor({ mode, task, onUpdate, onDuplicate, renderTrigger }) {
    const isMounted = useIsMounted()

    const [IsLoading, setIsLoading] = React.useState(false)
    const [RenderPostTask, setRenderPostTask] = React.useState(false)

    const handleOpen = (e) => {
        if (e)
            e.preventDefault();
        if (IsLoading)
            return undefined

        setIsLoading(true)
        setTimeout(() => {
            if (isMounted) {
                setIsLoading(false)
                setRenderPostTask(true)
            }
        }, 500)
    }
    const handleClose = (e) => {
        if (e)
            e.preventDefault();
        setRenderPostTask(false)
    }
    return (
        <>
            {renderTrigger &&
                renderTrigger(handleOpen, IsLoading)
            }
            {RenderPostTask &&
                <PostTaskModal
                    mode={mode}
                    category_id={task?.category_id || undefined}
                    task={task}
                    onEditSuccess={onUpdate ? onUpdate : false}
                    onDuplicate={onDuplicate}
                    onClose={handleClose}
                />
            }
        </>
    );
}
