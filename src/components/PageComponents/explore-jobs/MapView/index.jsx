import React, { useMemo } from "react";
import ReactMapboxGl, { Popup, Marker, ZoomControl } from "react-mapbox-gl";
import { FiX } from "react-icons/fi";
import { FaDirections } from "react-icons/fa";
import { MAP_BOX_TOKEN } from "constants/config";
import TaskItem from "../TaskItem";
import Modal from "components/Modal";

const Mapbox = ReactMapboxGl({
  accessToken: MAP_BOX_TOKEN,
});

function MapView({ center, zoom, data, mode = "task", task }) {
  const [Center, setCenter] = React.useState(
    center || [153.40391944986868, -28.014706676951178]
  );
  const [Zoom, setZoom] = React.useState(zoom || [11]);
  const [Task, setTask] = React.useState(task);
  const [Markers, setMarkers] = React.useState([]);

  React.useEffect(() => {
    if (center) {
      setCenter(center);
      setZoom([11]);
    }
    if (navigator?.geolocation) {
      navigator.geolocation.getCurrentPosition((pos) => {
        const coords = pos.coords;
        if (!center) setCenter([coords.longitude, coords.latitude]);
        setMarkers((markers) => [
          ...markers,
          {
            title: "Current position",
            isCurrent: true,
            position: [coords.longitude, coords.latitude],
          },
        ]);
      });
    }
  }, [center]);

  const handleZoom = (map, dif) => {
    var temp = Zoom[0] + dif;
    var newZoom = temp > 20 ? 20 : temp < 1 ? 1 : temp;
    setZoom([newZoom]);
  };

  const closePopup = () => {
    setZoom([11]);
    setTask(undefined);
  };

  const handleClick = (task, position) => {
    setTask(task);
    setCenter(position);
    setZoom([13]);
  };

  const handleMapLoad = (map) => {
    map.resize();
  };

  return (
    <>
      <Mapbox // eslint-disable-next-line
        style="mapbox://styles/jobtick/ckjveetlr0eqw17nul166z00t"
        center={Center}
        zoom={Zoom}
        flyToOptions={{ speed: 0.8 }}
        containerStyle={{ height: "100%", width: "100%" }}
        onStyleLoad={handleMapLoad}
      >
        {data.map((task, index) => {
          if (task.location && isValidPosition(task.latitude, task.longitude)) {
            var position = [task.longitude, task.latitude];
            if (!position) return "";
            return (
              <Marker key={index} coordinates={position}>
                <JBMarker onClick={() => handleClick(task, position)} />
              </Marker>
            );
          }
          return undefined;
        })}
        {Markers.map((marker, index) => (
          <Marker key={index} coordinates={marker.position}>
            <JBMarker
              markerColors={{ primary: "#39c43e", secondary: "#239927" }}
              onClick={() => {}}
            />
          </Marker>
        ))}
        {Task && (
          <Popup coordinates={[Task.longitude, Task.latitude]}>
            <TaskMiniView task={Task} mode={mode} close={closePopup} />
          </Popup>
        )}
      </Mapbox>
      <ZoomControl className="map-zoom-control" onControlClick={handleZoom} />
      {Task && (
        <a
          className="to-navigation"
          href={`https://www.google.com/maps/dir/?api=1&destination=${Task.location}&travelmode=driving`}
          target="_blank"
          rel="noopener noreferrer"
        >
          <FaDirections />
        </a>
      )}
    </>
  );
}

const TaskMiniView = ({ task, close }) => {
  return (
    <div className="map-popup-task">
      <span className="popup-close" onClick={close}>
        <FiX />
      </span>
      <TaskItem task={task} taskPath="explore-jobs" />
    </div>
  );
};

function isValidPosition(latitude, longitude) {
  if (!latitude && !longitude) return false;
  return true;
}

function getLatLongFromPoint(position) {
  if (position.latitude && position.longitude) {
    return [position.longitude, position.latitude];
  }
}

export default MapView;

export function ViewOnMapModal({ open, onClose, task }) {
  if (task.position) {
    task.latitude = task.position.latitude;
    task.longitude = task.position.longitude;
  }
  return (
    <Modal
      className="viewonmap-modal"
      open={open}
      onClose={onClose}
      title={task.location}
    >
      <MapView
        center={getLatLongFromPoint(task.position)}
        zoom={[10]}
        data={[task]}
        task={task}
      />
    </Modal>
  );
}

export const JBMarker = ({ onClick, markerColors }) => (
  <div onClick={onClick} className="jb-marker">
    <svg
      className={`jb-map-marker`}
      version="1.1"
      id="Layer_1"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      x="0px"
      y="0px"
      width="100px"
      height="100px"
      viewBox="0 0 100 100"
      enableBackground="new 0 0 100 100"
      xmlSpace="preserve"
    >
      <g transform="translate(0 -1028.4)">
        <path
          fill={markerColors ? markerColors.primary : "#5468FD"}
          d="M51,1030.4c-17.673,0-32,14.327-32,32c0,5.684,1.526,11,4.125,15.624c0.432,0.768,0.884,1.524,1.375,2.252
                    L51,1126.4l26.5-46.124c0.408-0.604,0.76-1.244,1.124-1.876l0.252-0.376c2.596-4.624,4.124-9.94,4.124-15.624
                    C83,1044.727,68.672,1030.4,51,1030.4z M51,1046.4c8.836,0,16,7.164,16,16c0,8.836-7.164,16-16,16c-8.836,0-16-7.164-16-16
                    C35,1053.564,42.164,1046.4,51,1046.4z"
        />
        <path
          fill={markerColors ? markerColors.secondary : "#3957C4"}
          d="M51,1042.4c-11.046,0-20,8.954-20,20c0,11.044,8.954,20,20,20c11.044,0,20-8.956,20-20
                    C71,1051.354,62.044,1042.4,51,1042.4z M51,1050.4c6.628,0,12,5.372,12,12s-5.372,12-12,12s-12-5.372-12-12
                    S44.372,1050.4,51,1050.4z"
        />
      </g>
    </svg>
  </div>
);
