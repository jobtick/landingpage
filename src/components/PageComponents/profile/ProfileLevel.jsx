import React from "react";
import { FiInfo } from "react-icons/fi";
import { Col, Row } from "reactstrap";
import { getLevelIconUrl } from "utils";
import Typography from "components/Typography";
import CircleProgressbar from "components/CircleProgressbar";

export default function ProfileLevel({ profile, isFetching }) {
  const getPercent = (level) => {
    if (profile.poster_tier?.level === level) return 50;
    if (profile.poster_tier?.level > level) return 100;
    if (profile.poster_tier?.level + 1 === level) return 25;

    return 0;
  };
  const Levels = [
    {
      label: "Level 1",
      level: 1,
      service_rate: 15,
      description: "Less than $999 in the last 30 days",
      icon: getLevelIconUrl(1),
      percent: getPercent(1),
    },
    {
      label: "Level 2",
      level: 2,
      service_rate: 12,
      description: "$1000+ in the last 30 days",
      icon: getLevelIconUrl(2),
      percent: getPercent(2),
    },
    {
      label: "Level 3",
      level: 3,
      service_rate: 10,
      description: "$2000+ in the last 30 days",
      icon: getLevelIconUrl(3),
      percent: getPercent(3),
    },
    {
      label: "Level 4",
      level: 4,
      service_rate: 8,
      description: "$4000+ in the last 30 days",
      icon: getLevelIconUrl(4),
      percent: getPercent(4),
    },
  ];

  const [HintToggle, setHintToggle] = React.useState(false);
  const HintRef = React.useRef();
  const handleHintToggle = (e) => {
    e.stopPropagation();

    const bodyEvent = (e) => {
      e.stopPropagation();
      if (e.target !== HintRef.current || !HintRef.current.includes(e.target)) {
        setHintToggle(false);
        HintRef.current.style = undefined;
        document.body.removeEventListener("click", bodyEvent);
      }
    };

    if (HintToggle) {
      setHintToggle(false);
      HintRef.current.style = undefined;
      document.body.removeEventListener("click", bodyEvent);
    } else {
      setHintToggle(true);
      if (HintRef.current) {
        const hintButton = document.querySelector(
          ".profile-level-container .hint-button"
        );
        HintRef.current.setAttribute(
          "style",
          `display:block;top:${
            hintButton.offsetTop + hintButton.offsetHeight
          }px;left:${hintButton.offsetLeft}px;`
        );
      }

      document.body.addEventListener("click", bodyEvent);
    }
  };
  return (
    <div className="plain-card">
      <div className="profile-level-container">
        <span className="profile-level-title">
          Level{" "}
          <span className="hint-button" onClick={handleHintToggle}>
            <FiInfo />
          </span>
        </span>
        <Row className="profile-level-item-container">
          {Levels.map((item) => (
            <Col
              key={item.label}
              className={`profile-level-item${
                profile.poster_tier?.level === item.level ? " active" : ""
              }`}
            >
              <span className="level-item-image">
                <img src={item.icon} alt={item.label} />
              </span>
              <Typography variant="caption1">Level {item.level}</Typography>
            </Col>
          ))}
        </Row>
      </div>
      <div className="profile-level-hint" ref={HintRef}>
        {Levels.map((item) => (
          <div
            className={`level-hint-item${
              profile.poster_tier?.level === item.level ? " active" : ""
            }`}
            key={item.label}
          >
            <span className="level-hint-icon">
              <img src={item.icon} alt={item.level} />
            </span>
            <div className="level-hint-details">
              <div className="level-hint-title">
                <Typography component="h3" variant="body1">
                  {item.label}
                </Typography>
                <Typography>{item.service_rate}% service fee</Typography>
              </div>
              <Typography className="level-hint-description">
                {item.description}
              </Typography>
            </div>
            <CircleProgressbar percent={item.percent} color="primary" />
          </div>
        ))}
      </div>
    </div>
  );
}
