import React from 'react'
import { FiFrown } from 'react-icons/fi'
import NoContent from 'components/NoContent'
export default function ProfileSkills({ profile }) {
    const [IsEmpty, setIsEmpty] = React.useState(true)

    const {skills} = profile

    React.useEffect(() => {
        if (skills)
            for (let i in Object.keys(skills))
                if (skills[Object.keys(skills)[i]]?.length > 0) {
                    setIsEmpty(false)
                    break;
                }
    }, [skills])
    return (
        <div className="plain-card">
            <div className="profile-skill-container">
                <div className="profile-skill-header">
                    <span className="profile-skill-title app-flex-center">Skills</span>
                </div>
                {!IsEmpty &&
                    <>
                        {skills.education?.length > 0 &&
                            <>
                                <span className="profile-skill-subject">Education</span>
                                <div className="row profile-skill-item-container">
                                    {skills.education.map((item, index) => (
                                        <span key={index} className="profile-skill-item">{item?.name}</span>
                                    ))}
                                </div>
                            </>
                        }
                        {skills.experience?.length > 0 &&
                            <>
                                <span className="profile-skill-subject">Experience</span>
                                <div className="row profile-skill-item-container">
                                    {skills.experience.map((item, index) => (
                                        <span key={index} className="profile-skill-item">{item?.name}</span>
                                    ))}
                                </div>
                            </>
                        }
                        {skills.language?.length > 0 &&
                            <>
                                <span className="profile-skill-subject">Language</span>
                                <div className="row profile-skill-item-container">
                                    {skills.language.map((item, index) => (
                                        <span key={index} className="profile-skill-item">{item?.name}</span>
                                    ))}
                                </div>
                            </>
                        }

                        {skills.specialities?.length > 0 &&
                            <>
                                <span className="profile-skill-subject">Specialities</span>
                                <div className="row profile-skill-item-container">
                                    {skills?.specialities.map((item, index) => (
                                        <span key={index} className="profile-skill-item">{item?.name}</span>
                                    ))}
                                </div>
                            </>
                        }

                        {skills.transportation?.length > 0 &&
                            <>
                                <span className="profile-skill-subject">Transportation</span>
                                <div className="row profile-skill-item-container">
                                    {skills.transportation.map((item, index) => (
                                        <span key={index} className="profile-skill-item">{item?.name}</span>
                                    ))}
                                </div>
                            </>
                        }
                    </>
                }
                {IsEmpty &&
                    <NoContent
                        icon={<FiFrown />}
                        label="No skills added yet"
                    />
                }
            </div>
        </div>

    )
}
