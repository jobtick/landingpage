import React from 'react'
import { FiFrown, FiInfo, FiPlayCircle } from 'react-icons/fi'
import { Row } from "reactstrap";
import MediaViewer from 'components/MediaViewer'
import { ImFileEmpty } from 'react-icons/im';
import { getMimeType } from 'utils';
import NoContent from 'components/NoContent';
import Typography from 'components/Typography';

export default function ProfilePortfo({ profile }) {
    const [CurrentPreview, setCurrentPreview] = React.useState()

    const handleModal = (item) => {
        setCurrentPreview(item)
        togglePreview()
    }
    const [PreviewModal, setPreviewModal] = React.useState(false)

    const togglePreview = () => {
        setPreviewModal(!PreviewModal)
    }
    return (
        <div className="plain-card">
            <div className="profile-portfo-container">
                <div className="profile-portfo-header">
                    <span className="profile-portfo-title app-flex-center">Portfolio</span>
                    <Typography className="info" variant="caption1"> <FiInfo size="16px" />Click to enlarge</Typography>
                </div>


                <div className="justify-content-center">
                    <Row className="profile-items-container">

                        {profile.portfolio?.length > 0 ?
                            profile.portfolio.map((item, index) => {
                                const mimeType = getMimeType(item.mime);
                                return (
                                    <div className="profile-portfo-item">
                                        <PortfolioItem
                                            className="profile-portfo-item-image"
                                            key={index}
                                            type={mimeType}
                                            data={item}
                                            onClick={() => { handleModal(item) }}
                                        />
                                    </div>
                                )
                            })
                            :
                            <NoContent
                                icon={<FiFrown />}
                                label="No portfolio added yet"
                            />
                        }

                        {PreviewModal && CurrentPreview &&
                            <MediaViewer
                                media={CurrentPreview}
                                download={true}
                                onClose={togglePreview}
                            />
                        }
                    </Row>
                </div>
            </div>
        </div>

    )
}

const PortfolioItem = ({ type, data, onClick }) => {
    return (
        <div className={`portfolio-item${type ? ` ${type}-item` : ''}`} onClick={onClick}>
            {type === "file" ?
                <div className="portfolio-preview">
                    <ImFileEmpty />
                    {data.file_name && <span className="file-name">{data.file_name}</span>}
                </div>
                :
                <div className="portfolio-preview" onClick={onClick} style={{ backgroundImage: `url(${data.modal_url})` }}>
                    {type === "video" && <FiPlayCircle />}
                </div>
            }
        </div>
    )
}