import React from 'react'
import StarRating from 'components/StarRating'
import Typography from 'components/Typography'
import Link from 'next/link'
export default function ProfileReviews({ profile }) {
    const { poster_ratings, worker_ratings, posted_task_statistics, work_task_statistics } = profile
    return (
        <div className="plain-card">
            <div className="profile-reviews-container">
                <div className="flex-row">
                    <span className="profile-reviews-title">Reviews</span>
                    <Link href={'/profile/reviews/' + profile.id}>
                        <a target="_blank" className="profile-reviews-view app-flex-right">
                            <Typography component="span" variant="caption1">View all</Typography>
                        </a>
                    </Link>
                </div>
                <div className="review-container">
                    <Typography component="span" variant="caption1" className="reviews-title">As a Ticker</Typography>
                    {worker_ratings?.total_ratings > 0 ?
                        <>
                            <div className="flex-row">
                                <StarRating
                                    value={worker_ratings.avg_rating}
                                    disabled
                                />
                                <Typography component="span" variant="caption1" className="rate-count mt-1 ml-2">({worker_ratings.received_reviews})</Typography>
                            </div>

                            <div className="mt-3">
                                <Typography component="span" variant="caption1" className="success-percent">{work_task_statistics.completion_rate}%</Typography>
                                <Typography component="span" variant="caption1"> Job Success</Typography>
                            </div>
                        </>
                        :
                        <Typography component="span" variant="body3" className="reviews-no-content app-flex-center">No review</Typography>
                    }
                </div>
                <div className="review-container">
                    <Typography component="span" variant="caption1" className="reviews-title">As a Poster</Typography>
                    {poster_ratings?.total_ratings > 0 ?
                        <>
                            <div className="app-flex-center mt-2">
                                <StarRating
                                    value={poster_ratings.avg_rating}
                                    disabled
                                />
                                <Typography component="span" variant="caption1" className="rate-count mt-1 ml-2">({poster_ratings.received_reviews})</Typography>
                            </div>

                            <div className="mt-2">
                                <Typography component="span" variant="caption1" className="success-percent">{posted_task_statistics.completion_rate}%</Typography>
                                <Typography component="span" variant="caption1"> Job Success</Typography>
                            </div>
                        </>
                        :
                        <Typography component="span" variant="body3" className="reviews-no-content app-flex-center">No review</Typography>
                    }

                </div>

            </div>
        </div>

    )
}
