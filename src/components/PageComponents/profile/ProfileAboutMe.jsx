import React from 'react'

export default function ProfileAboutMe({ profile }) {
    return (
        <div className="plain-card">
            <div className="profile-about-container">
                <div className="profile-about-header">
                    <span className="profile-about-title app-flex-center">About me</span>
                </div>

                <span className="profile-about-subject">“ {profile.tagline?.trim().length > 0 ? profile.tagline : 'Nothing to show'} ”</span>
                <span className="profile-about-description">{profile.about?.trim().length > 0 ? profile.about : ''} </span>
            </div>
        </div>
    )
}
