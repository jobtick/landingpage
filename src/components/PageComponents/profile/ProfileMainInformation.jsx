import React from 'react'
import ProfileAvatar from 'components/ProfileAvatar'
import { FiFlag, FiFacebook, FiCreditCard, FiInfo } from 'react-icons/fi'
import { IoIosCall } from 'react-icons/io'
import MediaViewer from 'components/MediaViewer'
import moment from 'moment'
import Typography from 'components/Typography'
import AuthModal from 'components/AuthModal'
import { APP_BASE_URL } from 'constants/config'

export default function ProfileMainInformation({ profile }) {

    const [CurrentPreview, setCurrentPreview] = React.useState()

    const handleAvatarPreview = () => {
        if (!profile.avatar)
            return undefined

        setCurrentPreview(profile.avatar)
    }


    const [HintToggle, setHintToggle] = React.useState(false)
    const HintRef = React.useRef()

    const handleHintToggle = (e) => {
        if (HintToggle) {
            setHintToggle(false)
            HintRef.current.style = undefined
        } else {
            setHintToggle(true)
            if (HintRef.current) {
                const hintButton = document.querySelector('.profile-badges-container .hint-button')
                HintRef.current.style.display = 'block'
                HintRef.current.style.top = hintButton.offsetTop + hintButton.offsetHeight + 'px'
                HintRef.current.style.left = hintButton.offsetLeft + 'px'
            }
            const bodyEvent = (e) => {
                if (e.target !== HintRef.current || !HintRef.current.includes(e.target)) {
                    e.stopPropagation()
                    setHintToggle(false)
                    HintRef.current.style = undefined
                    document.body.removeEventListener('click', bodyEvent)
                }
            }
            document.body.addEventListener('click', bodyEvent)
        }
    }

    const [ShowSignIn, setShowSignIn] = React.useState(false)

    const handleUserAction = () => {
        setShowSignIn(true)
    }

    return (
        <div className="plain-card">
            <div className="profile-container">

                <span className="report-icon absolute-label" data-label="Report" onClick={handleUserAction}><FiFlag /></span>
                <ProfileAvatar
                    style={{
                        width: "86px",
                        height: "86px"
                    }}
                    className={`profile-image${profile.avatar ? ' available' : ''}`}
                    profile={profile}
                    onClick={handleAvatarPreview}
                />
                <span className="profile-title">{profile?.name}</span>
                <span className="profile-last-seen">Last Seen {moment(profile.last_online).fromNow() || '...'}</span>
                <span className="profile-place">{profile?.location}</span>
                {profile?.account_status &&
                    <div className="profile-badges-container flex-row">
                        <span className={`profile-badges-icon${profile.account_status.basic_info_completed ? '' : '-disabled'}`}><FiFacebook /></span>
                        <span className={`profile-badges-icon${profile.mobile_verified_at ? '' : '-disabled'}`}><IoIosCall /></span>
                        <span className={`profile-badges-icon${profile.account_status?.bank_account && profile.account_status.credit_card ? '' : '-disabled'}`} ><FiCreditCard /></span>
                        <span className="hint-button" onClick={handleHintToggle}><FiInfo /></span>
                    </div>
                }
                <div className="profile-badges-hint" ref={HintRef}>
                    <Typography>
                        These badges will be shown on when all requirements are provided by user.
                    </Typography>
                </div>
            </div >
            {
                CurrentPreview &&
                <MediaViewer
                    media={CurrentPreview}
                    download={true}
                    onClose={() => setCurrentPreview(undefined)}
                />
            }

            {ShowSignIn &&
                <AuthModal
                    open={ShowSignIn}
                    onClose={() => setShowSignIn(false)}
                    redirectAfter={APP_BASE_URL + 'profile/' + profile.id}
                />
            }
        </div >
    )
}

