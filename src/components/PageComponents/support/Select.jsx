import React from 'react'


export default function Select({item}) {

    const [Toggle, setToggle] = React.useState("")

    return (
        <div className="choose">
            <div onClick={() => setToggle("active")}>
                <h3>{item.head}</h3>
                <span> > </span>
            </div>
            {Toggle === "active" &&
            <div className="active" >
                <span>{item.data_1}</span>
                <span>{item.data_2}</span>
                <span>{item.data_3}</span>
                <span>{item.data_4}</span>
            </div>
            }
        </div>
    )
}