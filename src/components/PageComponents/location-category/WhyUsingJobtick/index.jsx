
import Typography from 'components/Typography';
import React from 'react'
import { Col, Container } from 'reactstrap';
import classes from './style.module.scss'
function WhyUsingJobtick({staticData}) {
    return (
        <div className={classes['why-using-jobtick']}>
            <Container>
                <div className='header'>
                    <Typography component='h2' variant='heading3'>{staticData.title}</Typography>
                    <Typography component='p' variant='body1'>{staticData.subTitle }</Typography>
                </div>
                <div className='items'>
                    {staticData.items.map((item, index) =>
                        <Col lg={3} md={6} key={index}>
                            <div className='circle'>
                                <img src={item.image} />
                            </div>
                            <div className='content'>
                                <Typography component='h3' variant='sub-heading1'>{item.title}</Typography>
                                <Typography component='span' variant='body3'>{item.description}</Typography>
                            </div>
                        </Col>
                    )}
                </div>
            </Container>

        </div>
    );
}

export default WhyUsingJobtick;