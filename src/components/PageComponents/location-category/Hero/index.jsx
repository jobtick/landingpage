import React from "react";
import Typography from "components/Typography";
import { FiMapPin } from "react-icons/fi";
import { Button, Col, Container } from "reactstrap";
import classes from "./style.module.scss";
import Image from "next/image";
import Link from "next/link";

function Hero({ cat, title, location, type = "location" }) {
  const [InputFields, setInputFields] = React.useState({
    location: location
      ? location[0].toUpperCase() + location.substr(1)
      : undefined,
  });

  const getData = () => {
    setInputFields((inputFields) => {
      return { ...inputFields, category: cat };
    });
  };
  React.useEffect(getData, []);

  return (
    <div className={classes.hero}>
      <div className="background">
        <Image
          src={`/images/category/${cat}/hero-background.jpg`}
          alt={title}
          layout="fill"
          quality={100}
          priority={true}
        />
      </div>
      <Container>
        <Typography component="h2" variant="heading0">
          {type === "location" ? (
            <>Find top experts in your area to get your job perfectly done.</>
          ) : (
            <>
              Hire top rated {title} experts{" "}
              {location &&
                " in " +
                  location
                    .split("-")
                    .map((word) => word[0].toUpperCase() + word.substr(1))
                    .join(" ")}
            </>
          )}
        </Typography>

        <form
          className={`hero-searchbox${type === "category" ? " category" : ""}`}
          onSubmit={(e) => e.preventDefault()}
          autoComplete="off"
        >
          {type === "location" ? (
            <>
              <Col md={6} className="category-box">
                {/*<Input
                                    name='category'
                                    type='select'
                                    beforeIcon={<img src='/vector/category/search-box.svg' />}
                                    onChange={value => setInputFields(inputFields => { return { ...inputFields, category: value } })}
                                    placeholder="What is the category?"
                                    items={
                                        Categories.map(item => {
                                            return {
                                                label: item,
                                                value: item.toLowerCase().replace(/\s/g, '-')
                                            }
                                        })
                                    }
                                    value={InputFields.category}
                                />*/}
                <img src="/vector/category/search-box.svg" />
                <Typography variant="body2">{title}</Typography>
              </Col>
              <Col md={4} className="location-box">
                <FiMapPin className="icon" />
                <Typography variant="body2">{InputFields.location}</Typography>
              </Col>
              <Col md={2}>
                <Link href="/post-job">
                  <a>
                    <Button color="primary" block>
                      <Typography component="span" variant="heading4">
                        Get quote
                      </Typography>
                    </Button>
                  </a>
                </Link>
              </Col>
            </>
          ) : (
            <>
              <Col md={9} className="category-box">
                {/*<Input
                                    name='category'
                                    type='select'
                                    beforeIcon={<img src='/vector/category/search-box.svg' />}
                                    onChange={value => setInputFields(inputFields => { return { ...inputFields, category: value } })}
                                    placeholder="What is the category?"
                                    items={
                                        Categories.map(item => {
                                            return {
                                                label: item,
                                                value: item.toLowerCase().replace(/\s/g, '-')
                                            }
                                        })
                                    }
                                    value={InputFields.category}
                                />*/}
                <img src="/vector/category/search-box.svg" />
                <Typography variant="body2">{title}</Typography>
              </Col>
              <Col md={3}>
                <Link href="/post-job">
                  <a>
                    <Button color="primary" block>
                      <Typography component="span" variant="heading4">
                        Get quote
                      </Typography>
                    </Button>
                  </a>
                </Link>
              </Col>
            </>
          )}
        </form>
      </Container>
    </div>
  );
}
export default Hero;
