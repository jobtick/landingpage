
import Typography from 'components/Typography'
import React from 'react'
import { Col, Container, Button } from 'reactstrap';
import _ from 'lodash'
import classes from './style.module.scss'
import { getLineCounts, getLineHeight } from 'utils/domTools';

function WhatInclude({ staticData }) {
    return (
        <div className={classes["what-include"]}>
            <Container>
                <div className='header'>
                    <Typography component='h3' variant='heading3'>{staticData.title}</Typography>
                    <Typography component='span' variant='body1'>
                        {staticData.subTitle}
                    </Typography>
                </div>
                <div className='items'>
                    {staticData.items.map((item, index) => <WhatIncludeItem list={staticData.items} item={item} index={index} key={index} />)}
                </div>
            </Container>
        </div>
    );
}
function WhatIncludeItem({ item, index, list }) {
    const desktopLines = 5
    const mobileLines = 1
    const [DisplayLines, setDisplayLines] = React.useState()
    const [TotalLines, setTotalLines] = React.useState(0)
    const [ScrollHeight, setScrollHeight] = React.useState(0)
    const [ShowFade, setShowFade] = React.useState(false)

    const textRef = React.useRef()
    React.useLayoutEffect(() => {
        if (textRef.current) {
            setScrollHeight((window.innerWidth > 576 ? desktopLines : mobileLines) * getLineHeight(textRef.current))
            if (ScrollHeight && TotalLines === 0){
                console.log(textRef.current.offsetHeight)
                setTotalLines(getLineCounts(textRef.current))
            }
        }
    }, [textRef.current, TotalLines])

    React.useEffect(() => {
        setDisplayLines(window.innerWidth > 576 ? desktopLines : mobileLines)
        if (textRef.current && ScrollHeight > 0) {
            textRef.current.style.maxHeight = ScrollHeight + 'px'
        }
    }, [textRef.current, ScrollHeight])

    React.useEffect(() => {
        if (textRef.current) {
            if (DisplayLines === TotalLines) {
                textRef.current.style.overflow = 'auto'
            } else {
                textRef.current.style.overflow = 'hidden'
            }

            textRef.current.scrollTo(0, 0)

            if (textRef.current.scrollHeight > ScrollHeight)
                setShowFade(DisplayLines === TotalLines)

            if (window.innerWidth < 576 && TotalLines) {
                if (DisplayLines > mobileLines){
                    textRef.current.style.maxHeight = TotalLines * getLineHeight(textRef.current) + 'px'
                }else{
                    textRef.current.style.maxHeight = ScrollHeight + 'px'
                }
            }
        }
    }, [textRef.current, DisplayLines, TotalLines])

    const handleScroll = (e) => {
        if (e.target.scrollHeight <= ScrollHeight + e.target.scrollTop)
            setShowFade(false)
        else {
            if (DisplayLines === TotalLines)
                setShowFade(true)
        }
    }

    const handleShowMore = () => setDisplayLines(TotalLines)
    const handleShowLess = () => setDisplayLines(window.innerWidth > 576 ? desktopLines : mobileLines)

    return <Col lg={list.length === 4 ? 3 : 4} md={6}>
        <div className='item-wrapper'>
            <div className='number' >
                <Typography component='span' variant='heading2'>{index + 1}</Typography>
            </div>
            <div className='content'>
                <Typography component='h3' variant='sub-heading1'>{item.title}</Typography>
                <Typography component='div' variant='body3' className={`description ${ShowFade ? 'more-fade' : undefined}`}>
                    <p className={DisplayLines < TotalLines ? 'hide' : undefined} ref={textRef} onScroll={handleScroll}>
                        {item.description}
                    </p>
                </Typography>

                {typeof window !== 'undefined' && TotalLines > (window.innerWidth > 576 ? desktopLines : mobileLines) &&
                    (DisplayLines < TotalLines ?
                        <Button color="link" onClick={handleShowMore}>
                            <Typography variant='sub-heading1'>Read more</Typography>
                        </Button>
                        :
                        <Button color="link" onClick={handleShowLess}>
                            <Typography variant='sub-heading1'>Read less</Typography>
                        </Button>
                    )
                }
            </div>
        </div>
    </Col>
}
export default WhatInclude;