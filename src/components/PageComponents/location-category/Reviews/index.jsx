import React from 'react'
import StarRating from 'components/StarRating';
import Typography from 'components/Typography';
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import classes from './style.module.scss'
import { FiChevronLeft, FiChevronRight } from 'react-icons/fi';
import { FaQuoteRight } from 'react-icons/fa';
import Image from 'next/image';

function ReviewsBlock({ title, location, staticData }) {
    var settings = {
        arrows: true,
        centerMode: true,
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 0,
        swipeToSlide: true,
        autoplay: false,
        autoplaySpeed: 2000,
        variableWidth: true,
        nextArrow: <NextArrow />,
        prevArrow: <PrevArrow />,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    nextArrow: <NextArrow />,
                    prevArrow: <PrevArrow />,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    };
    return (
        <div className={classes.reviews}>
            <div className='header'>
                <Typography component='h3' variant='heading3'>Latest {title} Reviews{location && ' in ' + location.split('-').map(word => word[0].toUpperCase() + word.substr(1)).join(' ')}</Typography>
                <Typography component='span' variant='body1'>{staticData.subTitle}</Typography>
            </div>
            <Slider {...settings}>
                {staticData.items.map((review, index) =>
                    <div className="item" key={index}>
                        <FaQuoteRight className="icon" />
                        <Typography component='h4' variant='heading4'>{review.title}</Typography>
                        <Typography component='p' variant='body3'>
                            {review.message}
                        </Typography>
                        <div className='writer'>
                            <div className="avatar">
                                <Image src={review.avatar} alt={review.fname + ' ' + (review.lname || '')} width={55} height={55} quality={100} />
                            </div>
                            <div>
                                <Typography component='span' variant='sub-heading2'>{review.fname + ' ' + (review.lname || '')}</Typography>
                                <Typography component='span' variant='caption2' className='role'>{review.role[0].toUpperCase() + review.role.substr(1)}</Typography>
                                <StarRating value={review.rating} disabled />
                            </div>
                        </div>
                    </div>
                )}
            </Slider>
        </div>
    );
}
export default ReviewsBlock;
function NextArrow(props) {
    const { className, onClick } = props;
    return (
        <div
            className={className}
            onClick={onClick}
        >
            <FiChevronRight />
        </div>
    );
}

function PrevArrow(props) {
    const { className, onClick } = props;
    return (
        <div
            className={className}
            onClick={onClick}
        >
            <FiChevronLeft />
        </div>
    );
}