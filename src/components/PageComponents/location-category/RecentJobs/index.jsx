import React from "react";
import Typography from "components/Typography";
import {
  FiCalendar,
  FiChevronLeft,
  FiChevronRight,
  FiMapPin,
} from "react-icons/fi";
import { Container } from "reactstrap";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import moment from "moment";
import { truncateText } from "utils/typeTools";
import classes from "./style.module.scss";
import Image from "next/image";
import Link from "next/link";

function RecentJobs({ data, location, staticData }) {
  var settings = {
    arrows: true,
    centerMode: false,
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    autoplay: false,
    autoplaySpeed: 2000,
    variableWidth: true,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          centerMode: false,
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 992,
        settings: {
          arrows: false,
          centerMode: false,
          variableWidth: true,
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 680,
        settings: {
          arrows: false,
          centerMode: false,
          variableWidth: true,
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <div className={classes["recent-jobs"]}>
      <Container>
        <div className="header">
          <Typography component="h3" variant="heading3">
            Latest {data.title} jobs{" "}
            {location &&
              " in " +
                location
                  .split("-")
                  .map((word) => word[0].toUpperCase() + word.substr(1))
                  .join(" ")}
          </Typography>
          <Typography variant="body1">{staticData.subTitle}</Typography>
        </div>
        <Slider {...settings}>
          {data.tasks.map((job, index) => (
            <Link key={index} href={"/explore-jobs/" + job.slug}>
              <a className="item">
                <div className="content">
                  <div className="avatar">
                    <Image
                      src={job.avatar || "/vector/default-user.svg"}
                      alt={job.title}
                      width={70}
                      height={70}
                      quality={100}
                      priority={true}
                    />
                  </div>
                  <div className="user-info">
                    <div className="title">
                      <Typography component="h4" variant="sub-heading1">
                        {job.title}
                      </Typography>
                      <Typography component="span" variant="sub-heading1">
                        ${job.budget}
                      </Typography>
                    </div>

                    <div className="account-info">
                      <div>
                        <FiMapPin />
                        <Typography component="span" variant="sub-heading2">
                          {job.task_type === "remote" ? "Remote" : job.location}
                        </Typography>
                      </div>
                      <div>
                        <FiCalendar />
                        <Typography component="span" variant="sub-heading2">
                          {moment(job.due_date).format("DD MMMM YYYY")}
                        </Typography>
                      </div>
                    </div>
                  </div>
                </div>
                <Typography component="p" variant="body3">
                  “{truncateText(job.description, 200)}”
                </Typography>
                <div className="status-box">
                  <Typography variant="caption1" className="status">
                    {job.status[0].toUpperCase() + job.status.substr(1)}
                  </Typography>
                  <Typography variant="caption1" className="offers">
                    {job.offers} Offers
                  </Typography>
                </div>
              </a>
            </Link>
          ))}
        </Slider>
      </Container>
    </div>
  );
}
export default RecentJobs;

function NextArrow(props) {
  const { className, onClick } = props;
  return (
    <div className={className} onClick={onClick}>
      <FiChevronRight />
    </div>
  );
}

function PrevArrow(props) {
  const { className, onClick } = props;
  return (
    <div className={className} onClick={onClick}>
      <FiChevronLeft />
    </div>
  );
}
