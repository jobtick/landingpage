import Typography from 'components/Typography'
import React from 'react'
import { Container } from 'reactstrap'
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import classes from './style.module.scss'
import Link from 'next/link';
import { useRouter } from 'next/router';
function RelatedLink({ title, staticData }) {
    const router = useRouter()
    var settings = {
        arrows: false,
        centerMode: true,
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        initialSlide: 1,
        swipeToSlide: true,
        focusOnSelect: true,
        autoplay: false,
        variableWidth: false,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: true,
                    variableWidth: true,
                }
            }
        ]
    };

    const slugGenerator = (text) => {
        return text.toLowerCase().replace(/[&]/g, 'and').replace(/[,?!()./%$']/g, '').trim().replace(/\s/g, '-')
    }
    return (
        <div className={classes["related-link"]}>
            <Container>
                <Slider {...settings}>
                    <div className="column">
                        <div className='header'>
                            <Typography component='h3' variant='heading3'>Related Services</Typography>
                        </div>
                        <ul>
                            {staticData.map(item =>
                                <Typography component='li' variant='body2' key={item}>
                                    <Link href={item.url}>
                                        <a>
                                            {item.name}
                                        </a>
                                    </Link>
                                </Typography>
                            )}
                            {/*<li className='more'><Button color='link'><Typography component='span' variant='sub-heading1'>View more</Typography></Button></li>*/}
                        </ul>
                    </div>
                    <div className="column">
                        <div className='header'>
                            <Typography component='h2' variant='heading3'>Related Locations</Typography>
                        </div>
                        <ul>
                            {['Marrickville', 'Alexandria', 'Camperdown', 'Dawes Point', 'St James', 'Perth'].map(item =>
                                <Typography component='li' variant='body2' key={item}>
                                    <Link href={"/" +
                                        router?.asPath.split("/")[1] +
                                        "/" + slugGenerator(item)}>
                                        <a>
                                            {item}
                                        </a>
                                    </Link>
                                </Typography>
                            )}
                            {/*<li className='more'><Button color='link'><Typography component='span' variant='sub-heading1'>View more</Typography></Button></li>*/}
                        </ul>
                    </div>
                    <div className="column">
                        <div className='header'>
                            <Typography component='h2' variant='heading3'>Top Locations</Typography>
                        </div>
                        <ul>
                            {['Sunshine Coast', 'Brisbane', 'Gold Coast', 'Parramatta', 'Melbourne', 'Perth'].map(item =>
                                <Typography component='li' variant='body2' key={item}>
                                    <Link href={"/" +
                                        router?.asPath.split("/")[1] +
                                        "/" + slugGenerator(item)}>
                                        <a>
                                            {item} {title}
                                        </a>
                                    </Link>
                                </Typography>
                            )}
                        </ul>
                    </div>
                </Slider>
            </Container>

        </div>

    );
}
export default RelatedLink;