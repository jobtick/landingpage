import StarRating from "components/StarRating";
import Typography from "components/Typography";
import React from "react";
import { FaFacebook, FaPhoneAlt } from "react-icons/fa";
import { Button, Col, Container } from "reactstrap";
import moment from "moment";
import classes from "./style.module.scss";
import { round10 } from "utils/typeTools";
import PostTaskModal from "components/PostTaskModal";
import Image from "next/image";
import Link from "next/link";

function TopExpert({ category, staticData }) {
  const [CategoryId, setCategoryId] = React.useState();
  const [Job, setJob] = React.useState();

  return (
    <div className={classes["top-expert"]}>
      <Container>
        <div className="header">
          <div>
            <Typography component="h3" variant="heading3">
              {staticData.title}
            </Typography>
            <Typography component="span" variant="body1">
              {staticData.subTitle}
            </Typography>
          </div>
        </div>
        <div className="items">
          {staticData.items.map((expert, index) => (
            <Col lg={3} sm={5} xs={10} key={index}>
              <div className="wrapper">
                <div className="image-box">
                  <Image
                    src={`/images/category/${category}/expertbg-${
                      index + 1
                    }.jpg`}
                    layout="fill"
                    quality={100}
                    priority={true}
                  />
                  <div className="profile-img">
                    <Image
                      src={expert.avatar}
                      alt={
                        expert.fname +
                        " " +
                        (expert.lname ? expert.lname.substr(0, 1) + "." : "")
                      }
                      quality={100}
                      width={92}
                      height={92}
                      priority={true}
                    />
                  </div>
                  <div className="data-row rating-box">
                    <div className="data-row ">
                      <StarRating value={parseInt(expert.rating)} disabled />
                      <Typography
                        component="span"
                        variant="body3"
                        className="star-number"
                      >
                        {round10(expert.rating, -1)}
                      </Typography>
                    </div>
                    <Typography component="span" variant="body3">
                      {expert.location}
                    </Typography>
                  </div>
                </div>
                <div className="info-box">
                  <div className="data-row name-box">
                    <Typography component="span" variant="sub-heading1">
                      {expert.fname +
                        " " +
                        (expert.lname ? expert.lname.substr(0, 1) + "." : "")}
                    </Typography>
                    <Typography
                      component="span"
                      variant="body3"
                      className="join-date"
                    >
                      Joined in {moment(expert.registertime).format("MMM YYYY")}
                    </Typography>
                  </div>
                  <div className="data-row link-info">
                    <Typography component="span" variant="sub-heading2">
                      Verified Badges
                    </Typography>
                    <div className="data-row">
                      {expert.phone && <FaPhoneAlt />}
                      {expert.facebook && <FaFacebook />}
                    </div>
                  </div>
                  <div className="data-row reviews-box">
                    <Typography component="span" variant="sub-heading2">
                      {expert.rates} Reviews
                    </Typography>
                    <Link href="/post-job">
                      <a>
                        <Button
                          outline
                          color="primary"
                          onClick={() => setCategoryId(9)}
                        >
                          <Typography component="span" variant="caption2">
                            Get a quote
                          </Typography>
                        </Button>
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
            </Col>
          ))}
        </div>

        <PostTaskModal
          category_id={CategoryId}
          task={Job}
          onUpdate={setJob}
          account={undefined}
          onClose={() => setCategoryId()}
        />
      </Container>
    </div>
  );
}
export default TopExpert;
