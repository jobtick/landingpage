import React from 'react'
import Typography from 'components/Typography';
import { FiChevronDown, FiChevronUp } from 'react-icons/fi';
import { Container } from 'reactstrap';
import classes from './style.module.scss'

function FAQ({staticData}) {
    const [Active, setActive] = React.useState()
    return (
        <div className={classes.faq}>
            <Container>
                <Typography component="h3" variant="heading3">{staticData.title}</Typography>
                <div className="questions">
                    {staticData.items.map((item, index) =>
                        <div className={`item-wrapper${index === Active ? ' active' : ''}`} key={index}>
                            <div className="toggler" onClick={() => setActive(value=>value===index?undefined:index)}>
                                <Typography variant="sub-heading1" className="question-text">{item.question}</Typography>
                                <span className="icon">
                                    {index === Active ?
                                        <FiChevronUp size="16" />
                                        :
                                        <FiChevronDown size="16" />
                                    }
                                </span>
                            </div>
                            <Typography component="div" variant="body3" className="answer-text">{item.answer}</Typography>
                        </div>
                    )}
                </div>
            </Container>
        </div>
    )
}

export default FAQ;