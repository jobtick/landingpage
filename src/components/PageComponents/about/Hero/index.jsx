import React from "react";
import Typography from "components/Typography";
import { Col, Container } from "reactstrap";
import classes from "./style.module.scss";
import HeaderTags from "components/HeaderTags";
import { BASE_URL } from "constants/config";

function Hero() {
  const [Items, setItems] = React.useState([]);
  const getItems = () => {
    setItems([
      {
        number: "+15",
        title: "Team members",
      },
      {
        number: "2020",
        title: "Year founded",
      },
      {
        number: "+1000",
        title: "Posters",
      },
      {
        number: "+1000",
        title: "Tickers",
      },
    ]);
  };
  React.useEffect(getItems, []);
  return (
    <>
      <HeaderTags
        title="Jobtick | About us"
        description="It's where the job is done"
        image={BASE_URL.substr(0, BASE_URL.length - 1) + "/logo192.png"}
        url="/About"
      />
      <div className={classes.hero}>
        <Container>
          <Typography component="h2" variant="heading0" className="header">
            Who we are and what we do
          </Typography>
          <div className="content">
            <Col lg={9} xs={12} className="description">
              <Typography component="h3" variant="heading3">
                The product
              </Typography>
              <Typography component="p" variant="body1">
                Jobtick provides instant home services across Australia by
                enabling millions of Australian households to be connected to
                professional home service and vice versa whether the job is a
                home renovation, a tiny repair or garden maintenance, Jobtick
                Professional Ticker across Australia ready to get the job done
                for you. We aims to provide unmatched online performances. We
                are committed to providing you with value-added, timely
                solutions to all your home care needs, from repairs and
                maintenance to finding a babysitter or personal trainer.
              </Typography>
            </Col>
            {/* <Col lg={6} xs={12} className='statistics'>
                            {Items.map((item, index) =>
                                <Col xs={6} key={index}>
                                    <div>
                                        <Typography variant='heading0'>{item.number}</Typography>
                                        <Typography variant='body1'>{item.title}</Typography>
                                    </div>
                                </Col>
                            )}
                        </Col> */}
          </div>
        </Container>
      </div>
    </>
  );
}
export default Hero;
