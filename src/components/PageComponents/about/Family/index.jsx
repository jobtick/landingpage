import Typography from "components/Typography";
import React, { useRef } from "react";
import { FiChevronDown, FiChevronUp } from "react-icons/fi";
import { Button, Col, Container } from "reactstrap";
import classes from "./style.module.scss";
function Family({ staticData }) {
  const [ShowTeams, setShowTeams] = React.useState(false);
  const [TeamsHeight, setTeamsHeight] = React.useState(0);
  const teamsRef = React.useRef();
  React.useEffect(() => {
    setTeamsHeight(staticData.length * 300);
  }, [staticData]);
  //Persons.filter(persopn=>person.role === 'Director').map
  return (
    <div className={classes.family}>
      <Container>
        <Typography component="h2" variant="heading0">
          People
        </Typography>
        <div className="leaders">
          {staticData
            .filter((person) =>
              ["Director", "Chairwoman", "Chief Executive Officer"].includes(
                person.role
              )
            )
            .map((person, index) => (
              <Col lg={4} xs={12} key={index}>
                <img src={person.image} />
                <div className="description">
                  <Typography component="h4" variant="sub-heading1">
                    {person.name}
                  </Typography>
                  <Typography variant="body2">{person.role}</Typography>
                  <Typography component="p" variant="body3">
                    {person.description}
                  </Typography>
                </div>
              </Col>
            ))}
        </div>
        {/*<Button color="link" onClick={() => setShowTeams(val => !val)}>
                    <Typography variant='heading4'>Meet our team members</Typography>
                    {ShowTeams ?
                        <FiChevronUp />
                        :
                        <FiChevronDown />
                    }
                </Button>
                <div className={'teams leaders' + (ShowTeams ? ' show' : '')} ref={teamsRef} style={ShowTeams? {maxHeight:TeamsHeight+'px'}:undefined} >
                    {staticData.filter(person => !['Director', 'Chairwoman', 'Chief Executive Officer'].includes(person.role)).map((person, index) =>
                        <Col lg={3} sm={6} xs={12}>
                            <img src={person.image} />
                            <div className='description'>
                                <Typography component='h4' variant='sub-heading1'>{person.name}</Typography>
                                <Typography variant='body2'>{person.role}</Typography>
                            </div>
                        </Col>
                    )}
                </div>*/}
        <div></div>
      </Container>
    </div>
  );
}
export default Family;
