import Typography from "components/Typography";
import React from "react";
import { Container } from "reactstrap";
import classes from "./style.module.scss";
function Vision() {
  return (
    <div className={classes.vision}>
      <Container>
        <Typography component="h3" variant="heading3">
          Our vision
        </Typography>
        <Typography component="p" variant="body1">
          Create a simpler and easier way to create, and fulfil jobs and tasks
          to improve people's lives.
        </Typography>
      </Container>
    </div>
  );
}
export default Vision;
