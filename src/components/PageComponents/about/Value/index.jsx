import Typography from 'components/Typography';
import React from 'react'
import { Col, Container } from 'reactstrap'
import classes from './style.module.scss'

function Value({staticData}) {
    const [Items, setItems] = React.useState([]);
    const getItems = () => {
        setItems([
            {
                image: '/vector/about/reliability.svg',
                title: 'Reliability',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
            },
            {
                image: '/vector/about/efficiency.svg',
                title: 'Efficiency',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
            },
            {
                image: '/vector/about/innovation.svg',
                title: 'innovation',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
            },
            {
                image: '/vector/about/community.svg',
                title: 'Community Core',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
            },
        ])
    }
    React.useEffect(getItems, []);
    return (
        <div className={classes.value}>
            <Container>
                <div className='header'>
                    <Typography component='h3' variant='heading3'>{staticData.title}</Typography>
                    <Typography component='p' variant='body1'>
                        {staticData.subTitle}
                     </Typography>
                </div>
                <div className='items'>
                    {staticData.items.map((item, index) =>
                        <Col lg={3} md={6} key={index}>
                                <img src={item.image} />
                            <div className='content'>
                                <Typography component='h3' variant='sub-heading1'>{item.title}</Typography>
                                <Typography component='span' variant='body3'>{item.description}</Typography>
                            </div>
                        </Col>
                    )}
                </div>
            </Container>

        </div>
    );
}
export default Value;