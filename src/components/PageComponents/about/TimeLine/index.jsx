import Typography from 'components/Typography';
import React from 'react'
import { Container } from 'reactstrap'
import classes from './style.module.scss'
function TimeLine({staticData}) {

    return (
        <div className={classes['date-line']}>
            <Container>
                {staticData.map((item, index) =>
                    <>
                        <div className='item' key={index}>
                            <Typography variant='heading4' className='year'>{item.value}</Typography>
                            <div className='circle'>
                                <span />
                            </div>
                            <Typography variant='body3' classNamec='title'>{item.label}</Typography>
                        </div>
                        <div className='item responsive' key={index}>
                            <div className='circle'>
                                <span />
                            </div>
                            <div className='detail'>
                                <Typography variant='heading4' className='year'>{item.value}</Typography>
                                <Typography variant='body3' classNamec='title'>{item.label}</Typography>
                            </div>
                        </div>
                    </>
                )}
            </Container>
        </div>
    );
}
export default TimeLine;