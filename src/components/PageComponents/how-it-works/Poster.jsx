import React from 'react'
import { Row, Col } from 'reactstrap'
import Lottie from 'react-lottie'


const items = [
    {
        title: 'Post the Job need to get it done!',
        content: 'Posting your Job on Jobtick is just the start of the happy days; set up your account with an email address and click on the post a job button to get started.',
        lottie: require('assets/vector/animations/slide1.json')
    },
    {
        title: 'The best offers will be made on your job by Tickers',
        content: 'Review the offers and select your preferred Ticker to complete the Job. It doesn’t hurt to ask a few questions before accepting the offer. Jobtick provides you and the Ticker resources to connect and get the job done.',
        lottie: require('assets/vector/animations/slide5.json')
    },
    {
        title: 'Select the best Ticker to get your jobs done',
        content: 'Filter the offers, and select your preferred Ticker to complete the Job. Jobtick gives you and the Ticker resources to connect and get the Job done.',
        lottie: require('assets/vector/animations/slide2.json')
    },
    {
        title: 'Great! Job is done! Enjoy Jobtick!',
        content: 'The Ticker gets pay and recognition when you are satisfied with his performances; other posters will trust your positive reviews, so don\'t hesitate to rate his/her performance.',
        lottie: require('assets/vector/animations/slide3.json')
    }
]

export default function Poster() {

    return (
        <div className="content">
            <div className="numbers">
                <ul className="ulNumbers">
                    <li className="quarter">
                        <span className="borders" />
                        <span className="label">1</span>
                    </li>
                    <span></span>
                    <li className="half">
                        <span className="borders" />
                        <span className="label">2</span>
                    </li>
                    <span></span>
                    <li className="quarter3">
                        <span className="borders" />
                        <span className="label">3</span>
                    </li>
                    <span></span>
                    <li>
                        <span className="borders" />
                        <span className="label">4</span>
                    </li>
                </ul>
                <ul className="ulText">
                    {items.map((item, index) =>
                        <li key={index}>{item.title}</li>
                    )}
                </ul>
            </div>

            <div className="content">

                {items.map((item, index) =>
                    <>
                        {index % 2 === 0 ?
                            <Row className="box" key={index}>
                                <Col md="5">
                                    <Lottie className="animation"
                                        width="auto"
                                        height="400px"
                                        options={
                                            {
                                                loop: true,
                                                autoplay: true,
                                                animationData: item.lottie,
                                            }
                                        }
                                        isClickToPauseDisabled={true}
                                    />
                                </Col>
                                <Col md="7" className="texts">
                                    <h3>{item.title}</h3>
                                    <p>{item.content}</p>
                                </Col>
                            </Row>

                            :
                            <Row className="box">
                                <Col md="7" className="texts">
                                    <h3>{item.title}</h3>
                                    <p>{item.content}</p>
                                </Col>
                                <Col md="5" className="left">
                                    <Lottie className="animation"
                                        width="auto"
                                        height="400px"
                                        options={
                                            {
                                                loop: true,
                                                autoplay: true,
                                                animationData: item.lottie,
                                            }
                                        }
                                        isClickToPauseDisabled={true}
                                    />
                                </Col>

                            </Row>
                        }
                    </>
                )}

            </div>
        </div>
    )
}