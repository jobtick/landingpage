import React from 'react'
import { Row, Col } from 'reactstrap'
import Lottie from 'react-lottie'



const items = [
    {
        title: 'Explore Jobs that interest you',
        content: 'At Jobtick, you will experience something that speaks to your heart. An excellent way of serving your community while exploring your chances of matching your skills and experiences with the available Jobs at Jobtick Platforms.',
        lottie: require('assets/vector/animations/slide4.json')
    },
    {
        title: 'Make an offer that will get you the job',
        content: 'Don\'t feel pressured to send an offer to a Job immediately; it\'s advisable to thank the poster for their job and ask questions if you need to before sending your bid. Be honest about your service fee and time scheduled. Put the poster mind at ease by showing why the poster should select you to get the job done.',
        lottie: require('assets/vector/animations/slide5.json')
    },
    {
        title: 'Jobtick will notify you once the poster accepts your offer',
        content: 'For your offer to be accepted, you must have all the requirements provided; the notification will include specific instructions and details of the job; send the poster a brief message. Introduce yourself, express your gratitude, and any other relevant information.',
        lottie: require('assets/vector/animations/slide2.json')
    },
    {
        title: 'Do your best and enjoy your money!',
        content: 'Keeping the customers happy is one of the most important factors for Tickers\' success. It refers to the attitude and feeling posters have about you and your work.',
        lottie: require('assets/vector/animations/slide6-2.json')
    }
]

export default function Ticker() {

    return (
        <div className="content">
            <div className="numbers">
                <ul className="ulNumbers">
                    <li className="quarter">
                        <span className="borders" />
                        <span className="label">1</span>
                    </li>
                    <span></span>
                    <li className="half">
                        <span className="borders" />
                        <span className="label">2</span>
                    </li>
                    <span></span>
                    <li className="quarter3">
                        <span className="borders" />
                        <span className="label">3</span>
                    </li>
                    <span></span>
                    <li>
                        <span className="borders" />
                        <span className="label">4</span>
                    </li>
                </ul>
                <ul className="ulText">
                    {items.map((item, index) =>
                        <li key={index}>{item.title}</li>
                    )}
                </ul>
            </div>

            <div className="content">

                {items.map((item, index) =>
                    <>
                        {index % 2 === 0 ?
                            <Row className="box" key={index}>
                                <Col md="5">
                                    <Lottie className="animation"
                                        width="auto"
                                        height="400px"
                                        options={
                                            {
                                                loop: true,
                                                autoplay: true,
                                                animationData: item.lottie,
                                            }
                                        }
                                        isClickToPauseDisabled={true}
                                    />
                                </Col>
                                <Col md="7" className="texts">
                                    <h3>{item.title}</h3>
                                    <p>{item.content}</p>
                                </Col>
                            </Row>

                            :
                            <Row className="box">
                                <Col md="7" className="texts">
                                    <h3>{item.title}</h3>
                                    <p>{item.content}</p>
                                </Col>
                                <Col md="5" className="left">
                                    <Lottie className="animation"
                                        width="auto"
                                        height="400px"
                                        options={
                                            {
                                                loop: true,
                                                autoplay: true,
                                                animationData: item.lottie,
                                            }
                                        }
                                        isClickToPauseDisabled={true}
                                    />
                                </Col>

                            </Row>
                        }
                    </>
                )}
            </div>
        </div>
    )
}