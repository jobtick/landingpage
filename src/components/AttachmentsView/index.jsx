import React from 'react'
import { FiImage, FiVideo, FiPlayCircle, FiX, FiZoomIn } from 'react-icons/fi'
import { getMimeType } from 'utils'
import MediaViewer from '../MediaViewer'

export default function AttachmentsView({ className, style, data, onRemove, onClick, itemStyle, showFileIcon, notRemovable }) {
    return (
        <div className={`attachment-view clearfix${className ? ' ' + className : ''}`} style={style || {}}>
            {data.map((item, index) => {
                var mimeType = getMimeType(item.mime);
                return (
                    <div key={index} title={item.file_name} className={`attachment-item ${["image", "video"].indexOf(mimeType) !== -1 ? "image-at" : "other-at"}`} style={itemStyle ? itemStyle : {}}>
                        <div className="attachment-item-container">
                            {!notRemovable && <span className="remove-attachment" onClick={() => onRemove(item)}><FiX /></span>}
                            {["image", "video"].indexOf(mimeType) !== -1 ?
                                <div className="attachment-preview" style={{ backgroundImage: `url(${item.url})` }}>
                                    {showFileIcon &&
                                        <div className="file-type">
                                            {mimeType === "image" && <FiImage className="media-icon" />}
                                            {mimeType === "video" && <FiVideo className="media-icon" />}
                                        </div>
                                    }
                                </div>
                                :
                                <>
                                    <span className="attachment-name">{item.file_name}</span>
                                </>
                            }
                        </div>
                    </div>
                )
            })}
        </div>
    )
}

export function AttachmentPreview(props) {
    const [CurrentPreview, setCurrentPreview] = React.useState()
    const [PreviewModal, setPreviewModal] = React.useState(false)

    const handleModal = React.useCallback(
        (item) => {
            if (!props.preview)
                return undefined
            setCurrentPreview(item)
            toggleModal()
        },
        [props.preview],
    )

    const toggleModal = () => {
        setPreviewModal(value => { return !value })
    }

    const renderAttachment = (item, index) => {
        const { downloadable } = props;
        const { itemStyle } = props;
        const mimeType = getMimeType(item.mime);
        return (
            <div key={index} title={item.file_name} className={`attachment-item preview-item ${mimeType === "image" ? "image-at" : mimeType === "video" ? "show-nav" : "other-at"}`} style={itemStyle ? itemStyle : {}}>
                <div className="attachment-item-container">
                    {mimeType === "image" ?
                        <div onClick={() => handleModal(item)} className="attachment-preview app-flex-both-center" style={{ backgroundImage: `url(${item.url})` }}>
                            <FiZoomIn />
                        </div>
                        :
                        mimeType === "video" ?
                            <div onClick={() => handleModal(item)} className="attachment-preview app-flex-both-center" style={{ backgroundImage: `url(${item.thumb_url})` }}>
                                <FiPlayCircle />
                            </div>
                            :
                            <>
                                {downloadable ?
                                    <a target="_blank"
                                        rel="noopener noreferrer"
                                        className="app-flex-center"
                                        href={item.url}
                                        download={item.file_name} >
                                        <span className="attachment-name">{item.file_name}</span>
                                    </a>
                                    :
                                    <span className="attachment-name">{item.file_name}</span>
                                }
                            </>
                    }
                </div>
            </div>
        )
    }

    const { attachments } = props;
    const { containerStyle } = props;
    return (
        <div className="attachment-view clearfix" style={containerStyle ? containerStyle : {}}>
            {attachments.map((item, index) => {
                return renderAttachment(item, index)
            })}
            {PreviewModal && CurrentPreview &&
                <MediaViewer
                    media={CurrentPreview}
                    download={true}
                    onClose={toggleModal}
                />
            }
        </div>
    )
}
