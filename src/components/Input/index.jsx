import React from "react";
import { FiChevronDown, FiChevronUp, FiEye, FiEyeOff } from "react-icons/fi";
import Typography from "components/Typography";

const Input = ({
  afterIcon,
  beforeIcon,
  className,
  children,
  checked,
  disabled,
  error,
  onChange,
  onBlur,
  onFocus,
  onAddClick,
  onClick,
  label,
  name,
  max,
  min,
  type,
  value,
  ref,
  items,
  ...inputProps
}) => {
  const [ShowPassword, setShowPassword] = React.useState(false);
  const [DropdownOpen, setDropdownOpen] = React.useState(false);
  const [Focused, setFocused] = React.useState(false);

  const inputWrapper = React.useRef();
  const dropdownRef = React.useRef();

  const handleFocus = (e) => {
    if (!disabled) {
      setFocused(true);
      if (onFocus) onFocus(e);
    }
  };

  const handleBlur = (e) => {
    if (!disabled) {
      setFocused(false);
      if (onBlur) onBlur(e);
    }
  };

  React.useEffect(() => {
    console.log(items);
    const onBodyClickFunc = (e) => {
      if (
        !dropdownRef.current?.contains(e.target) ||
        e.target === dropdownRef.current
      )
        setDropdownOpen(false);
    };
    const onKeyDown = (e) => {
      if (e.key !== "Tab") e.preventDefault();
      const activeIndex = items.findIndex((item) => item.value === value);
      if (e.key === "ArrowDown")
        onChange(
          items[
            activeIndex !== -1 && activeIndex !== items.length - 1
              ? activeIndex + 1
              : 0
          ].value
        );

      if (e.key === "ArrowUp")
        onChange(
          items[
            activeIndex !== -1 && activeIndex !== 0
              ? activeIndex - 1
              : items.length - 1
          ].value
        );

      if (e.key === "Enter" || e.key === "Tab") setDropdownOpen(false);
    };
    if (DropdownOpen) {
      document.body.addEventListener("click", onBodyClickFunc);
      document.body.addEventListener("keydown", onKeyDown);
    } else {
      document.body.removeEventListener("click", onBodyClickFunc);
      document.body.removeEventListener("keydown", onKeyDown);
    }
    return () => {
      document.body.removeEventListener("click", onBodyClickFunc);
      document.body.removeEventListener("keydown", onKeyDown);
    };
  }, [DropdownOpen, value]);

  const renderInput = () => {
    switch (type) {
      case "select":
        return (
          <>
            <div
              className="dropdown-container"
              onClick={() => setDropdownOpen((value) => !value)}
            >
              <span className="label">
                {items.find((item) => item.value === value)?.label ||
                  inputProps?.placeholder ||
                  "Select..."}
              </span>
              <span className="dropdown-icon">
                {DropdownOpen ? <FiChevronUp /> : <FiChevronDown />}
              </span>
            </div>
            <div
              className={`dropdown-body${DropdownOpen ? " open" : ""}`}
              ref={dropdownRef}
            >
              {items.map((item, index) => {
                return (
                  <div
                    className={`dropdown-item${
                      item.value === value ? " active" : ""
                    }`}
                    key={index}
                    onClick={() => {
                      if (onChange) onChange(item.value);
                      setDropdownOpen(false);
                    }}
                  >
                    <span className="label">{item.label}</span>
                  </div>
                );
              })}
            </div>
          </>
        );
      case "textarea":
        return (
          <textarea
            id={name}
            name={name}
            type={type}
            onChange={!disabled && onChange ? onChange : () => {}}
            onBlur={handleBlur}
            onFocus={handleFocus}
            ref={ref}
            value={value || ""}
            maxLength={max}
            {...inputProps}
          />
        );

      case "radio":
        return (
          <>
            <input
              id={value}
              name={name}
              type={type}
              onChange={!disabled && onChange ? onChange : () => {}}
              onBlur={handleBlur}
              onFocus={handleFocus}
              checked={checked || false}
              ref={ref}
              value={value}
              {...inputProps}
            />
            <Typography
              component="label"
              variant="sub-heading2"
              htmlFor={value}
            >
              {label}
            </Typography>
          </>
        );

      case "checkbox":
        return (
          <>
            <input
              id={name}
              name={name}
              type={type}
              onChange={!disabled && onChange ? onChange : undefined}
              onBlur={handleBlur}
              onFocus={handleFocus}
              checked={checked || false}
              ref={ref}
              value={value}
              {...inputProps}
            />
            <Typography component="label" variant="sub-heading2" htmlFor={name}>
              {label}
            </Typography>
          </>
        );
      case "switch":
        return (
          <>
            <Typography
              className="mb-0"
              component="label"
              variant="sub-heading2"
              htmlFor={name}
            >
              {label}
            </Typography>
            <input
              id={name}
              name={name}
              className="no-checkbox"
              type="checkbox"
              onChange={!disabled && onChange ? onChange : undefined}
              onBlur={handleBlur}
              onFocus={handleFocus}
              checked={checked || false}
              ref={ref}
              value={value}
              {...inputProps}
            />
            <label
              className={`handle${checked ? " active" : ""}`}
              htmlFor={name}
            ></label>
          </>
        );
      case "password":
        return (
          <>
            <input
              id={name}
              name={name}
              type={ShowPassword ? "text" : "password"}
              onChange={!disabled && onChange ? onChange : () => {}}
              onBlur={handleBlur}
              onFocus={handleFocus}
              value={value || ""}
              ref={ref}
              {...inputProps}
            />
            <span
              className="icon-after toggle-password"
              onClick={() => setShowPassword((value) => !value)}
            >
              {ShowPassword ? <FiEye /> : <FiEyeOff />}
            </span>
          </>
        );
      default:
        return (
          <input
            id={name}
            name={name}
            type={type || "text"}
            onChange={!disabled && onChange ? onChange : () => {}}
            onBlur={handleBlur}
            onFocus={handleFocus}
            maxLength={max || undefined}
            value={value || ""}
            ref={ref}
            {...inputProps}
          />
        );
    }
  };
  return (
    <>
      <div
        className={[
          "input-wrapper",
          className,
          disabled ? "disabled" : "",
          error ? "error" : "",
          !(label || max || min) ? "no-header" : "",
          checked ? "checked" : "",
          Focused ? "focused" : "",
          value ? "non-empty" : "",
          ["checkbox", "radio", "switch"].includes(type)
            ? "no-border no-header"
            : "",
          ["switch"].includes(type) ? "no-background" : "",
        ]
          .join(" ")
          .replace(/[ ]+/g, " ")}
        ref={inputWrapper}
      >
        {(label || max || min) &&
          !["checkbox", "radio", "switch"].includes(type) && (
            <div className="input-header">
              {label && (
                <Typography
                  component="label"
                  variant={value || Focused ? "sub-heading3" : "sub-heading2"}
                  htmlFor={name}
                >
                  {label}
                </Typography>
              )}

              {(max || min) && (
                <Typography
                  component="span"
                  variant="caption2"
                  className={`counter${
                    value.length > max || value.trim().length < min
                      ? " disallow"
                      : " allow"
                  }`}
                >
                  {value.trim().length +
                    " / " +
                    (min && value.trim().length < min ? min + "+" : max || "")}
                </Typography>
              )}
            </div>
          )}
        <div onClick={!disabled ? onClick : undefined}>
          {beforeIcon && <div className="icon-before">{beforeIcon}</div>}
          {children ? children : renderInput()}
          {afterIcon && <div className="icon-after">{afterIcon}</div>}
        </div>
      </div>
      {error && (
        <Typography variant="sub-heading3" className="input-error">
          {error}
        </Typography>
      )}
    </>
  );
};

export default Input;
