import React from 'react'
import { getLevelIconUrl } from 'utils'

const verifiedIcon = '/vector/varified-mark.svg'
const ProfileAvatar = ({ className, style, profile, showLevel, onClick }) => {

    return (
        <div className={`profile-avatar-container${className ? ` ${className}` : ''}`} style={style ? style : {}} onClick={onClick}>
            <div className="profile-avatar" style={{ backgroundImage: `url(${profile.avatar?.url || '/vector/default-user.svg'})` }} />
            {profile.is_verified_account ? <div className="verified_icon"><img src={verifiedIcon} alt="" /></div> : ""}
            {showLevel && <div className="level_icon"><img src={getLevelIconUrl(profile.poster_tier?.id)} alt={"Level " + profile.poster_tier?.id} /></div>}
        </div>
    )
}

export default ProfileAvatar