import React from 'react'
import Typography from 'components/Typography'

export default function NoContent({ icon, label, desc }) {

    return (
        <div className="no-content">
            {icon &&
                <div className="icon">
                    {icon}
                </div>
            }
            {label &&
                <Typography component="span" variant="body3" className="label">{label}</Typography>
            }
            {desc &&
                <Typography component="span" variant="body3" className="desc">{desc}</Typography>
            }
        </div>
    )
}