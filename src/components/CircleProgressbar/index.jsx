import React from 'react'

export default function CircleProgressbar({ className , percent, color }) {

    return (
        <div className={`progress progress${parseInt(percent/25)*25} ${color} ${className}`}>
            <span className="progress-left">
                <span className="progress-bar"></span>
            </span>
            <span className="progress-right">
                <span className="progress-bar"></span>
            </span>
            <div className="progress-value">%{percent}</div>
        </div>
    )
}