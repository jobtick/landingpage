import React from "react";
import Head from "next/head";
import { BASE_URL } from "constants/config";
import { isDevelopment } from "utils";

export default function HeaderTags({
  title,
  description,
  image,
  url,
  children,
  schema,
}) {
  const baseURL = BASE_URL.endsWith("/")
    ? BASE_URL.substr(0, BASE_URL.length - 1)
    : BASE_URL;

  return (
    <Head>
      <title>{title}</title>
      <link rel="icon" href="/favicon.ico" />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={image} />
      <meta property="og:type" content="website" />
      <meta property="og:site_name" content="Jobtick" />
      <meta property="og:url" content={baseURL + url} />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="description" content={description} />
      <meta name="apple-mobile-web-app-capable" content="yes" />
      <meta httpEquiv="x-ua-compatible" content="IE=edge" />
      <link rel="alternate" href={baseURL + url} hrefLang="en-au" />
      <link rel="alternate" href={baseURL + url} hrefLang="x-default" />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
      />
      {!isDevelopment && (
        <>
          <meta
            name="p:domain_verify"
            content="d1bc3efbf1ca7c72c0d31b401d012717"
          />
          <meta
            name="facebook-domain-verification"
            content="4kv23a95p7xm842agfuesorjc996fo"
          />
          /* Google Tag Manager */
          <script
            defer
            dangerouslySetInnerHTML={{
              __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                    })(window,document,'script','dataLayer','GTM-WK73Z5J');`,
            }}
          />
          /* End Google Tag Manager */
          {schema && (
            <script
              type="application/ld+json"
              dangerouslySetInnerHTML={{ __html: JSON.stringify(schema) }}
            />
          )}
        </>
      )}
      {children}
    </Head>
  );
}
