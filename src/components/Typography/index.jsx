import React from 'react'

/*
    Special props
    component : span , p , a, button , ...
    variant : dsp-xl,dsp-lg,dsp-md,dsp-sm,heading,sub-heading,body,caption,heading1,heading2,heading3,heading4,sub-heading1,sub-heading2,sub-heading3,body1,body2,body3,caption1,caption2
    weight : thin , normal , bold , black
    children
*/
export default function Typography({ component='span', variant='body', weight, children, className  , ...outputProps} ) {

    const Output = React.createElement(component, {
        ...outputProps,
        className: `typo${variant ? '-' + variant : '-body'}${weight ? ' typo-weight-' + weight : ''}${className ? ' ' + className : ''}`
    },
        children)

    return Output
}