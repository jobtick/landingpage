import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { FiX, FiDownloadCloud } from 'react-icons/fi'
import { getMimeType } from 'utils'
import { Player } from 'video-react';

export default class MediaViewer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            media: props.media ? props.media : false,
            downloadable: props.download,
            viewer: false
        }
        this._isMounted = false;
        this.timeout = null;
        this.closeViewer = props.onClose;
        this.root = document.getElementById('root') ? document.getElementById('root') : document.getElementsByTagName('body')[0];

        this.toggleViewer = this.toggleViewer.bind(this);
        this.renderVideo = this.renderVideo.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        this.root = document.getElementById('root') ? document.getElementById('root') : document.getElementsByTagName('body')[0];
        this.toggleViewer();
    }

    componentWillUnmount() {
        this._isMounted = false;
        if (this.timeout !== null) clearTimeout(this.timeout);
    }

    componentDidUpdate(prevProps) {
        if (this.props.media !== prevProps.media) this.setState({ media: this.props.media });
    }

    toggleViewer(closeViewer) {
        this.timeout = setTimeout(function () {
            if (this._isMounted) {
                this.setState({ viewer: !this.state.viewer });
                if (closeViewer) this.closeViewer();
            }
        }.bind(this), 250);
    }

    renderVideo() {
        const { media } = this.state;
        return (
            <div className="viewer-video">
                <Player
                    playsInline
                    poster={media.modal_url}
                    src={media.url}
                />
            </div>
        )
    }

    render() {
        const { media, downloadable } = this.state;
        return ReactDOM.createPortal(
            <div id="mMedia-viewer" className={`mMedia-viewer${this.state.viewer ? ' active' : ''}`}>
                <div className="viewer-control app-flex-center">
                    {downloadable && <a className="control control-success mr-2" href={media && media.url} download={media && media.file_name} target="_blank" rel="noopener noreferrer" onClick={this.toggleViewer}><FiDownloadCloud /></a>}
                    <span className="control control-danger" onClick={this.toggleViewer}><FiX /></span>
                </div>
                <div className="viewer-body">
                    {media && getMimeType(media.mime) === "image" &&
                        <div className="viewer-image">
                            <img src={media.modal_url} alt="" />
                        </div>
                    }
                    {media && getMimeType(media.mime) === "video" && this.renderVideo()}
                </div>
            </div>
            , this.root
        );
    }
}
