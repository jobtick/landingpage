import React from "react";
import {
  APPLE_AUTH_CLIENT_ID,
  FACEBOOK_AUTH_CLIENT_ID,
  GOOGLE_AUTH_CLIENT_ID,
} from "constants/config";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import { useGoogleLogin } from "components/react-google-login";
import AppleLogin from "react-apple-login";
import { facebookSignIn, googleSignIn, appleSignIn } from "api/user";
import useIsMounted from "react-is-mounted-hook";
import useToast from "components/Toast/Toast";
import { Button } from "reactstrap";
import Spinner from "components/Spinner";
import { getUrlQuery } from "utils";

export default function ExternalAuth({ onSuccess }) {
  const isMounted = useIsMounted();
  const toast = useToast();

  const { auth, referrer } = getUrlQuery();
  const handleGoogleLogin = (response) => {
    if (response.accessToken) {
      googleSignIn(
        response.accessToken,
        response.profileObj.givenName,
        response.profileObj.familyName,
        auth === "invite" ? referrer : undefined
      )
        .then((payload) => {
          if (isMounted && payload.success) {
            onSuccess(payload.data);
          }
        })
        .catch((reason) => {
          if (isMounted && reason.response) {
            const errorData = reason.response.data
              ? reason.response.data.error
              : {};
            if (errorData?.errors) setErrors(errorData?.errors);
            toast.showMessage(
              "danger",
              errorData?.message || "Something went wrong"
            );
          }
        });
    }
  };
  const { signIn, loaded } = useGoogleLogin({
    clientId: GOOGLE_AUTH_CLIENT_ID,
    onSuccess: handleGoogleLogin,
    onFailure: handleGoogleLogin,
    cookiePolicy: "none",
  });

  const handleFacebookLogin = (response) => {
    if (response.accessToken) {
      facebookSignIn(
        response.accessToken,
        response.name,
        undefined,
        auth === "invite" ? referrer : undefined
      )
        .then((payload) => {
          if (isMounted && payload.success) {
            onSuccess(payload.data);
          }
        })
        .catch((reason) => {
          if (isMounted && reason) {
            const errorData = reason.response.data
              ? reason.response.data.error
              : {};
            if (errorData.errors) setErrors(errorData.errors);
            toast.showMessage(
              "danger",
              errorData.message || "Something went wrong"
            );
          }
        });
    }
  };

  const handleAppleLogin = (response) => {
    if (response.authorization?.id_token) {
      appleSignIn(
        response.authorization.id_token,
        response.user?.name?.firstName || "Jobtick",
        response.user?.name?.lastName || "User",
        auth === "invite" ? referrer : undefined
      )
        .then((payload) => {
          if (isMounted && payload.success) {
            onSuccess(payload.data);
          }
        })
        .catch((reason) => {
          if (isMounted && reason.response) {
            const errorData = reason.response.data
              ? reason.response.data.error
              : {};
            if (errorData.errors) setErrors(errorData.errors);
            toast.showMessage(
              "danger",
              errorData.message || "Something went wrong"
            );
          }
        });
    }
  };

  return (
    <>
      <FacebookLogin
        appId={FACEBOOK_AUTH_CLIENT_ID}
        version="6.0"
        render={(renderProps) => (
          <Button
            className="external-login facebook-login jt-login-action"
            size="lg"
            onClick={renderProps.onClick}
            disabled={renderProps.isDisabled}
            block
          >
            <img src={"/vector/facebook.svg"} alt="Facebook" />
            <span className="label">Continue with Facebook</span>
          </Button>
        )}
        autoLoad={false}
        fields="name,email,picture"
        callback={handleFacebookLogin}
      />
      <Button
        className="external-login google-login jt-login-action"
        size="lg"
        onClick={signIn}
        block
        disabled={!loaded}
      >
        <img src={"/vector/google.svg"} alt="Google" />
        <span className="label">Continue with Google</span>
        {!loaded && <Spinner type="box" />}
      </Button>
      <AppleLogin
        clientId={APPLE_AUTH_CLIENT_ID}
        usePopup={true}
        scope="name email"
        responseType="id_token"
        responseMode="form_post"
        redirectURI={
          location.origin.includes("localhost")
            ? "https://dev.jobtick.com"
            : location.origin
        }
        render={(renderProps) => (
          <Button
            className="external-login apple-login jt-login-action"
            size="lg"
            onClick={renderProps.onClick}
            disabled={renderProps.isDisabled}
            block
          >
            <img src={"/vector/apple.svg"} alt="Apple" />
            <span className="label">Continue with Apple</span>
          </Button>
        )}
        callback={handleAppleLogin}
      />
      <span className="more-method">Or</span>
    </>
  );
}
