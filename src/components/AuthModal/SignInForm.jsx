import React from 'react'
import Spinner from 'components/Spinner'
import { Button, FormGroup } from 'reactstrap'
import { login, editProfile } from 'api/user';
import useIsMounted from 'react-is-mounted-hook';
import Input from 'components/Input'
import VerifyOtpForm from './VerifyOtpForm';
import { APP_BASE_URL } from 'constants/config';
import ForgotPasswordForm from './ForgotPasswordForm';
import { ValidateEmail } from 'utils/typeTools';
import useToast from 'components/Toast/Toast';
import Cookies from 'js-cookie'
import CompleteProfile from './CompleteProfile';
import ExternalAuth from './ExternalAuth';
import { getUrlQuery } from 'utils';

export default function SignInForm({ redirectAfter, onUpdate, setModalTitle, onSignUp, onClose, afterLogin = null, redirect = true }) {
    const isMounted = useIsMounted()
    const toast = useToast()

    const [IsProcessing, setIsProcessing] = React.useState(false)
    const [InputFields, setInputFields] = React.useState({})
    const [Errors, setErrors] = React.useState({})
    // 1 : signin - 2 : verify - 3 : forgot password - 4 : Complete profile
    const [CurrentStep, setCurrentStep] = React.useState(1)
    const [Title, setTitle] = React.useState('')

    const handleChange = (e) => {
        e.persist()
        setInputFields(value => {
            return {
                ...value,
                [e.target.name]: e.target.value
            }
        })

        setErrors(errors => {
            return {
                ...errors,
                [e.target.name]: undefined
            }
        })
    }

    const handleSigInSuccess = (data, message) => {
        if (data.access_token) {
            Cookies.set('token', data.access_token, { domain: window.location.origin.includes('localhost') ? null : '.jobtick.com' })
        }


        if (data.user && !data.user.account_status.basic_info_completed) {
            toast.showMessage('danger', "Please complete your profile")
            setInputFields({})
            setCurrentStep(4)
        }
        else {
            if (onClose)
                onClose()
            if (onUpdate && data.user) {
                onUpdate(true, data.user)
            }
            else {
                toast.showMessage('success', message || "Authenticated")
                if (afterLogin) afterLogin()
                if (redirectAfter) {
                    if(redirect) window.location.href = redirectAfter;
                    return undefined;
                }
                if(redirect) window.location.href = APP_BASE_URL;
                return undefined;
            }
        }
    }
    const handleSigIn = (e) => {
        e.preventDefault();
        e.stopPropagation();
        if (IsProcessing) return undefined;

        let verify = true
        if (InputFields.email && InputFields.email !== '') {
            if (!ValidateEmail(InputFields.email)) {
                setErrors(errors => {
                    return {
                        ...errors,
                        email: ['Email address is not valid']
                    }
                })
                verify = false
            }
        } else {
            setErrors(errors => {
                return {
                    ...errors,
                    email: ['Email address is required']
                }
            })
            verify = false
        }

        if (!InputFields.password && InputFields.password !== '') {
            setErrors(errors => {
                return {
                    ...errors,
                    password: ['Password is required']
                }
            })
            verify = false
        }
        if (!verify)
            return undefined

        setIsProcessing(true)

        const { auth, referrer } = getUrlQuery()
        login(auth === 'invite' ?
            {
                ...InputFields,
                referrer_code: referrer
            }
            :
            InputFields)
            .then(payload => {
                if (isMounted && payload.success) {
                    handleSigInSuccess(payload.data, payload.message)
                }
            })
            .catch(reason => {
                if (isMounted && reason.response) {
                    const errorData = reason.response.data ? reason.response.data.error : {};
                    if (errorData.error_code === 1002) {
                        setCurrentStep(2)
                    }
                    if (errorData.errors) setErrors(errorData.errors);
                    toast.showMessage('danger', errorData.message || "Something went wrong")
                }
            })
            .finally(() => {
                if (isMounted)
                    setIsProcessing(false)
            })
    }


    const handleProfile = (e) => {
        e.preventDefault();
        e.stopPropagation();

        if (IsProcessing)
            return undefined
        let verify = true
        if (!InputFields.fname || InputFields.fname === '') {
            setErrors(errors => {
                return {
                    ...errors,
                    fname: ['First name is requiered']
                }
            })
            verify = false
        }

        if (!InputFields.lname || InputFields.lname === '') {
            setErrors(errors => {
                return {
                    ...errors,
                    lname: ['Last name is requiered']
                }
            })
            verify = false
        }

        if (!InputFields.role_as) {
            setErrors(errors => {
                return {
                    ...errors,
                    role_as: ['Select one']
                }
            })
            verify = false
        }

        if (!InputFields.location) {
            setErrors(errors => {
                return {
                    ...errors,
                    location: ['Suburb is requiered']
                }
            })
            verify = false
        }

        if (!verify)
            return undefined

        setIsProcessing(true)
        editProfile(InputFields)
            .then(payload => {
                if (isMounted && payload.success) {
                    handleSigInSuccess({ user: payload.data }, "Profile completed successfully")
                }
            })
            .catch(reason => {
                if (isMounted && reason.response) {
                    const errorData = reason.response.data ? reason.response.data.error : {};

                    if (errorData.errors) setErrors(errorData.errors);
                    toast.showMessage('danger', errorData.message || "Something went wrong")
                }
            })
            .finally(() => {
                if (isMounted)
                    setIsProcessing(false)
            })
    }

    const onVerifySuccess = () => {
        toast.showMessage('success', "Your email has verified. Please login again")
        setCurrentStep(1)
    }

    const onForgotSuccess = (data) => {
        handleSigInSuccess(data, "Password changed successfully")
    }

    React.useEffect(
        () => {
            const setter = setModalTitle || setTitle
            switch (CurrentStep) {
                case 2:
                    setter('Verification code')
                    break;
                case 3:
                    break;
                case 4:
                    setter('Complete profile')
                    break;
                default:
                    setter('Sign In to Jobtick')
            }
            // eslint-disable-next-line
        }, [CurrentStep])
    return (
        <>
            {!setModalTitle &&
                <h1 className="auth-form-title">{Title}</h1>
            }
            {CurrentStep === 1 &&
                <form onSubmit={handleSigIn} className="auth-form">
                    <ExternalAuth onSuccess={handleSigInSuccess} />
                    <FormGroup>
                        <Input
                            type="email"
                            name="email"
                            label="Email"
                            value={InputFields.email || ''}
                            onChange={handleChange}
                            error={Errors.email && true}
                        />
                        {Errors.email && <span className="input-error">{Errors.email[0]}</span>}
                    </FormGroup>
                    <FormGroup>
                        <Input
                            type="password"
                            name="password"
                            label="Password"
                            value={InputFields.password || ''}
                            onChange={handleChange}
                            error={Errors.password && true}
                        />
                        {Errors.password && <span className="input-error">{Errors.password[0]}</span>}
                    </FormGroup>
                    <Button className="jt-login-action" color="primary" type="submit" size="lg" block>
                        {IsProcessing ?
                            <Spinner type="button" className="mr-3" /> :
                            <span className="label">Continue</span>
                        }
                    </Button>

                    <div className="forgot-password">
                        <Button
                            color="link"
                            onClick={() => setCurrentStep(3)}
                        >
                            Forgot password?
                        </Button>
                    </div>



                    {onSignUp &&
                        <div className="switch-form">
                            <span>Not a member yet? </span>
                            <Button
                                className="jt-signup-link"
                                color="link"
                                onClick={onSignUp}
                            >
                                Join now
                            </Button>
                        </div>
                    }

                </form>
            }
            {CurrentStep === 2 &&
                <VerifyOtpForm
                    email={InputFields.email}
                    onSuccess={onVerifySuccess}
                    onBack={() => { setCurrentStep(1) }}
                />
            }

            {CurrentStep === 3 &&
                <ForgotPasswordForm
                    email={InputFields.email}
                    onSuccess={onForgotSuccess}
                    setTitle={setModalTitle || setTitle}
                />
            }
            {CurrentStep === 4 &&
                <CompleteProfile
                    data={InputFields}
                    onUpdate={setInputFields}
                    errors={Errors}
                    isProcessing={IsProcessing}
                    onSubmit={handleProfile}
                />
            }
        </>
    )
}