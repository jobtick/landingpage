import React from 'react'
import Spinner from 'components/Spinner'
import { FormGroup, Button } from 'reactstrap'
import useIsMounted from 'react-is-mounted-hook';
import { signup, editProfile } from 'api/user'
import Input from 'components/Input';
import VerifyOtpForm from './VerifyOtpForm';
import { APP_BASE_URL } from 'constants/config';
import { ValidateEmail } from 'utils/typeTools';
import useToast from 'components/Toast/Toast';
import Cookies from 'js-cookie';
import CompleteProfile from './CompleteProfile';
import ExternalAuth from './ExternalAuth';
import { getUrlQuery } from 'utils';

export default function SignUpForm({ redirectAfter, onUpdate, setModalTitle, onSignIn, onClose, afterRegister = null, redirect = true }) {
    const isMounted = useIsMounted()
    const toast = useToast()

    const [IsProcessing, setIsProcessing] = React.useState(false)
    const [InputFields, setInputFields] = React.useState({})

    const [Errors, setErrors] = React.useState({})

    // 1 : email password - 2 : verify email - 3 : Complete profile
    const [CurrentStep, setCurrentStep] = React.useState(1)
    const [Title, setTitle] = React.useState('')
    React.useEffect(
        () => {
            if (Errors.email || Errors.password)
                setCurrentStep(1)
        }, [Errors])
    const handleChange = (e) => {
        e.stopPropagation()
        e.preventDefault()
        e.persist()
        setInputFields(inputFields => {
            return {
                ...inputFields,
                [e.target.name]: e.target.value
            }
        })

        setErrors(errors => {
            return {
                ...errors,
                [e.target.name]: undefined
            }
        })

    }

    const handleSignUp = (e) => {
        if (e) {
            e.preventDefault();
            e.stopPropagation();
        }
        if (IsProcessing)
            return undefined

        setIsProcessing(true)

        let verify = true
        if (InputFields.email && InputFields.email !== '') {
            if (!ValidateEmail(InputFields.email)) {
                setErrors(errors => {
                    return {
                        ...errors,
                        email: ['Email address is not valid']
                    }
                })
                verify = false
            }
        } else {
            setErrors(errors => {
                return {
                    ...errors,
                    email: ['Email address is required']
                }
            })
            verify = false
        }

        if (!InputFields.password && InputFields.password !== '') {
            setErrors(errors => {
                return {
                    ...errors,
                    password: ['Password is required']
                }
            })
            verify = false
        }

        if (InputFields.password_confirm && InputFields.password_confirm !== '') {
            if (InputFields.password_confirm !== InputFields.password) {
                setErrors(errors => {
                    return {
                        ...errors,
                        password_confirm: ['Confirm password doesn\'t match with password']
                    }
                })
                verify = false
            }
        } else {
            setErrors(errors => {
                return {
                    ...errors,
                    password_confirm: ['Confirm password is required']
                }
            })
            verify = false
        }


        if (!verify) {
            setIsProcessing(false)
            return undefined
        }

        const { auth, referrer } = getUrlQuery()

        signup(
            auth === 'invite' ?
                {
                    fname: 'Jobtick',
                    lname: 'User',
                    location: 'No location',
                    ...InputFields,
                    referrer_code: referrer
                }
                :
                {
                    fname: 'Jobtick',
                    lname: 'User',
                    location: 'No location',
                    ...InputFields
                }
        )
            .then(payload => {
                if (isMounted && payload.success) {
                    toast.showMessage('success', payload.message || "We sent an varification email. Please check your inbox.")
                    setCurrentStep(2)
                }
            })
            .catch(reason => {
                if (isMounted && reason.response) {
                    const errorData = reason.response.data ? reason.response.data.error : {};
                    if (errorData.errors) setErrors(errorData.errors)
                    toast.showMessage('danger', errorData.message || "Something went wrong")
                }
            })
            .finally(() => {
                if (isMounted)
                    setIsProcessing(false)
            })

    }

    const handleSigInSuccess = (data, message) => {
        if (data.access_token)
            Cookies.set('token', data.access_token, { domain: window.location.origin.includes('localhost') ? null : '.jobtick.com' })

        if (!data.user.account_status.basic_info_completed) {
            toast.showMessage('danger', "Please complete your profile")
            setInputFields({})
            setCurrentStep(3)
        }
        else {
            if (onClose)
                onClose();
            if (onUpdate) {
                onUpdate(true, data)
            }
            else {
                toast.showMessage('success', message || "Authenticated")
                if (afterRegister) afterRegister()
                setTimeout(() => {
                    if (redirectAfter) {
                        if(redirect) window.location.href = redirectAfter;
                        return undefined;
                    }
                    if(redirect) window.location.href = APP_BASE_URL;
                    return undefined;
                }, 500)
            }
        }
    }

    const handleProfile = (e) => {
        e.preventDefault();
        e.stopPropagation();

        if (IsProcessing)
            return undefined
        let verify = true
        if (!InputFields.fname || InputFields.fname === '') {
            setErrors(errors => {
                return {
                    ...errors,
                    fname: ['First name is requiered']
                }
            })
            verify = false
        }

        if (!InputFields.lname || InputFields.lname === '') {
            setErrors(errors => {
                return {
                    ...errors,
                    lname: ['Last name is requiered']
                }
            })
            verify = false
        }

        if (!InputFields.role_as) {
            setErrors(errors => {
                return {
                    ...errors,
                    role_as: ['Select one']
                }
            })
            verify = false
        }

        if (!InputFields.location) {
            setErrors(errors => {
                return {
                    ...errors,
                    location: ['Suburb is requiered']
                }
            })
            verify = false
        }

        if (!verify)
            return undefined

        setIsProcessing(true)
        editProfile(InputFields)
            .then(payload => {
                if (isMounted && payload.success) {
                    handleSigInSuccess({ user: payload.data }, "Profile completed successfully")
                }
            })
            .catch(reason => {
                if (isMounted && reason.response) {
                    const errorData = reason.response.data ? reason.response.data.error : {};

                    if (errorData.errors) setErrors(errorData.errors);
                    toast.showMessage('danger', errorData.message || "Something went wrong")
                }
            })
            .finally(() => {
                if (isMounted)
                    setIsProcessing(false)
            })
    }

    const onVerifySuccess = (data) => {
        if (data?.access_token) {
            handleSigInSuccess(data, "Your email has verified")
        }
        else {
            onSignIn()
            toast.showMessage('success', "You can login with your email and password now")
        }
    }

    React.useEffect(
        () => {
            const setter = setModalTitle || setTitle
            switch (CurrentStep) {
                case 2:
                    setter('Confirm Email')
                    break;
                case 3:
                    setter('Complete profile')
                    break;
                default:
                    setter('Join Jobtick')
            }
            // eslint-disable-next-line
        }, [CurrentStep])

    return (
        <>
            {!setModalTitle &&
                <h1 className="auth-form-title">{Title}</h1>
            }
            {CurrentStep === 1 &&
                <form onSubmit={handleSignUp} className="auth-form">
                    <ExternalAuth onSuccess={handleSigInSuccess} />
                    <FormGroup>
                        <Input
                            type="email"
                            name="email"
                            label="Email"
                            value={InputFields.email || ''}
                            onChange={handleChange}
                            error={Errors.email && true}
                        />
                        {Errors.email && <span className="input-error">{Errors.email[0]}</span>}
                    </FormGroup>

                    <FormGroup>
                        <Input
                            type="password"
                            name="password"
                            label="Password"
                            value={InputFields.password || ''}
                            onChange={handleChange}
                            error={Errors.password && true}
                        />
                        {Errors.password && <span className="input-error">{Errors.password[0]}</span>}
                    </FormGroup>

                    <FormGroup>
                        <Input
                            type="password"
                            name="password_confirm"
                            label="Confim password"
                            value={InputFields.password_confirm || ''}
                            onChange={handleChange}
                            error={Errors.password_confirm && true}
                        />
                        {Errors.password_confirm && <span className="input-error">{Errors.password_confirm[0]}</span>}
                    </FormGroup>

                    <Button color="primary" className="jt-signup-gostep2" type="submit" size="lg" block>
                        {IsProcessing ?
                            <Spinner type="button" className="mr-3" style={{ borderTopColor: "#fff" }} /> :
                            <span className="label">Continue</span>
                        }
                    </Button>
                    {onSignIn &&
                        <div className="switch-form" >
                            <span>Already a member?</span>
                            <Button
                                className="jt-login-link"
                                color="link"
                                onClick={onSignIn}
                            >
                                Sign In
                            </Button>
                        </div>
                    }
                </form>
            }
            {CurrentStep === 2 &&
                <VerifyOtpForm
                    email={InputFields.email}
                    onSuccess={onVerifySuccess}
                    onBack={() => setCurrentStep(2)}
                    hasSentFirst
                />
            }
            {CurrentStep === 3 &&
                <CompleteProfile
                    data={InputFields}
                    onUpdate={setInputFields}
                    errors={Errors}
                    isProcessing={IsProcessing}
                    onSubmit={handleProfile}
                />
            }

        </>
    )
}