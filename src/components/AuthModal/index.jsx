import React from 'react'
import Modal from 'components/Modal';
import SignInForm from './SignInForm';
import SignUpForm from './SignUpForm';

export default function AuthModal({ open, onClose, onUpdate, redirectAfter, SignUp, afterLogin = null, redirect = true }) {

    // 0 : signin - 1 : signup
    const [FormSwitch, setFormSwitch] = React.useState(SignUp ? 1 : 0)
    const [Title, setTitle] = React.useState('')

    React.useEffect(() => {
        if (SignUp)
            setFormSwitch(1)
        else
            setFormSwitch(0)
    }, [SignUp])

    return (
        <Modal
            className="auth-modal"
            open={open}
            onClose={onClose}
            title={Title}>
            {FormSwitch === 0 &&
                <SignInForm
                    onUpdate={onUpdate}
                    redirectAfter={redirectAfter}
                    onSignUp={() => setFormSwitch(1)}
                    setModalTitle={setTitle}
                    onClose={onClose}
                    afterLogin={afterLogin}
                    redirect={redirect}
                />
            }

            {FormSwitch === 1 &&
                <SignUpForm
                    onUpdate={onUpdate}
                    redirectAfter={redirectAfter}
                    onSignIn={() => setFormSwitch(0)}
                    setModalTitle={setTitle}
                    onClose={onClose}
                    afterRegister={afterLogin}
                    redirect={redirect}
                />
            }
        </Modal>
    )
}