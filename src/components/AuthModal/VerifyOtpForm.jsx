import React from 'react'
import { requestForgotPassword, requestSendEmailOtp, verifyEmailOtp } from 'api/user'
import Input from 'components/Input'
import Spinner from 'components/Spinner'
import useIsMounted from 'react-is-mounted-hook'
import { Button, Col } from 'reactstrap'
import useTimer from 'components/Timer'
import useToast from 'components/Toast/Toast'

const EXPIRE_TIME = 120

export default function VerifyOtpForm({ email, onSuccess, onBack, hasSentFirst, forgotPassword, buttonLabel }) {
    const isMounted = useIsMounted()
    const timer = useTimer()
    const toast = useToast()

    const [IsRequesting, setIsRequesting] = React.useState(false)
    const [IsSubmiting, setIsSubmiting] = React.useState(false)
    const [RequestSent, setRequestSent] = React.useState(false)
    const [VerificationCode, setVerificationCode] = React.useState('')


    const handleChange = (e) => { if (e.target.value.length < 7) setVerificationCode(parseInt(e.target.value) || "") }

    const startCountdown = React.useCallback(
        () => {
            timer.countdown(EXPIRE_TIME)
            setRequestSent(true)
            setTimeout(() => {
                if (isMounted)
                    setRequestSent(false)
            }, EXPIRE_TIME * 1000)
        },
        [timer, isMounted],
    )

    const requestCode = React.useCallback(
        () => {
            if (IsRequesting)
                return undefined

            setIsRequesting(true)
            const request = forgotPassword ? requestForgotPassword : requestSendEmailOtp
            request(email || 'it.vahidtaheri@gmail.com')
                .then(payload => {
                    if (isMounted && payload.success) {
                        startCountdown()

                        if (payload.message)
                            toast.showMessage('success', payload.message)
                    }
                })
                .catch(reason => {
                    if (isMounted && reason.response) {
                        const errorData = reason.response.data ? reason.response.data.error : {};
                        toast.showMessage('danger', errorData.message || "Something went wrong")
                    }
                })
                .finally(() => {
                    if (isMounted)
                        setIsRequesting(false)
                })
        },
        [IsRequesting, isMounted, startCountdown, email, forgotPassword],
    )

    React.useEffect(
        () => {
            if (hasSentFirst)
                startCountdown()
            else
                requestCode()
            // eslint-disable-next-line
        }, [])


    const handleSubmit = React.useCallback(
        (e) => {
            e.preventDefault()
            e.stopPropagation()

            if (IsSubmiting)
                return undefined

            setIsSubmiting(true)
            verifyEmailOtp(email, VerificationCode, forgotPassword)
                .then(payload => {
                    if (isMounted && payload.success)
                        if (onSuccess)
                            onSuccess(forgotPassword ? VerificationCode : payload.data)
                        else
                            toast.showMessage('success', "Your email has verified")
                })
                .catch(reason => {
                    if (isMounted && reason.response) {
                        const errorData = reason.response.data ? reason.response.data.error : {};
                        toast.showMessage('danger', errorData.message || "Something went wrong")
                    }
                })
                .finally(() => {
                    if (isMounted)
                        setIsSubmiting(false)
                })
        },
        [IsSubmiting, email, VerificationCode, isMounted, onSuccess, forgotPassword],
    )

    return (
        <form onSubmit={handleSubmit} className="auth-form">
            <span className="verify-desc">
                Enter verification code sent to<br />
                {email}
            </span>
            <Input
                type="text"
                name="otp"
                label="Email verification code"
                placeholder="6 digit code"
                onChange={handleChange}
                value={VerificationCode}
            />
            <div className="otp-timer-box">
                <span>{timer.output}</span>
                <Button color="link" size="sm" onClick={requestCode} disabled={RequestSent}>
                    {IsRequesting ? <Spinner type="button" className="mr-3" /> : "Resend code"}
                </Button>
            </div>

            <div className="actions">
                <Col xs={4}>
                    <Button outline color="secondary" size="lg" block onClick={onBack} className="back">
                        <span className="label">Back</span>
                    </Button>
                </Col>

                <Col xs={8}>
                    <Button color="primary" type="submit" size="lg" block disabled={VerificationCode.toString().length < 6}>
                        {IsSubmiting && <Spinner type="button" className="mr-3" />}
                        {buttonLabel || 'Finish'}
                    </Button>
                </Col>
            </div>
        </form>
    )
}