import React from 'react'
import Input from 'components/Input'
import { Col, FormGroup, Button } from 'reactstrap'
import LocationInput from 'components/LocationInput'
import { formatPlaceName } from 'utils/typeTools'
import Spinner from 'components/Spinner'

export default function CompleteProfile({ onSubmit, data, onUpdate, errors, isProcessing, onBack }) {
    const [Errors, setErrors] = React.useState(errors || {})

    React.useEffect(() => {
        setErrors(errors)
    }, [errors])

    const handleChange = (e) => {
        e.stopPropagation()
        e.preventDefault()
        e.persist()
        onUpdate({
            ...data,
            [e.target.name]: e.target.value
        })

        setErrors(errors => {
            return {
                ...errors,
                [e.target.name]: undefined
            }
        })

    }

    const handleRole = (e) => {
        e.persist()
        if (e.target.checked) {
            onUpdate({
                ...data,
                role_as: e.target.value
            })
        }
        setErrors(errors => {
            return {
                ...errors,
                role_as: undefined
            }
        })
    }

    const handleLocation = (object) => {
        if (!object.id) return undefined;
        const { coordinates } = object.geometry;
        onUpdate({
            ...data,
            location: formatPlaceName(object.place_name),
            latitude: coordinates[1],
            longitude: coordinates[0]
        })

        if (Errors.location)
            setErrors(errors => {
                return {
                    ...errors,
                    location: undefined
                }
            })
    }

    return (
        <form onSubmit={onSubmit} className="auth-form">
            <FormGroup>
                <Input
                    type="text"
                    name="fname"
                    value={data.fname || ''}
                    onChange={handleChange}
                    label="First Name"
                    error={Errors.fname && true}
                />
                {Errors.fname && <span className="input-error">{Errors.fname[0]}</span>}
            </FormGroup>
            <FormGroup>
                <Input
                    type="text"
                    name="lname"
                    value={data.lname || ''}
                    onChange={handleChange}
                    label="Last Name"
                    error={Errors.lname && true}
                />
                {Errors.lname && <span className="input-error">{Errors.lname[0]}</span>}
            </FormGroup>
            <FormGroup className="job_type">
                <span className="title">What would you like to do on Jobtick?</span>
                <Input
                    type="radio"
                    onChange={handleRole}
                    checked={(data.role_as && data.role_as === "poster") ? true : false}
                    name="role_as"
                    value={"poster"}
                    label="Post a job"
                />
                <Input
                    type="radio"
                    onChange={handleRole}
                    checked={(data.role_as && data.role_as === "worker") ? true : false}
                    name="role_as"
                    value={"worker"}
                    label="Explore jobs"
                />
                {Errors.role_as && <span className="input-error">{Errors.role_as[0]}</span>}
            </FormGroup>

            <FormGroup>
                <LocationInput
                    label="Suburb"
                    value={data.location || ''}
                    onChange={handleLocation}
                    error={Errors.location && true}

                />
                {Errors.location && <span className="input-error">{Errors.location[0]}</span>}
            </FormGroup>
            <div className="actions">
                {onBack &&
                    <Col xs={4}>
                        <Button outline color="secondary" size="lg" block onClick={onBack} className="back">
                            <span className="label">Back</span>
                        </Button>
                    </Col>
                }

                <Col xs={onBack ? 8 : undefined}>
                    <Button color="primary" type="submit" size="lg" block className="jt-signup-action">
                        {isProcessing ? <Spinner type="button" className="mr-3" style={{ borderTopColor: "#fff" }} /> :
                            <span className="label">Next</span>
                        }
                    </Button>
                </Col>
            </div>
        </form>
    )
}