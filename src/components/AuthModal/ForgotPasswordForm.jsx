import React from 'react'
import Input from 'components/Input'
import { ValidateEmail } from 'utils/typeTools'
import Spinner from 'components/Spinner'
import useIsMounted from 'react-is-mounted-hook'
import { Button, Col, FormGroup } from 'reactstrap'
import VerifyOtpForm from './VerifyOtpForm'
import { requestForgotPassword, verifyForgotPassword } from 'api/user'
import useToast from 'components/Toast/Toast'

export default function ForgotPasswordForm({ email, onSuccess, setTitle }) {
    const isMounted = useIsMounted()
    const toast = useToast()

    const [IsProcessing, setIsProcessing] = React.useState()
    const [CurrentStep, setCurrentStep] = React.useState(1)
    const [InputFields, setInputFields] = React.useState({ email })
    const [Errors, setErrors] = React.useState({})

    React.useEffect(
        () => {
            switch (CurrentStep) {
                case 2:
                    setTitle('Verification code')
                    break;
                case 3:
                    setTitle('New Password')
                    break;
                default:
                    setTitle('Forgot password')
            }
            // eslint-disable-next-line
        }, [CurrentStep])

    const handleChange = (e) => {
        e.persist()
        setInputFields(value => {
            return {
                ...value,
                [e.target.name]: e.target.value
            }
        })

        setErrors(value => {
            return {
                ...value,
                [e.target.name]: undefined
            }
        })
    }
    const handleForgotPassword = (e) => {
        e.preventDefault()
        e.stopPropagation()

        if (InputFields.email) {
            if (!ValidateEmail(InputFields.email)) {
                setErrors(errors => {
                    return {
                        ...errors,
                        email: ['Email address is not valid']
                    }
                })
                return undefined
            }
        } else {
            setErrors(errors => {
                return {
                    ...errors,
                    email: ['Email address is required']
                }
            })
            return undefined
        }

        requestForgotPassword(InputFields.email)
            .then(payload => {
                if (isMounted && payload.success) {
                    setCurrentStep(2)
                    if (payload.message)
                        toast.showMessage('success', payload.message)
                }
            })
            .catch(reason => {
                if (isMounted && reason.response) {
                    const errorData = reason.response.data ? reason.response.data.error : {};
                    if (errorData.errors) setErrors(errorData.errors);
                    toast.showMessage('danger', errorData.message || "Something went wrong")
                }
            })
            .finally(() => {
                if (isMounted)
                    setIsProcessing(false)
            })
    }


    const handleSetPassword = (e) => {
        e.preventDefault()
        e.stopPropagation()

        if (!InputFields.new_password) {
            setErrors(errors => {
                return {
                    ...errors,
                    new_password: ['Password is required']
                }
            })
            return undefined
        }

        if (InputFields.new_password !== InputFields.new_password_confirm) {
            setErrors(errors => {
                return {
                    ...errors,
                    new_password_confirm: ['Confirm Password doesn\'t match with new password']
                }
            })
            return undefined
        }


        verifyForgotPassword(InputFields)
            .then(payload => {
                if (isMounted && payload.success) {
                    toast.showMessage('success', "Password changed successfuly")
                    if (onSuccess)
                        onSuccess(payload.data)
                }
            })
            .catch(reason => {
                console.clear()
                console.log(reason)
                const errorData = reason.response?.data ? reason.response.data.error : {};
                if (errorData.errors) setErrors(errorData.errors);
                toast.showMessage('danger', errorData.message || "Something went wrong")
            })
            .finally(() => {
                if (isMounted)
                    setIsProcessing(false)
            })
    }
    return (
        <>
            {CurrentStep === 1 &&
                <form onSubmit={handleForgotPassword} className="auth-form">
                    <FormGroup>
                        <Input
                            type="email"
                            name="email"
                            label="Email"
                            placeholder="example@mail.com"
                            value={InputFields.email || ''}
                            onChange={handleChange}
                        />
                        {Errors.email && <span className="input-error">{Errors.email[0]}</span>}
                    </FormGroup>

                    <Button color="primary" type="submit" size="lg" block>
                        {IsProcessing ? <Spinner type="button" className="mr-3" /> : "Next"}
                    </Button>
                </form>
            }
            {CurrentStep === 2 &&
                <VerifyOtpForm
                    email={InputFields.email}
                    onSuccess={
                        (otp) => {
                            setCurrentStep(3)
                            setInputFields(inputFields => {
                                return {
                                    ...inputFields,
                                    otp
                                }
                            })
                        }
                    }
                    onBack={() => setCurrentStep(1)}
                    buttonLabel="Next"
                    hasSentFirst
                    forgotPassword
                />
            }
            {CurrentStep === 3 &&
                <form onSubmit={handleSetPassword} className="auth-form">
                    <FormGroup>
                        <Input
                            type="password"
                            name="new_password"
                            label="Password"
                            value={InputFields.new_password || ''}
                            onChange={handleChange}
                        />
                        {Errors.new_password && <span className="input-error">{Errors.new_password[0]}</span>}
                    </FormGroup>

                    <FormGroup>
                        <Input
                            type="password"
                            name="new_password_confirm"
                            label="Confirm Password"
                            value={InputFields.new_password_confirm || ''}
                            onChange={handleChange}
                        />
                        {Errors.new_password_confirm && <span className="input-error">{Errors.new_password_confirm[0]}</span>}
                    </FormGroup>

                    <div className="actions">
                        <Col xs={4}>
                            <Button outline color="secondary" size="lg" block onClick={() => { setCurrentStep(2) }} className="back">
                                <span className="label">Back</span>
                            </Button>
                        </Col>

                        <Col xs={8}>
                            <Button color="primary" type="submit" size="lg" block>
                                {IsProcessing ? <Spinner type="button" className="mr-3" /> : "Update"}
                            </Button>
                        </Col>
                    </div>
                </form>
            }
        </>
    )
}