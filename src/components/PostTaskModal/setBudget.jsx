import React from 'react'
import { FormGroup } from 'reactstrap'
import ButtonSelect from 'components/ButtonSelect'
import Input from 'components/Input'
import { FiDollarSign } from 'react-icons/fi'

export default function SetBudget({ data, errors, onUpdate, onSubmit, authMessage }) {

    const [Errors, setErrors] = React.useState(errors || {})

    React.useEffect(() => {
        setErrors(errors || {})
    }, [errors])

    const handleBudget = React.useCallback(
        (data) => {
            let name = Object.entries(data)[0][0]
            let value = Object.entries(data)[0][1]

            setErrors({})

            if (name === 'total_hours' && value * data.hourly_rate > 9999) {
                setErrors(errors => {
                    return {
                        ...errors,
                        [name]: ["Estimated budget must be lower than $9999"]
                    }
                })
            }

            if (name === 'hourly_rate' && value * data.total_hours > 9999) {
                setErrors(errors => {
                    return {
                        ...errors,
                        [name]: ["Estimated budget must be lower than $9999"]
                    }
                })
            }
            if (name === 'budget' && value > 9999) {
                setErrors(errors => {
                    return {
                        ...errors,
                        [name]: ["Estimated budget must be lower than $9999"]
                    }
                })
            }

            onUpdate({
                ...data
            })
        },
        [onUpdate],
    )

    const handleTabChange = React.useCallback(
        (value) => {

            onUpdate({
                payment_type: value
            })
        },
        [onUpdate],
    )

    React.useEffect(
        React.useCallback(
            () => {
                if (data.payment_type === "hourly")
                    onUpdate({
                        budget: parseInt(data.total_hours) * parseInt(data.hourly_rate),
                    })
                else {
                    onUpdate({
                        budget: parseInt(data.budget)
                    })
                }
            },
            [data, onUpdate],
        ), [data.total_hours, data.hourly_rate, data.payment_type])


    return (
        <form onSubmit={(e)=>{onSubmit(e)}}>
            <FormGroup className="budget-tab">
                <ButtonSelect
                    value={data.payment_type}
                    onChange={handleTabChange}
                    items={
                        [
                            {
                                label: 'Total',
                                value: 'fixed'
                            },
                            {
                                label: 'Hourly',
                                value: 'hourly'
                            }
                        ]
                    } />
            </FormGroup>
            <div className="budget-body">
                <BudgetEditor
                    data={data}
                    onChange={handleBudget}
                    errors={Errors}
                    setErrors={setErrors} />
            </div>
            <div className="budget-total">
                Estimated Budget :
                <div className="amount-view">
                    <FiDollarSign />
                    <span className="amount">{data.budget > 0 ? data.budget : "0"}</span>
                </div>
            </div>
            {Errors.category_id && <span className="input-error mt-3 mb-3 pl-0">{Errors.category_id[0]}</span>}
            {authMessage &&
                <div className="auth-message">
                    {authMessage}
                </div>
            }
        </form>
    )
}

const BudgetEditor = ({ data, onChange, errors }) => {

    const handleChange = React.useCallback(
        (e) => {
            onChange({
                [e.target.name]: parseInt(e.target.value)
            })
        },
        [onChange],
    )

    return (
        <div className="budget-editor">
            {data.payment_type === "hourly" ?
                <>
                    <div className="mb-3">
                        <Input
                            name="total_hours"
                            label="Hours"
                            placeholder="Hours"
                            value={data.total_hours}
                            onChange={handleChange}
                            error={errors.total_hours}
                        />
                        {errors.total_hours && <span className="input-error">{errors.total_hours[0]}</span>}
                    </div>
                    <div className="mb-3">
                        <Input
                            name="hourly_rate"
                            label="Amount"
                            placeholder="Amount"
                            value={data.hourly_rate}
                            onChange={handleChange}
                            beforeIcon={"$"}
                            error={errors.hourly_rate}
                        />
                        {errors.hourly_rate && <span className="input-error">{errors.hourly_rate[0]}</span>}
                    </div>
                </>
                :
                <div className="mb-3">
                    <Input
                        name="budget"
                        label="Budget"
                        placeholder="Amount"
                        value={data.budget}
                        onChange={handleChange}
                        beforeIcon={"$"}
                        error={errors.budget}
                    />
                    {errors.budget && <span className="input-error">{errors.budget[0]}</span>}
                </div>
            }
        </div>
    )
}
