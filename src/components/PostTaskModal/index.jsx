import React from "react";
import { FiInfo, FiCalendar, FiDollarSign, FiCheck } from "react-icons/fi";

import AddDetails from "./AddDetails";
import DateTime from "./DateTime";
import SetBudget from "./setBudget";

import AuthModal from "components/AuthModal";
import useIsMounted from "react-is-mounted-hook";

import { submitTask } from "api/job";
import Modal from "components/Modal";
import { Button, Col } from "reactstrap";
import { FaCircle } from "react-icons/fa";
import moment from "moment";
import Spinner from "components/Spinner";
import useToast from "components/Toast/Toast";
import { APP_BASE_URL } from "constants/config";

export default function PostTaskModal({
  category_id,
  mode,
  task,
  onClose,
  onEditSuccess,
  onUpdate,
  onDuplicate,
}) {
  const isMounted = useIsMounted();
  const toast = useToast();

  const [IsProcessing, setIsProcessing] = React.useState(false);
  const [CurrentStep, setCurrentStep] = React.useState(1);
  const [InputData, setInputData] = React.useState({
    task_type: "physical",
    payment_type: "fixed",
    due_time: [],
    musthave: [],
    attachments: [],
  });
  const [ShowSignIn, setShowSignIn] = React.useState(false);
  const [Errors, setErrors] = React.useState({});
  const [TaskSuccess, setTaskSuccess] = React.useState(false);
  const [AuthMessage, setAuthMessage] = React.useState();

  React.useEffect(
    React.useCallback(() => {
      if (category_id) {
        setInputData({
          ...InputData,
          category_id: category_id,
        });
      } else {
        setIsProcessing(false);
        setShowSignIn(false);
        setTaskSuccess(false);
        setIsProcessing(false);
        setCurrentStep(1);
        setInputData({
          task_type: "physical",
          payment_type: "fixed",
          due_time: [],
          musthave: [],
          attachments: [],
          category_id: undefined,
        });
        setErrors({});
      }
    }, [category_id, InputData]),
    [category_id]
  );

  React.useEffect(
    React.useCallback(() => {
      if (!task) {
        return undefined;
      }
      if (!TaskSuccess)
        setInputData({
          title: task.title,
          category_id: task.category_id,
          description: task.description,
          due_date:
            mode !== "duplicate"
              ? moment(task.due_date).format("YYYY-MM-DD")
              : undefined,
          due_time:
            mode !== "duplicate" && task.due_time
              ? Object.keys(task.due_time).filter(
                  (key) => task.due_time[key] === true
                )
              : [],
          payment_type: mode !== "duplicate" ? task.payment_type : "fixed",
          budget: mode !== "duplicate" ? task.budget : undefined,
          hourly_rate: mode !== "duplicate" ? task.hourly_rate : undefined,
          total_hours: mode !== "duplicate" ? task.total_hours : undefined,
          musthave: mode !== "duplicate" && task.musthave ? task.musthave : [],
          task_type: mode !== "duplicate" ? task.task_type : "physical",
          location:
            mode !== "duplicate" && task.task_type === "physical"
              ? task.location
              : undefined,
          latitude:
            mode !== "duplicate" && task.task_type === "pysical"
              ? task.position.latitude
              : undefined,
          longitude:
            mode !== "duplicate" && task.task_type === "pysical"
              ? task.position.longitude
              : undefined,
          attachments: mode !== "duplicate" ? task.attachments : [],
        });
    }, [task, mode, TaskSuccess]),
    [task]
  );

  const prevStep = () => {
    if (CurrentStep === 1) {
      onClose();
      return undefined;
    }
    if (!IsProcessing) setCurrentStep(CurrentStep - 1);
  };

  const nextStep = () => {
    if (CurrentStep < 3) setCurrentStep(CurrentStep + 1);
  };

  const handleUpdate = (newData) => {
    setInputData((inputField) => {
      return {
        ...inputField,
        ...newData,
      };
    });
  };

  const validateAddDetails = () => {
    let tempErrors = {};
    var moveNext = true;
    if (!InputData.title || InputData.title === "") {
      tempErrors.title = ["Job title is required."];
      moveNext = false;
    } else if (InputData.title.length < 10) {
      tempErrors.title = ["Job title must be at least 10 characters."];
      moveNext = false;
    } else if (InputData.title.length > 50) {
      tempErrors.title = ["Job title limit is maximum 50 characters."];
      moveNext = false;
    } else if (tempErrors.title) {
      delete tempErrors.title;
    }

    if (!InputData.description || InputData.description === "") {
      tempErrors.description = ["Job Description is required."];
      moveNext = false;
    } else if (InputData.description.length < 25) {
      tempErrors.description = [
        "Job description must be at least 25 characters.",
      ];
      moveNext = false;
    } else if (InputData.description.length > 500) {
      tempErrors.description = [
        "Job description limit is maximum 500 characters.",
      ];
      moveNext = false;
    } else if (tempErrors.description) {
      delete tempErrors.description;
    }

    if (InputData.task_type === "physical") {
      if (!InputData.location || InputData.location === "") {
        tempErrors.location = ["Job Location is required."];
        moveNext = false;
      } else if (tempErrors.location) {
        delete tempErrors.location;
      }
    } else if (tempErrors.location) {
      delete tempErrors.location;
    }

    setErrors({ ...Errors, ...tempErrors });
    return { errors: tempErrors, moveNext };
  };

  const handleNextAddDetails = (e) => {
    e.preventDefault();
    const valData = validateAddDetails();
    setErrors(valData.errors);
    if (valData.moveNext) nextStep();
  };

  const validateDateTime = () => {
    var moveNext = true;
    if (!InputData.due_date || InputData.due_date === "") {
      setErrors((errors) => {
        return {
          ...errors,
          due_date: ["Please Select a Date."],
        };
      });
      moveNext = false;
    } else if (Errors.due_date) {
      setErrors((errors) => {
        return {
          ...errors,
          due_date: undefined,
        };
      });
    }

    if (!InputData.due_time || InputData.due_time?.length === 0) {
      setErrors((errors) => {
        return {
          ...errors,
          due_time: ["Please Select a Time."],
        };
      });
      moveNext = false;
    } else if (Errors.due_time) {
      setErrors((errors) => {
        return {
          ...errors,
          due_time: undefined,
        };
      });
    }

    return moveNext;
  };

  const handleNextDateTime = (e) => {
    e.preventDefault();

    if (validateDateTime()) nextStep();
  };

  const validateSetBudget = () => {
    var moveNext = true;
    if (InputData.payment_type === "hourly") {
      if (
        !parseInt(InputData.total_hours) ||
        !parseInt(InputData.hourly_rate)
      ) {
        setErrors({
          ...Errors,
          total_hours: !parseInt(InputData.total_hours)
            ? ["Hours is required."]
            : undefined,
          hourly_rate: !parseInt(InputData.hourly_rate)
            ? ["Amount is required."]
            : undefined,
        });
        moveNext = false;
      } else {
        setErrors({
          ...Errors,
          total_hours: undefined,
          hourly_rate: undefined,
        });
      }
    } else {
      if (!parseInt(InputData.budget)) {
        setErrors({
          ...Errors,
          budget: !parseInt(InputData.budget)
            ? ["Amount is required."]
            : undefined,
        });
        moveNext = false;
      } else {
        setErrors({
          ...Errors,
          budget: undefined,
        });
      }
    }

    return moveNext;
  };

  const handleSubmit = (e) => {
    if (e) e.preventDefault();
    if (IsProcessing && !validateSetBudget()) return undefined;

    setIsProcessing(true);

    const data = {
      ...InputData,
      attachments: InputData.attachments.map((attachment) => attachment.id),
    };

    const request =
      mode === "edit" ? editTask(task.slug, data) : submitTask(data);

    request
      .then((payload) => {
        if (isMounted && payload.success) {
          setInputData(payload.data);
          if (mode === "edit" && onEditSuccess) onEditSuccess(payload.data);

          if (mode !== "edit" && onDuplicate) onDuplicate(payload.data);

          setTaskSuccess(true);
          toast.showMessage(
            "success",
            payload.message || "Job created successfully"
          );
          if (onUpdate) onUpdate(payload.data);
        }
      })
      .catch((error) => {
        if (isMounted) {
          setTaskSuccess(false);

          if (error.response.status === 401) {
            setShowSignIn(true);
            toast.showMessage(
              "danger",
              "Unauthenticated, Please login to your account."
            );
            return false;
          }
          const errorData = error.response.data
            ? error.response.data.error
            : {};
          if (errorData.errors) setErrors(errorData.errors);
          toast.showMessage(
            "danger",
            errorData.message || "Something went wrong"
          );
        }
      })
      .finally(() => {
        setIsProcessing(false);
      });
  };

  const handleRedirect = (e, url) => {
    setTimeout(() => {
      window.location.href = url;
      onClose(e);
    }, 1000);
  };

  const handleAuth = (isLoggedin, _, message) => {
    if (isLoggedin) {
      setShowSignIn(false);
      setAuthMessage(message);
      if (!message) handleSubmit();
    }
  };

  const getModalTitle = React.useCallback(() => {
    if (TaskSuccess) return "Completed";
    if (mode === "edit") {
      return "Edit your Job";
    }
    return "Post a Job";
  }, [mode, TaskSuccess]);
  const renderModalBody = () => {
    return (
      <>
        {task && TaskSuccess ? (
          <PostTaskSuccess mode={mode} />
        ) : (
          <>
            <StepNavigator step={CurrentStep} onClick={() => {}} />
            {CurrentStep === 1 && (
              <AddDetails
                data={InputData}
                errors={Errors}
                onUpdate={handleUpdate}
                mode={mode}
                onNext={handleNextAddDetails}
              />
            )}
            {CurrentStep === 2 && (
              <DateTime
                data={InputData}
                errors={Errors}
                onUpdate={handleUpdate}
                onNext={handleNextDateTime}
              />
            )}
            {CurrentStep === 3 && (
              <SetBudget
                data={InputData}
                errors={Errors}
                onUpdate={handleUpdate}
                onSubmit={handleSubmit}
                authMessage={AuthMessage}
              />
            )}
          </>
        )}
      </>
    );
  };

  const renderModalFooter = () => {
    return (
      <>
        {task && TaskSuccess ? (
          <>
            <Col>
              <Button
                color="primary"
                block
                size="lg"
                onClick={(e) => handleRedirect(e, APP_BASE_URL)}
              >
                Post a new job
              </Button>
            </Col>
            {InputData.slug && (
              <Col>
                <Button
                  color="link"
                  block
                  size="lg"
                  className="back"
                  onClick={(e) =>
                    handleRedirect(
                      e,
                      `${APP_BASE_URL}my-jobs/${InputData.slug}`
                    )
                  }
                >
                  View Job
                </Button>
              </Col>
            )}
          </>
        ) : (
          <>
            {CurrentStep === 1 && (
              <Button
                color="primary"
                onClick={handleNextAddDetails}
                size="lg"
                block
              >
                Next
              </Button>
            )}
            {CurrentStep === 2 && (
              <>
                <Col xs={4}>
                  <Button
                    color="link"
                    onClick={prevStep}
                    block
                    size="lg"
                    className="back"
                  >
                    Back
                  </Button>
                </Col>
                <Col xs={8}>
                  <Button
                    color="primary"
                    onClick={handleNextDateTime}
                    block
                    size="lg"
                  >
                    Next
                  </Button>
                </Col>
              </>
            )}
            {CurrentStep === 3 && (
              <>
                <Col xs={4}>
                  <Button
                    color="link"
                    onClick={prevStep}
                    block
                    size="lg"
                    className="back"
                  >
                    Back
                  </Button>
                </Col>
                <Col xs={8}>
                  <Button
                    color="primary"
                    onClick={handleSubmit}
                    block
                    size="lg"
                    disabled={
                      !InputData.budget ||
                      InputData.budget === 0 ||
                      InputData.budget > 9999
                    }
                  >
                    {IsProcessing && <Spinner type="button" className="mr-3" />}
                    {IsProcessing ? "Processing" : "Post the job"}
                  </Button>
                </Col>
              </>
            )}
          </>
        )}
      </>
    );
  };

  const handleModalClose = () => {
    setInputData({
      task_type: "physical",
      payment_type: "fixed",
      due_time: [],
      musthave: [],
      attachments: [],
    });
    onClose();
  };

  React.useEffect(() => {
    if (
      Errors.title ||
      Errors.description ||
      Errors.task_type ||
      Errors.location
    )
      setCurrentStep(1);
    if (Errors.due_date || Errors.due_time) setCurrentStep(2);
    if (
      Errors.payment_type ||
      Errors.budget ||
      Errors.total_hours ||
      Errors.hourly_rate
    )
      setCurrentStep(3);
  }, [Errors]);
  React.useEffect(() => {
    let modalBody = document.querySelector(".post-job-modal .modal-body");
    if (modalBody) modalBody.scrollTo(0, 0);
  }, [CurrentStep]);

  return (
    <>
      <Modal
        className="post-job-modal"
        title={getModalTitle()}
        onClose={handleModalClose}
        open={category_id ? true : false}
        footer={renderModalFooter()}
      >
        {renderModalBody()}
      </Modal>
      {ShowSignIn && (
        <AuthModal
          open={ShowSignIn}
          onClose={() => setShowSignIn(false)}
          onUpdate={handleAuth}
        />
      )}
    </>
  );
}

const PostTaskSuccess = ({ mode }) => {
  return (
    <div className="post-task-success">
      <div className="check-mark jt-post-job-success">
        <FiCheck />
      </div>
      <span className="message">
        {mode === "edit" ? "Job Saved Successfuly" : "Job Posted Successfuly"}
      </span>
    </div>
  );
};

const StepNavigator = ({ className, style, step, onClick }) => (
  <div
    className={`step-navigator${className ? ` ${className}` : ``}`}
    style={style ? style : {}}
  >
    <div
      className={`step-item${step === 1 ? " active" : " done"}`}
      onClick={() => onClick(1)}
    >
      <span className="icon">
        <FiInfo />
      </span>
      <span className="label">Details</span>
    </div>
    <FaCircle />
    <div
      className={`step-item${
        step === 2 ? " active" : step === 3 ? " done" : ""
      }`}
      onClick={() => onClick(2)}
    >
      <span className="icon">
        <FiCalendar />
      </span>
      <span className="label">Date & Time</span>
    </div>
    <FaCircle />
    <div
      className={`step-item${step === 3 ? " active" : ""}`}
      onClick={() => onClick(3)}
    >
      <span className="icon">
        <FiDollarSign />
      </span>
      <span className="label">Budget</span>
    </div>
  </div>
);
