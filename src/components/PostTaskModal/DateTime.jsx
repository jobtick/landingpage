import React from 'react'
import { FormGroup } from 'reactstrap'
import DatePickerInput from 'components/DatePickerInput';
import Input from 'components/Input';

export default function DateTime({ data, errors, onUpdate, onNext }) {
    const [Errors, setErrors] = React.useState(errors || {})

    React.useEffect(() => {
        setErrors(errors || {})
    }, [errors])

    const handleTime = React.useCallback(
        (e) => {
            onUpdate({
                ...data,
                due_time: [e.target.value]
            })

            setErrors(errors => {
                return {
                    ...errors,
                    due_time: undefined
                }
            })
        },
        [onUpdate, data],
    )

    return (
        <form onSubmit={onNext}>
            <FormGroup>
                <DatePickerInput
                    name="datetime"
                    label="Date"
                    error={Errors.due_date ? true : false}
                    placeholder="Select a Date"
                    onChange={(date) => {
                        onUpdate({
                            due_date: date
                        })
                        setErrors({
                            ...Errors,
                            due_date: undefined
                        })
                    }}
                    value={data.due_date}
                    minDate={new Date()}
                    showMonthAndYearPickers={false}
                />
                {Errors.due_date && <span className="input-error">{Errors.due_date[0]}</span>}
            </FormGroup>
            <FormGroup className="due_time_container">
                <span className="label">Time</span>
                <div className="items">
                    <Input
                        type="radio"
                        name="due_time"
                        onChange={handleTime}
                        checked={data.due_time?.includes("morning") ? true : false}
                        value="morning"
                        label={
                            <div className="time-label">
                                <span className="time-string">Morning</span>
                                <span className="time-info">Before 12:00 PM</span>
                            </div>
                        } />
                    <Input
                        type="radio"
                        name="due_time"
                        onChange={handleTime}
                        checked={data.due_time?.includes("afternoon") ? true : false}
                        value="afternoon"
                        label={
                            <div className="time-label">
                                <span className="time-string">Afternoon</span>
                                <span className="time-info">Between 12:00 PM to 06:00 PM</span>
                            </div>
                        } />
                    <Input
                        type="radio"
                        name="due_time"
                        onChange={handleTime}
                        checked={data.due_time?.includes("evening") ? true : false}
                        value="evening"
                        label={
                            <div className="time-label">
                                <span className="time-string">Evening</span>
                                <span className="time-info">After 06:00 PM</span>
                            </div>
                        }
                    />
                    <Input
                        type="radio"
                        name="due_time"
                        onChange={handleTime}
                        checked={data.due_time?.includes("anytime") ? true : false}
                        value="anytime"
                        label={
                            <div className="time-label">
                                <span className="time-string">Anytime</span>
                                <span className="time-info">You and Ticker agree on</span>
                            </div>
                        }
                    />
                </div>
            </FormGroup>
            {Errors.due_time && <span className="input-error">{Errors.due_time[0]}</span>}
        </form>
    )
}