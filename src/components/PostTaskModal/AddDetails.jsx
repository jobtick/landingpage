import React from 'react';
import Input from 'components/Input';
import LocationInput from 'components/LocationInput';
import { FormGroup, Button } from 'reactstrap'
import Spinner from 'components/Spinner';
import { FiFileText, FiMinus, FiPlayCircle, FiPlus, FiX } from 'react-icons/fi';
import useIsMounted from 'react-is-mounted-hook';
import { uploadTemporaryAttachment } from 'api/job';
import Modal from 'components/Modal';
import { getMimeType } from 'utils';
import useToast from 'components/Toast/Toast';
import Cookies from 'js-cookie';
import { formatPlaceName } from 'utils/typeTools';


export default function AddDetails({ data, errors, onUpdate, mode, onNext }) {

    const [Errors, setErrors] = React.useState(errors)

    React.useEffect(() => {
        setErrors(errors)
    }, [errors])

    const handleChange = React.useCallback(
        (e) => {
            const { name, value } = e.target
            onUpdate({
                [name]: value
            })
            setErrors({
                ...Errors,
                [name]: undefined
            })
        },
        [onUpdate, Errors],
    )

    const handleCheckbox = (e) => {
        onUpdate({
            task_type: e.target.checked ? "remote" : "physical"
        })
        setErrors(errors => {
            return {
                ...errors,
                location: undefined
            }
        })
    }

    const handleGeoData = React.useCallback(
        (data) => {
            if (!data.id) return undefined;
            const { coordinates } = data.geometry;

            if (Errors.location)
                setErrors({ ...Errors, location: undefined })

            onUpdate({
                location: formatPlaceName(data.place_name),
                latitude: coordinates[1],
                longitude: coordinates[0]
            })
        },
        [Errors, onUpdate],
    )

    return (
        <form onSubmit={onNext}>
            <FormGroup>
                <Input
                    name="title"
                    label="Job title"
                    error={Errors.title ? true : false}
                    value={data.title || ''}
                    onChange={handleChange}
                    min={10}
                    max={50}
                />
                {Errors.title && <span className="input-error">{Errors.title[0]}</span>}
            </FormGroup>
            <FormGroup>
                <Input
                    name="description"
                    label="Description"
                    error={Errors.description ? true : false}
                    value={data.description || ''}
                    onChange={handleChange}
                    min={25}
                    max={500}
                    type="textarea"
                />
                {Errors.description && <span className="input-error">{Errors.description[0]}</span>}
            </FormGroup>
            <HandleAttachment data={data} onUpdate={onUpdate} />

            <HandleRequirements data={data} onUpdate={onUpdate} />

            <FormGroup className="job_type">
                <input type="checkbox"
                    id="task_type"
                    name="task_type"
                    onChange={handleCheckbox}
                    checked={data.task_type === "remote" ? true : false}
                />
                <label htmlFor="task_type">
                    <span>Can the job be done remotely? </span>
                    <div className={`handle${data.task_type === "remote" ? ' active' : ''}`} />
                </label>
            </FormGroup>

            <LocationInput
                name="location"
                label="Location"
                error={Errors.location ? true : false}
                disabled={data.task_type === "remote"}
                placeholder="Enter job location"
                onChange={handleGeoData}
                value={data.location || ''}
            />
            {Errors.location && <span className="input-error">{Errors.location[0]}</span>}
        </form>
    )
}

const HandleAttachment = ({ data, onUpdate }) => {
    const isMounted = useIsMounted()
    const toast = useToast()

    const previewBox = React.useRef()

    const [IsUploading, setIsUploading] = React.useState(false)

    const handleRemove = (item) => {
        onUpdate({
            attachments: data.attachments.filter(attachment => attachment.id !== item.id)
        })
    }

    const handleUpload = (e) => {

        if (IsUploading)
            return undefined

        if (previewBox.current) {

            previewBox.current.src = URL.createObjectURL(e.target.files[0]);
            previewBox.current.onload = function () {
                URL.revokeObjectURL(previewBox.current.src) // free memory
            }
        }

        setIsUploading(true)
        uploadTemporaryAttachment(e.target.files[0])
            .then(payload => {
                if (payload.success && isMounted)
                    onUpdate({
                        attachments: [...data.attachments, payload.data]
                    })
            })
            .catch(reason => {
                if (isMounted && reason?.response)
                    if (reason.status === 401 || reason.response.status === 401) {
                        Cookies.remove("token")
                        toast.showMessage('danger', "Unauthenticated, you must login for upload attachment.")
                        return undefined
                    }
                if (reason.response.data) {
                    const errorData = reason.response.data
                    toast.showMessage('danger', errorData.message || "Something went wrong")
                }
            })
            .finally(
                () => {
                    if (isMounted)
                        setIsUploading(false)
                }
            )
    }

    return (
        <div className="attachment-handler">
            <span className="label">Attachments</span>
            <div className="attachment-items">
                {data.attachments.map((item, index) => {
                    const mineType = getMimeType(item.mime);
                    return (
                        <AttachmentItem
                            key={index}
                            type={mineType}
                            data={item}
                            onClick={() => { }}
                            onRemove={() => handleRemove(item)}
                        />
                    )
                })}

                <div className={`upload-box${IsUploading ? ' uploading' : ''}`} >
                    {IsUploading && <Spinner type="box" />}
                    <img ref={previewBox} src="" alt="preview" />
                    <FiPlus />
                    <input type="file" onChange={handleUpload} onClick={!Cookies.get("token") ? (e) => {
                        e.preventDefault()
                        toast.showMessage('danger','You must login to can upload attachment.')
                    }
                        : undefined
                    }
                    />
                </div>
            </div>
        </div >
    )
}

const AttachmentItem = ({ type, data, onClick, onRemove }) => {
    return (
        <div className={`attachment-item${type ? ` ${type}-item` : ''}`} onClick={onClick}>
            {onRemove && <span className="remove-trigger" onClick={onRemove}><FiX /></span>}
            <div className="attachment-preview">
                {type === "file" ?
                    <>
                        <FiFileText />
                        {data.file_name && <span className="file-name">{data.file_name}</span>}
                    </>
                    :
                    <>
                        <img onClick={onClick} src={data.url} alt={data.file_name} />
                        {type === "video" && <FiPlayCircle />}
                    </>
                }
            </div>
        </div>
    )
}

const HandleRequirements = ({ data, onUpdate }) => {

    const [Open, setOpen] = React.useState(false)
    const [InputValue, setInputValue] = React.useState('')

    const handleRemove = (index) => {
        onUpdate({
            musthave: data.musthave.filter((_, i) => i !== index)
        })
    }

    const handleAdd = (e) => {
        e.preventDefault()
        e.stopPropagation()

        if (data.musthave.length >= 3 || !InputValue)
            return;

        onUpdate({
            musthave: [...data.musthave, InputValue]
        })
        setInputValue('')
    }

    return (
        <div className="requirements-handler">
            <ul className="requirements-list">
                {data.musthave?.map((requirement, index) =>
                    <li key={index}>
                        <Button className="remove" color="danger" size="sm" onClick={() => handleRemove(index)}>
                            <FiMinus />
                        </Button>
                        <span>
                            {requirement}
                        </span>
                    </li>
                )}
            </ul>
            {(!data.musthave || data.musthave?.length < 3) &&
                <div className="add-requirement" onClick={(e) => {
                    e.preventDefault()
                    e.stopPropagation()
                    setOpen(true)
                }
                }>
                    <Button size="sm">
                        <FiPlus />
                    </Button>
                    <span>
                        Add requirements
                    </span>
                </div>
            }

            <Modal
                className="add-requirement-modal"
                open={Open}
                onClose={() => setOpen(false)}
                title="Add requirements"
                footer={
                    <Button
                        color="primary"
                        disabled={data.musthave.length > 2 || InputValue === ''}
                        onClick={handleAdd}
                        size="lg"
                        block>
                        Add
                    </Button>
                }
            >
                <div className="description">
                    <p>
                        Write maximum three things you expect from Ticker before he/she makes offer.<br />
                        For example:<br />
                        · Licence required for doing the job.<br />
                        · Should have and bring required tools to complete the job.
                    </p>
                    <div className="label">
                        <span>Requirements</span>
                        <span className={data.musthave.length > 2 ? "error" : ''}>{data.musthave.length}/3</span>
                    </div>
                    <ul className="requirements-list">
                        {data.musthave?.map((requirement, index) =>
                            <li key={index}>
                                <Button className="remove" color="danger" size="sm" onClick={() => handleRemove(index)}>
                                    <FiMinus />
                                </Button>
                                <span>
                                    {requirement}
                                </span>
                            </li>
                        )}
                    </ul>
                    <form onSubmit={handleAdd}>
                        <Input
                            name="requirements"
                            label="Requirements"
                            max={40}
                            disabled={data.musthave.length > 2}
                            placeholder={data.musthave.length > 2 ? "You can Write maximum 3 things" : "What requirements they must have?"}
                            onChange={e => {
                                if (e.target.value.length <= 40)
                                    setInputValue(e.target.value)
                            }}
                            value={InputValue}
                        />
                    </form>
                </div>
            </Modal>
        </div>
    )
}