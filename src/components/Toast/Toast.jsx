import { store } from 'react-notifications-component'
import 'animate.css/animate.compat.css'

export default function useToast() {

    return {
        showMessage : (type,title, message, duration) => store.addNotification({
            title: message ? title : '',
            message: !message ? title : message,
            type: type,
            insert: "top",
            container: "top-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            showIcon: true,
            dismiss: {
                duration: duration || 3000,
                onScreen: true
            }
        }),
    }
}