import React from 'react'

const Spinner = ({ type, className, style, label, forwardRef }) => {
    if (type === "button") {
        return (
            <span ref={forwardRef} className={`inline-spinner spinner${className ? ` ${className}` : ''}`} style={style ? style : {}}></span>
        )
    }
    if (type === "box") {
        return (
            <div ref={forwardRef} className={`box-spinner spinner${className ? ` ${className}` : ''}`} style={style ? style : {}}>
                <div className="spinner-wrapper">
                    <SpinnerChild />
                </div>
                {label && <span className="spinner-label">{label}</span>}
            </div>
        )
    }
    return (
        <div ref={forwardRef} className={`overlay-spinner spinner${className ? ` ${className}` : ''}`} style={style ? style : {}}>
            <div className="spinner-wrapper">
                <SpinnerChild />
            </div>
            {label && <span className="spinner-label">{label}</span>}
        </div>
    )
}
const SpinnerChild = () => (
    <>
        <span className="spinner-circle1 spinner-child"></span>
        <span className="spinner-circle2 spinner-child"></span>
        <span className="spinner-circle3 spinner-child"></span>
        <span className="spinner-circle4 spinner-child"></span>
        <span className="spinner-circle5 spinner-child"></span>
        <span className="spinner-circle6 spinner-child"></span>
        <span className="spinner-circle7 spinner-child"></span>
        <span className="spinner-circle8 spinner-child"></span>
        <span className="spinner-circle9 spinner-child"></span>
        <span className="spinner-circle10 spinner-child"></span>
        <span className="spinner-circle11 spinner-child"></span>
        <span className="spinner-circle12 spinner-child"></span>
    </>
)

export default Spinner;

