import React from 'react'
import { Pagination as BootstrapPagination, PaginationItem, PaginationLink } from 'reactstrap';

const Pagination = ({ active, count, onChange, hasFirst, hasLast }) => {
    if (count > 1)
        return (
            <BootstrapPagination>
                {hasFirst &&
                    <PaginationItem onClick={active > 1 ? () => onChange(1) : undefined} disabled={active === 1}>
                        <PaginationLink first className="icon"/>
                    </PaginationItem>
                }
                <PaginationItem onClick={active > 1 ? () => onChange(active - 1) : undefined} disabled={active === 1}>
                    <PaginationLink previous className="icon"/>
                </PaginationItem>

                {active > 2 &&
                    <>
                        <PaginationItem onClick={() => onChange(1)} disabled={active === 1}>
                            <PaginationLink>1</PaginationLink>
                        </PaginationItem>
                        <span className="dots">...</span>
                        </>
                }

                        {[...Array(count + 1).keys()].filter(num => num !== 0).map(pageNumber =>
                            Math.abs(pageNumber - active) < 2 &&
                            <PaginationItem key={pageNumber} active={pageNumber === active} onClick={() => onChange(pageNumber)}>
                                <PaginationLink >
                                    {pageNumber}
                                </PaginationLink>
                            </PaginationItem>
                        )
                        }
                        {Math.abs(count - active) > 2 &&
                            <>
                                <span className="dots">...</span>
                                <PaginationItem onClick={() => onChange(count)}>
                                    <PaginationLink>{count}</PaginationLink>

                                </PaginationItem>
                            </>
                        }


                        <PaginationItem onClick={active < count ? () => onChange(active + 1) : undefined} disabled={active === count}>
                            <PaginationLink next className="icon"/>
                        </PaginationItem>
                        {hasLast &&
                            <PaginationItem onClick={active < count ? () => onChange(count) : undefined} disabled={active === count}>
                                <PaginationLink last className="icon"/>
                            </PaginationItem>
                        }
                    </BootstrapPagination>
        )
    return <></>
}

export default Pagination;