import React from "react";

function useTimer() {

    const [TimerOutput, setTimerOutput] = React.useState("0:00")
    // Update the count down every 1 second
    const [Timer, setTimer] = React.useState()
    const [TimeoutId, setTimeoutId] = React.useState()

    const reset = () => {
        if (Timer)
            clearInterval(Timer)
        if (TimeoutId)
            clearTimeout(TimeoutId)
        setTimerOutput("0:00")
    }
    return {
        countdown: (seconds) => {
            reset();

            var countDownDate = new Date().getTime() + seconds * 1000;

            let timer = setInterval(function () {
                // Get today's date and time
                var now = new Date().getTime();

                // Find the distance between now and the count down date
                var distance = countDownDate - now;

                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                if (parseInt(seconds / 10) === 0)
                    seconds = "0" + seconds


                setTimerOutput((days > 0 ? days + ":" : '') + (hours > 0 ? hours + ":" : "")
                    + minutes + ":" + seconds)
            }, 1000);
            setTimer(timer);
            setTimeoutId(
                setTimeout(() => {
                    clearInterval(timer);
                    setTimerOutput("0:00");
                }, seconds * 1000)
            )
        },

        output: TimerOutput,
        reset: reset
    };
}

export default useTimer;