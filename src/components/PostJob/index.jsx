import { submitJob } from "api/job";
import Typography from "components/Typography";
import React from "react";
import useIsMounted from "react-is-mounted-hook";
import Container from "reactstrap/lib/Container";
import AddDetails from "./Steps/AddDetails";
import Budget from "./Steps/Budget";
import Date from "./Steps/Date";
import Done from "./Steps/Done";
import jobContext from "./context";
import classes from "./style.module.scss";
import useToast from "components/Toast/Toast";
import AuthModal from "components/AuthModal";
import { AccountContext } from "layout";
import { useDispatch } from "react-redux";
import { event_log, useEvent } from "utils/firebase";

function PostJob (props) {
  const toast = useToast();
  const dispatch = useDispatch();
  const account = React.useContext(AccountContext);
  const { pushEvent } = useEvent();
  const [RquestLoading, setRquestLoading] = React.useState(false);
  const edit = props?.editJob;
  const job = props?.job;
  const slug = null;
  const init = {
    CurrentStep: 1,
    categoryId: edit && job ? job.category_id : 0,
    title: edit ? "Edit job" : "Post a job",
  };

  const IsMounted = useIsMounted();
  const [Steps, setSteps] = React.useState();
  const [AuthModalState, setAuthModalState] = React.useState(false);
  const [InputData, setInputData] = React.useState({
    task_type: "physical",
    category_id: 8,
  });
  const [Errors, setErrors] = React.useState({});
  const [IsSubmitting, setIsSubmitting] = React.useState(false);
  const [CurrentStep, setCurrentStep] = React.useState(init.CurrentStep);
  const [NewSlug, setNewSlug] = React.useState(slug);
  const [MoveBeforeSubmit, setMoveBeforeSubmit] = React.useState(true);

  const handleReset = () => {
    setCurrentStep(init.CurrentStep);
    setInputData({});
    setErrors({});
    setIsSubmitting(false);
  };
  const getSteps = () => {
    setSteps([
      {
        title: "Details",
      },
      {
        title: "Date & Time",
      },
      {
        title: "Budget",
      },
      {
        title: "Done!",
        hiddenInMobile: true,
      },
    ]);
  };
  // const handleSelectCategory = (category_id = 0) => {
  //   setInputData((inputData) => {
  //     return {
  //       ...inputData,
  //       category_id,
  //     };
  //   });
  //   handleNext();
  // };
  const handleNext = () => setCurrentStep((val) => val + 1);
  const handleBack = () => setCurrentStep((val) => val - 1);

  const createJobSubmit = () => {
    setRquestLoading(true);

    const images = attachmentIds();
    const values = InputData;
    if (values?.attachments) delete values.attachments;
    const data = {
      ...values,
      ...(images && { attachments: images }),
      fixed_price: values?.budget,
    };
    submitJob(data)
      .then((payload) => {
        setRquestLoading(false);
        if (IsMounted && payload.success) {
          pushEvent(event_log.api.post_job, data);
          setInputData(payload.data);
          toast.showMessage(
            "success",
            payload.message ? payload.message : "Job posted successfully"
          );
          setMoveBeforeSubmit(false);
          setCurrentStep(4);
          setNewSlug(payload.data.slug);
        }
      })
      .catch((reason) => {
        setRquestLoading(false);
        if (IsMounted && reason?.response) {
          const errorData = reason.response.data
            ? reason.response.data.error
            : {};
          if (errorData?.errors) setErrors(errorData.errors);
          toast.showMessage(
            "danger",
            errorData?.message ? errorData?.message : "Something went wrong"
          );
        }
      })
      .finally(() => {
        if (IsMounted) setIsSubmitting(false);
      });
  };

  const attachmentIds = () => {
    const attachments = InputData?.attachments;
    if (attachments && Object.keys(attachments).length)
      return Object.assign(
        {},
        Object.keys(attachments).map((key) => attachments[key].id.toString())
      );
    return false;
  };

  const handleSubmit = (e, draft = false) => {
    e?.preventDefault();

    if (IsSubmitting) return undefined;
    setIsSubmitting(true);

    createJobSubmit(draft);
  };

  const footerToggle = (payload) => {
    dispatch({
      type: "FOOTER_VISIBLE",
      payload: payload,
    });
  };

  React.useEffect(() => {
    footerToggle(false);
    getSteps();
    return () => {
      footerToggle(true);
    };
  }, []);

  React.useEffect(() => {
    if (InputData.due_date === "Invalid date")
      setInputData((prev) => ({ ...prev, due_date: null }));
  }, [InputData]);

  React.useEffect(() => {
    if (
      Errors.title ||
      Errors.description ||
      Errors.task_type ||
      Errors.location ||
      Errors.latitude ||
      Errors.longitude
    ) {
      setCurrentStep(2);
      return;
    }
    if (Errors.due_date) {
      setCurrentStep(3);
      return;
    }
    if (Errors.payment_type || Errors.budget) {
      setCurrentStep(4);
      return;
    }
  }, [Errors]);

  const contextValues = {
    jobData: job,
  };

  return (
    <jobContext.Provider value={contextValues}>
      <Container className={classes["post-job"]}>
        <div className="responsive-header">
          <Typography component="h4" variant="heading4">
            {init.title}
          </Typography>
        </div>
        <div className="header">
          <Typography component="h4" variant="heading4">
            {init.title}
          </Typography>
          <div className="steps">
            {Steps?.map((step, index) => (
              <div
                className={`step${CurrentStep === index + 1 ? " active" : ""}${CurrentStep > index + 1 ? " done" : ""
                  }`}
                key={index}
                onClick={(e) => {
                  if (index === 3) {
                    return;
                  }
                  if (MoveBeforeSubmit) {
                    setCurrentStep(index + 1)
                  } else {
                    return;
                  }
                }
                }
              >
                <Typography variant="sub-heading1" className="number">
                  {index + 1}
                </Typography>
                <Typography variant="sub-heading3" className="title">
                  {step.title}
                </Typography>
              </div>
            ))}
          </div>
          <div className="steps mobile">
            {Steps?.filter((step) => !step.hiddenInMobile).map(
              (step, index) => (
                <div
                  className={`step${CurrentStep === index + 1 ? " active" : ""
                    }${CurrentStep > index + 1 ? " done" : ""}`}
                  key={index}
                  onClick={(e) => {
                    if (index === 3) {
                      return;
                    }
                    if (MoveBeforeSubmit) {
                      setCurrentStep(index + 1)
                    } else {
                      return;
                    }
                  }
                  }
                >
                  <Typography variant="sub-heading1" className="number">
                    {index + 1}
                  </Typography>
                  <Typography variant="sub-heading3" className="title">
                    {step.title}
                  </Typography>
                </div>
              )
            )}
          </div>
          {/* )} */}
        </div>
        <div className="post-job-wrapper">
          {/* {CurrentStep === 1 && (
            <SelectCategories onSelect={handleSelectCategory} />
          )} */}

          {CurrentStep === 1 && (
            <AddDetails
              onNext={handleNext}
              onBack={handleBack}
              slug={slug}
              data={InputData}
              setData={setInputData}
              errors={Errors}
              setErrors={setErrors}
              edit={edit}
            />
          )}
          {CurrentStep === 2 && (
            <Date
              onNext={handleNext}
              onBack={handleBack}
              data={InputData}
              setData={setInputData}
              errors={Errors}
              setErrors={setErrors}
            />
          )}
          {CurrentStep === 3 && (
            <Budget
              onSubmit={() =>
                account ? createJobSubmit() : setAuthModalState(true)
              }
              isSubmitting={IsSubmitting}
              onBack={handleBack}
              data={InputData}
              setData={setInputData}
              errors={Errors}
              setErrors={setErrors}
              loading={RquestLoading}
            />
          )}
          {CurrentStep === 4 && <Done slug={NewSlug} onReset={handleReset} />}
        </div>
      </Container>
      <AuthModal
        open={AuthModalState}
        onClose={() => setAuthModalState(false)}
        SignUp={false}
        afterLogin={() => createJobSubmit()}
        redirect={false}
      />
    </jobContext.Provider>
  );
}

export default PostJob;
