import { fetchJob } from 'api/jobs'
import PageNotFound from 'pages/errors/404'
import React, { useEffect } from 'react'
import { useMutation } from 'react-query'
import { withRouter } from 'react-router'
import PostJob from '../index'

const CopyJob = (props) => {
    const { mutate, data, isLoading } = useMutation((slug) => fetchJob(slug))

    useEffect(() => {
        mutate(props.match.params.slug)
    }, [])

    return (
        <>
            {!isLoading && (
                <>
                    {data ?
                        <PostJob
                            job={data.data}
                            copyJob={true}
                        />
                        :
                        'no content'
                    }
                </>
            )}
        </>
    )
}

export default withRouter(CopyJob)
