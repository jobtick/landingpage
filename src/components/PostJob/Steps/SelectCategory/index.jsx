import React, { useEffect } from 'react'
import Typography from 'components/Typography'
import classes from './style.module.scss'
import { useMutation } from 'react-query'
import { fetchCategories } from 'api/job'

export default function SelectCategory({ onSelect = () => { } }) {
    const { mutate, data } = useMutation(() => fetchCategories())
    useEffect(() => {
        mutate()
    }, [])
    console.log(data);
    return (
        <div className={classes["select-category"]}>
            {data?.data && data?.data.length && data.data.map((data, index) =>
                <div className='category' key={index} onClick={() => onSelect(data.id)}>
                    <img src={data.icon} alt={data.name} />
                    <Typography variant='sub-heading1'>{data.name}</Typography>
                </div>
            )}
            <Typography variant='sub-heading2' className='select-mobile'>Select a category</Typography>

        </div>
    )

}