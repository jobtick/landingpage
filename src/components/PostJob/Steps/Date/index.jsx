import Typography from "components/Typography";
import Calender from "components/Calendar";
import React from "react";
import { Button, Col, FormGroup, Row } from "reactstrap";
import classes from "./style.module.scss";
import moment from "moment";
import Input from "components/Input";
import { FiChevronLeft } from "react-icons/fi";
import { event_log, useEvent } from "utils/firebase";

function DateComponent({ onNext, onBack, data, setData, errors, setErrors }) {
  const { pushEvent } = useEvent();
  React.useEffect(() => {
    pushEvent(event_log.page_view.post_job.date);
  }, []);
  const handleNext = () => {
    onNext();
    pushEvent(event_log.click.post_job.date, data);
  };
  React.useEffect(() => {
    setData({
      ...data,
      due_time: ["anytime"],
      due_date: moment(new Date()).format("YYYY-MM-DD"),
    });
    setErrors((errors) => {
      return {
        ...errors,
        due_time: undefined,
      };
    });
  }, []);
  const handleDate = (date) => {
    setData({
      ...data,
      due_date: moment(date).format("YYYY-MM-DD"),
    });

    setErrors((errors) => {
      return {
        ...errors,
        due_date: undefined,
      };
    });
  };
  const handleTime = (value) => {
    setData({
      ...data,
      due_time: [value],
    });

    setErrors((errors) => {
      return {
        ...errors,
        due_time: undefined,
      };
    });
  };
  return (
    <div className={classes.date}>
      <Row className="step-body">
        <Col md={6}>
          <Typography variant="sub-heading1">Preferred date</Typography>
          <Calender
            value={data.due_date}
            onChange={handleDate}
            min={moment().format("YYYY-MM-DD")}
            max={moment().add(3, "months").format("YYYY-MM-DD")}
          />
        </Col>
        <Col md={6} className="desktop-duetime">
          <Typography variant="sub-heading2">Select a certain time</Typography>
          <FormGroup className="due_time_container">
            <Input
              type="radio"
              name="due_time"
              onChange={(e) => handleTime(e.target.value)}
              checked={
                data?.due_time && data.due_time[0] === "morning" ? true : false
              }
              value="morning"
              disabled={
                moment().format("YYYY-MM-DD") === data.due_date &&
                moment().hour() >= 12
              }
              label={
                <div className="time-label">
                  <Typography variant="sub-heading3">Morning</Typography>
                  <Typography variant="sub-heading3">
                    {" "}
                    (Before 12:00 PM)
                  </Typography>
                </div>
              }
            />
            <Input
              type="radio"
              name="due_time"
              onChange={(e) => handleTime(e.target.value)}
              checked={
                data?.due_time && data.due_time[0] === "afternoon"
                  ? true
                  : false
              }
              value="afternoon"
              disabled={
                moment().format("YYYY-MM-DD") === data.due_date &&
                moment().hour() >= 18
              }
              label={
                <div className="time-label">
                  <Typography variant="sub-heading3">Afternoon</Typography>
                  <Typography variant="sub-heading3">
                    {" "}
                    (Between 12:00 PM to 06:00 PM)
                  </Typography>
                </div>
              }
            />
            <Input
              type="radio"
              name="due_time"
              onChange={(e) => handleTime(e.target.value)}
              checked={
                data?.due_time && data.due_time[0] === "evening" ? true : false
              }
              value="evening"
              label={
                <div className="time-label">
                  <Typography variant="sub-heading3">Evening</Typography>
                  <Typography variant="sub-heading3">
                    {" "}
                    (After 06:00 PM)
                  </Typography>
                </div>
              }
            />
            <Input
              type="radio"
              name="due_time"
              onChange={(e) => handleTime(e.target.value)}
              checked={
                data?.due_time && data.due_time[0] === "anytime" ? true : false
              }
              value="anytime"
              label={
                <div className="time-label">
                  <Typography variant="sub-heading3">Anytime</Typography>
                  <Typography variant="sub-heading3">
                    {" "}
                    (You and Ticker agree on)
                  </Typography>
                </div>
              }
            />
          </FormGroup>
          {errors.due_time && (
            <Typography variant="sub-heading3" className="input-error">
              {errors.due_time[0]}
            </Typography>
          )}
        </Col>
        <Col md={6} className="mobile-duetime">
          <Input
            type="select"
            items={[
              {
                value: "morning",
                label: "Morning (Before 12:00 pm)",
              },
              {
                value: "afternoon",
                label: "Afternoon (Between 12:00 pm to 6:00 pm)",
              },
              {
                value: "evening",
                label: "Evening (After 6:00 pm)",
              },
              {
                value: "anytime",
                label: "Anytime (You and the ticker agree on)",
              },
            ]}
            value={data?.due_time && data.due_time[0]}
            onChange={(value) => handleTime(value)}
            placeholder="Select a certain time"
          />
        </Col>
      </Row>
      <div className="pagination">
        <Button color="link" onClick={onBack}>
          <FiChevronLeft />
          <Typography variant="sub-heading1">Back</Typography>
        </Button>
        <Button
          color="primary"
          onClick={handleNext}
          type="submit"
          disabled={!data.due_date}
          id="jt-pj-date&time"
        >
          <Typography variant="sub-heading1">Next</Typography>
        </Button>
      </div>
    </div>
  );
}
export default DateComponent;
