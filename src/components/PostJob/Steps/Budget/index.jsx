import React from "react";
import Input from "components/Input";
import Typography from "components/Typography";
import classes from "./style.module.scss";
import { PriceVerify } from "utils/validators";
import { Button, Col } from "reactstrap";
import { FiChevronLeft } from "react-icons/fi";
import useToast from "components/Toast/Toast";
import { Formik, Form } from "formik";
import { event_log, useEvent } from "utils/firebase";

function Budget({
  onSubmit,
  onBack,
  data,
  setData,
  errors,
  setErrors,
  loading,
}) {
  const toast = useToast();
  const { pushEvent } = useEvent();
  React.useEffect(() => {
    pushEvent(event_log.page_view.post_job.budget);
  }, []);

  const handleNext = () => {
    pushEvent(event_log.click.post_job.budget, data);
  };
  const handleBudget = (e) => {
    const { name, value } = e.target;
    const acceptChange = () => {
      setData((data) => {
        return {
          ...data,
          [name]: value,
        };
      });
      setErrors((errors) => {
        return {
          ...errors,
          [name]: undefined,
        };
      });
    };
    if (!value || PriceVerify(value)) {
      acceptChange();
    }
  };
  return (
    <Formik
      initialValues={{}}
      onSubmit={async (values) => {
        if (data.budget < 5)
          return toast.showMessage(
            "danger",
            "Budget must be more than 5 dollar"
          );
        onSubmit();
      }}
    >
      <Form className={classes.budget}>
        <Col md={4} className="step-body">
          <Typography variant="sub-heading1">
            Set your preferred budget
          </Typography>
          <Input
            name="budget"
            placeholder="budget"
            error={errors.budget && errors.budget[0]}
            value={data.budget || ""}
            onChange={handleBudget}
            beforeIcon="$"
            className="set-budget"
          />
        </Col>
        <div className="pagination">
          <Button color="link" onClick={onBack}>
            <FiChevronLeft />
            <Typography variant="sub-heading1">Back</Typography>
          </Button>
          <Button
            className={classes.disableButton}
            id="jt-pj-budget"
            color="primary"
            type="submit"
            disabled={!data.budget || loading}
            onClick={handleNext}
          >
            <Typography variant="sub-heading1">
              {loading ? "processing ..." : "Submit"}
            </Typography>
          </Button>
        </div>
      </Form>
    </Formik>
  );
}
export default Budget;
