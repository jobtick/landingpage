import React from "react";
import Typography from "components/Typography";
import { FiFile, FiPlus, FiUpload, FiX } from "react-icons/fi";
import { getMimeType } from "utils/typeTools";
import { deleteJobAttachment, uploadTemporaryAttachment } from "api/job";
import Spinner from "components/Spinner";
import useIsMounted from "react-is-mounted-hook";
import { Col, Row } from "reactstrap";
import { useDropzone } from "react-dropzone";
import classes from "./style.module.scss";
import { useMutation } from "react-query";
import useToast from "components/Toast/Toast";

const Attachment = ({ data, setData, errors, setErrors, className }) => {
  const toast = useToast();
  const attachments = data.attachments || [];
  const isMounted = useIsMounted();
  const dropzoneRef = React.useRef();
  const [IsUploading, setIsUploading] = React.useState(false);
  const checkEditMode = null;
  const { mutate } = useMutation((id) =>
    deleteJobAttachment(checkEditMode, id)
  );

  const onDrop = (acceptedFiles) => {
    if (IsUploading || getMimeType(acceptedFiles[0].type) !== "image") {
      setErrors((errors) => {
        return {
          errors,
          attachments: ["You can upload only images"],
        };
      });
      return undefined;
    }

    setErrors((errors) => {
      return {
        errors,
        attachments: undefined,
      };
    });
    setIsUploading(true);
    uploadTemporaryAttachment(acceptedFiles[0])
      .then((payload) => {
        if (isMounted && payload.success) {
          setData((data) => {
            return {
              ...data,
              attachments: [...(data.attachments || []), payload.data],
            };
          });
          toast.showMessage(
            "success",
            payload.message || "File uploaded successfully"
          );
        }
      })
      .catch((reason) => {
        if (isMounted && reason?.response) {
          if (reason?.status === 401 || reason.response?.status === 401) {
            toast.showMessage(
              "danger",
              reason.message || "Unauthenticated, Redirecting to login."
            );
            return undefined;
          }
          if (reason.response.data) {
            console.log(reason.response.data);
            const errorData = reason.response.data.error;
            setErrors(errorData?.errors);
            toast.showMessage(
              "danger",
              errorData.message || "Something went wrong"
            );
          }
        }
      })
      .finally(() => {
        if (isMounted) {
          if (dropzoneRef.current) dropzoneRef.current.value = "";
          setIsUploading(false);
        }
      });
  };
  const { getRootProps, getInputProps } = useDropzone({ onDrop });
  const handleRemove = (id) => {
    setData((data) => {
      return {
        ...data,
        attachments: data.attachments.filter((item) => item.id !== id),
      };
    });
    if (checkEditMode) mutate(id);
  };
  return (
    <div className={[classes.attachment, className || ""].join(" ").trim()}>
      <div className="attachment-header">
        <Typography variant="sub-heading1" className="optional-typo">
          Attachment<span> (Optional)</span>
        </Typography>
        {attachments?.length > 0 && (
          <div className="mobile-choose" {...getRootProps()}>
            <input
              {...getInputProps()}
              id="mobile-upload"
              name="mobile-upload"
              ref={dropzoneRef}
              disabled={IsUploading}
            />
            {IsUploading ? (
              <>
                <Spinner type="button" />
                <Typography variant="sub-heading2">Uploading ...</Typography>
              </>
            ) : (
              <Typography
                variant="sub-heading2"
                component="label"
                htmlFor="mobile-upload"
              >
                Choose file
              </Typography>
            )}
          </div>
        )}
      </div>
      <div className="attachment-upload">
        <div {...getRootProps()} className="dropzone">
          <input
            {...getInputProps()}
            id="attachment-upload"
            name="attachment-upload"
            ref={dropzoneRef}
            disabled={IsUploading}
          />
          {IsUploading ? (
            <>
              <Spinner type="button" />
              <Typography variant="sub-heading2">Uploading ...</Typography>
            </>
          ) : (
            <>
              <FiUpload />
              <Typography variant="sub-heading2">
                Drag and drop files here
              </Typography>
            </>
          )}
        </div>
        <Typography
          variant="sub-heading2"
          component="label"
          htmlFor="attachment-upload"
          className="choose-file-btn"
        >
          {IsUploading ? (
            <>
              <Spinner type="button" /> Uploading ...
            </>
          ) : (
            <>
              <FiFile disabled={true} />
              <Typography variant="sub-heading2">Choose file</Typography>
            </>
          )}
        </Typography>
      </div>
      {errors.attachments && (
        <Typography variant="sub-heading3" className="input-error">
          {errors.attachments[0]}
        </Typography>
      )}

      {attachments?.length === 0 && (
        <Col className="mobile-upload" {...getRootProps()}>
          <Typography variant="sub-heading3">
            Press the button and choose a file
          </Typography>
          <input
            {...getInputProps()}
            id="mobile-upload"
            name="mobile-upload"
            ref={dropzoneRef}
            disabled={IsUploading}
          />
          <label htmlFor="mobile-upload" className="btn btn-primary">
            <FiPlus />
          </label>
        </Col>
      )}
      {attachments?.length > 0 && (
        <Row>
          <Col className="attachment-items">
            {attachments.map((item, index) => (
              <div className="item" key={index}>
                <span
                  className="remove-btn"
                  onClick={() => handleRemove(item.id)}
                >
                  <FiX />
                </span>
                <img src={item.thumb_url} alt={item.file_name} />
              </div>
            ))}
          </Col>
        </Row>
      )}
    </div>
  );
};

export default Attachment;
