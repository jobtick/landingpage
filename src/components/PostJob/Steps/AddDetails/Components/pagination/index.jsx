import Typography from "components/Typography";
import React from "react";
import { FiChevronLeft } from "react-icons/fi";
import { Button, Col, Row } from "reactstrap";

function PaginationAddDetails({
  data,
  onBack,
  onNext,
  backDisabled,
  eventClass,
}) {
  return (
    <Row className="pagination">
      <Col sm="6">
        {onBack && backDisabled && (
          <Button color="link" onClick={onBack}>
            <FiChevronLeft />
            <Typography variant="sub-heading1">Back</Typography>
          </Button>
        )}
      </Col>
      <Col sm="6" className="text-right">
        <Button
          color="primary"
          onClick={onNext}
          // disabled={
          //   !data.title ||
          //   data.title?.length < 5 ||
          //   !data.description ||
          //   data.description?.length < 5 ||
          //   (data.task_type !== "remote" && !data.location)
          // }
        >
          <Typography variant="sub-heading1">Next</Typography>
        </Button>
      </Col>
    </Row>
  );
}

export default PaginationAddDetails;
