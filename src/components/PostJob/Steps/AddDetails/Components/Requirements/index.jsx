import Input from "components/Input";
import Typography from "components/Typography";
import React from "react";
import { FiMinus, FiPlus } from "react-icons/fi";
import { Button } from "reactstrap";
import { PostJobRequirementVerify } from "utils/validators";
import classes from "./style.module.scss";

export default function Requirements({ data, setData, errors }) {
  const musthave = data.musthave || [];
  const [InputValue, setInputValue] = React.useState("");

  const handleRemove = (index) => {
    setData((data) => {
      return {
        ...data,
        musthave: data.musthave.filter((_, i) => i !== index),
      };
    });
  };
  const handleChange = (e) => {
    const { value } = e.target;
    if (!value || PostJobRequirementVerify(value)) setInputValue(value);
  };

  const handleAdd = (e) => {
    e.preventDefault();
    e.stopPropagation();

    if (musthave.length >= 3 || !InputValue) return;

    setData((data) => {
      return {
        ...data,
        musthave: [...(data.musthave || []), InputValue],
      };
    });
    setInputValue("");
  };

  return (
    <div className={classes.requirements}>
      <div className="requirment-header">
        <Typography variant="sub-heading1" className="optional-typo">
          Requirements<span> (Optional)</span>
        </Typography>
        <Typography variant="sub-heading3">{musthave.length}/3</Typography>
      </div>
      <Input
        name="requirement"
        onChange={handleChange}
        value={InputValue}
        max={100}
        error={errors?.musthave ? errors.musthave?.[0] : undefined}
        placeholder="Ex. Licence required for doing the job."
        afterIcon={
          <Button
            color="primary"
            type="button"
            onClick={handleAdd}
            size="sm"
            className="add-btn"
            disabled={!InputValue}
          >
            <FiPlus />
          </Button>
        }
      />
      {musthave?.length > 0 && (
        <ul className="requirement-list">
          {musthave?.map((item, index) => (
            <li key={index}>
              <Button
                color="danger"
                size="sm"
                onClick={() => handleRemove(index)}
                className="remove-btn"
              >
                <FiMinus />
              </Button>
              <Typography variant="sub-heading2">{item}</Typography>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}
