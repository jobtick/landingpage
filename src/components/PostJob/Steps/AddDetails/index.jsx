import React, { useContext } from "react";
import Input from "components/Input";
import LocationInput from "components/LocationInput";
import Typography from "components/Typography";
import { FormGroup, Row } from "reactstrap";
import { formatPlaceName } from "utils/typeTools";
import { RegularTextVerify } from "utils/validators";
import { Col } from "reactstrap";
import classes from "./style.module.scss";
import Attachment from "./Components/Attachment";
import Requirements from "./Components/Requirements";
import PaginationAddDetails from "./Components/pagination";
import useToast from "components/Toast/Toast";
import { AccountContext } from "layout";
import { event_log, useEvent } from "utils/firebase";

function AddDetails({
  onNext,
  onBack,
  data,
  setData,
  errors,
  setErrors,
  slug,
}) {
  const [Step, setStep] = React.useState(1);
  const [CustomErrors, setCustomErrors] = React.useState({});
  const toast = useToast();
  const { pushEvent } = useEvent();
  React.useEffect(() => {
    if (Step === 1) {
      pushEvent(event_log.page_view.post_job.details_1);
    } else {
      pushEvent(event_log.page_view.post_job.details_2);
    }
  }, [Step]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    const acceptChange = () => {
      setData((data) => {
        return {
          ...data,
          [name]: value,
        };
      });
      setCustomErrors((errors) => {
        return {
          ...errors,
          [name]: undefined,
        };
      });
    };
    switch (name) {
      case "title":
        if (!value || RegularTextVerify(value)) {
          acceptChange();
        }
        break;
      case "description":
        if (!value || RegularTextVerify(value)) {
          acceptChange();
        }
        break;
      default:
        acceptChange();
    }
  };
  const handleJobType = (e) => {
    e.preventDefault();
    const { name, checked } = e.target;
    setData((data) => {
      return {
        ...data,
        [name]: checked ? "remote" : "physical",
        latitude: null,
        longitude: null,
        location: null,
      };
    });
    setCustomErrors((errors) => {
      return {
        ...errors,
        location: undefined,
      };
    });
  };
  const handleLocation = (location) => {
    if (!location.id) return undefined;
    const { coordinates } = location.geometry;

    setCustomErrors((errors) => {
      return { ...errors, location: undefined };
    });
    setData((data) => {
      return {
        ...data,
        location: formatPlaceName(location.place_name),
        latitude: coordinates[1],
        longitude: coordinates[0],
      };
    });
  };
  const handleNext = (e) => {
    if (e) e.preventDefault();
    if (!data.title || data.title?.length < 10) {
      setCustomErrors((errors) => {
        return {
          ...errors,
          title: ["10 characters minimum"],
        };
      });
    }
    if (!data.description || data.description?.length < 30) {
      setCustomErrors((errors) => {
        return {
          ...errors,
          description: ["30 characters minimum"],
        };
      });
    }
    if (data.task_type !== "remote" && !data.location) {
      setCustomErrors((errors) => {
        return {
          ...errors,
          location: ["Location is requiered"],
        };
      });
    }
    if (Step === 1) {
      if (
        data?.task_type === "physical" &&
        (!data?.location || !data?.longitude || !data?.location)
      )
        return toast.showMessage("danger", "Please enter your location");
      if (
        !(
          !data.title ||
          data.title?.length < 10 ||
          !data.description ||
          data.description?.length < 30 ||
          (data.task_type !== "remote" && !data.location)
        )
      ) {
        pushEvent(event_log.click.post_job.details_1, data);
        setStep(2);
      }
    } else {
      onNext();
      pushEvent(event_log.click.post_job.details_2, data);
    }
  };
  const handleBack = Step === 2 ? () => setStep(1) : undefined;

  const pagination = (backDisabled = true, eventClass) => (
    <PaginationAddDetails
      {...{
        data,
        onBack: handleBack,
        onNext: handleNext,
        backDisabled,
        eventClass,
      }}
    />
  );

  const account = useContext(AccountContext);

  return (
    <form
      onSubmit={handleNext}
      className={classes["add-details"]}
      autoComplete="nope"
    >
      {Step === 1 ? (
        <>
          <Row className="step1 step-body">
            <Col md={6} xs={12}>
              <Input
                name="title"
                label="Job title *"
                error={CustomErrors.title && CustomErrors.title[0]}
                value={data.title || ""}
                onChange={handleChange}
                min={5}
                max={50}
              />
              <Input
                name="description"
                label="Job description  *"
                error={CustomErrors.description && CustomErrors.description[0]}
                value={data.description || ""}
                onChange={handleChange}
                className={classes.jobDescription}
                min={10}
                max={500}
                type="textarea"
              />
            </Col>
            <Col md={6} xs={12}>
              <FormGroup>
                <Input
                  type="switch"
                  label={
                    <Typography variant="body3">
                      Can the job be done remotely?
                    </Typography>
                  }
                  name="task_type"
                  onChange={handleJobType}
                  checked={data.task_type === "remote"}
                />
                <LocationInput
                  name="location"
                  placeholder="Suburb"
                  error={
                    (CustomErrors.location && CustomErrors.location[0]) ||
                    (CustomErrors.latitude && CustomErrors.latitude[0]) ||
                    (CustomErrors.longitude && CustomErrors.longitude[0])
                  }
                  onChange={handleLocation}
                  value={data.location || ""}
                  disabled={data.task_type === "remote" ? true : false}
                />
              </FormGroup>
            </Col>
          </Row>
          {pagination(!slug, "jt-pj-detail-first")}
        </>
      ) : (
        <>
          <Row className="step2 step-body">
            {/* {account &&
                            <Col md={6} xs={12}>
                                <Attachment data={data} setData={setData} errors={errors} setErrors={setErrors} className="attach-file" />
                            </Col>
                        } */}
            <Col md={6} xs={12}>
              <Requirements
                data={data}
                setData={setData}
                errors={errors}
                setErrors={setErrors}
              />
            </Col>
          </Row>
          {pagination(true, "jt-pj-detail-second")}
        </>
      )}
    </form>
  );
}
export default AddDetails;
