import React from "react";
import Typography from "components/Typography";
import classes from "./style.module.scss";
import { Button } from "reactstrap";
import { FiCheck } from "react-icons/fi";
import { useRouter } from "next/router";
import { APP_BASE_URL } from "constants/config";
import { event_log, useEvent } from "utils/firebase";
function Done({ onReset, slug }) {
  const history = useRouter();
  const { pushEvent } = useEvent();
  React.useEffect(() => {
    pushEvent(event_log.page_view.post_job.done);
  }, []);
  const handleDone = () => {
    location.replace(`${APP_BASE_URL}my-jobs/${slug}`);
    pushEvent(event_log.click.post_job.done_view_job, data);
  };
  const handlePostJob = () => {
    location.replace(`${APP_BASE_URL}post-job`);
    pushEvent(event_log.click.post_job.done_pj, data);
  };

  return (
    <div className={classes.done}>
      <div className="job-success">
        <FiCheck />
      </div>
      <Typography variant="sub-heading1">Job posted successfully</Typography>
      <div className={`${classes.buttons} flex-center`}>
        <Button color="primary" size="lg" onClick={handlePostJob}>
          <Typography variant="sub-heading2">Post a new job</Typography>
        </Button>
        <Button outline color="primary" size="lg" onClick={handleDone}>
          <Typography variant="sub-heading2">View your job</Typography>
        </Button>
      </div>
    </div>
  );
}
export default Done;
