import { createContext } from "react";

const jobContext = createContext({
    jobData: null
})

export default jobContext