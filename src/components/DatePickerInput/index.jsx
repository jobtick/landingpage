import Input from 'components/Input'
import Modal from 'components/Modal'
import React from 'react'
import { FiCalendar } from 'react-icons/fi'
import 'react-date-range/dist/styles.css'; // main css file
import 'react-date-range/dist/theme/default.css'; // theme css file
import { Calendar } from 'react-date-range';
import { Button, Col } from 'reactstrap';
import moment from 'moment';

export default function DatePickerInput({
    name,
    className,
    disabled,
    error,
    label,
    placeholder,
    value,
    onChange,
    outputFormat,
    renderFormat,
    minDate,
    maxDate,
    showMonthAndYearPickers
}) {

    const [Open, setOpen] = React.useState(false)
    const [Date, setDate] = React.useState();

    const onSubmit = (e) => {
        e.preventDefault()
        e.stopPropagation()
        onChange(moment(Date).format(outputFormat || "YYYY-MM-DD"))
        setOpen(false)
    }



    const renderModalFooter = (
        <form onSubmit={onSubmit}>
            <Col>
                <Button className="cancel" color="link" block size="lg" onClick={() => setOpen(false)}>
                    Cancel
                    </Button>
            </Col>

            <Col>
                <Button disabled={!Date} className="done" color="link" block size="lg" type="submit">
                    Done
                    </Button>
            </Col>
        </form>
    )

    return (
        <div className={`datepicker-input${className ? ' ' + className : ''}`}>
            <Input
                autoComplete="off"
                name={name}
                afterIcon={<FiCalendar />}
                disabled={disabled}
                error={error}
                label={label}
                placeholder={placeholder}
                onClick={
                    () => {
                        if (!disabled)
                            setOpen(true)
                    }
                }
                value={value ? moment(value, outputFormat || "YYYY-MM-DD").format(renderFormat || "dddd, MMMM Do YYYY") : ''}
            />

            <Modal
                className="datepicker-modal"
                title="Date"
                open={Open}
                onClose={() => setOpen(false)}
                footer={renderModalFooter}
            >
                <Calendar
                    onChange={item => setDate(item)}
                    date={Date}
                    color="#586BF5"
                    minDate={minDate}
                    maxDate={maxDate}
                    showMonthAndYearPickers={showMonthAndYearPickers}
                />
            </Modal>

        </div>
    )
}
