import React from 'react'
import { FaRegStar, FaStar } from 'react-icons/fa';

const StarRating = ({ value, onUpdate, className, style, showLabel, disabled }) => {
    const selected = parseInt(value);
    const [Hovered, setHovered] = React.useState(0)
    const handleClick = (value) => {
        if (!disabled) {
            var rating = parseInt(value);
            if (rating > 5) rating = 5;
            if (onUpdate)
                onUpdate(rating);
        }
    }
    const getLabel = () => {
        switch (value) {
            case 5:
                return 'Great'
            case 4:
                return 'Good'
            case 3:
                return 'Normal'
            case 2:
                return 'Bad'
            case 1:
                return 'Very bad'
            default:
                return ''
        }
    }
    return (
        <div className={`star-rating flex-row app-flex-center${className ? ` ${className}` : ''}${disabled ? ' disabled':''}`}>
            <div className="rating flex-row" onMouseLeave={() => setHovered(undefined)}>
                {[1, 2, 3, 4, 5].map(number =>
                    <div key={number}
                        className={`star${(!Hovered && selected >= number) || Hovered >= number ? " selected" : ""}`}
                        onClick={() => handleClick(number)}
                        onMouseEnter={() => {
                            if (!disabled)
                                setHovered(number)
                        }}
                    >
                        <span className="active"><FaStar /></span>
                        <span className="deactive"><FaRegStar /></span>
                    </div>
                )}
            </div>
            {showLabel &&
                <span className="rating-label">{getLabel()}</span>
            }
        </div>
    )
}

export default StarRating;