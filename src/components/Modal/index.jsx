import Typography from 'components/Typography'
import React from 'react'
import { FiX } from 'react-icons/fi'
import { Modal as BootstrapModal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap'

export default function Modal({ className, open, onClose, title, children, footer, backdrop }) {
    const ref = React.useRef()

    return (
        <>
            <BootstrapModal isOpen={open} toggle={onClose} className={`custom-modal ${className || ''}`} backdrop={backdrop || 'static'} ref={ref}>
                <ModalHeader toggle={onClose}><Typography variant="sub-heading1">{title}</Typography></ModalHeader>
                <ModalBody>
                    {children}
                </ModalBody>
                {footer &&
                    <ModalFooter>
                        {footer}
                    </ModalFooter>
                }
            </BootstrapModal>
        </>
    )
}