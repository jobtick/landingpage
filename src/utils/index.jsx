import React from "react";
import Axios from "axios";
import useIsMounted from "react-is-mounted-hook";
import { MAP_BOX_TOKEN, BASE_URL } from "constants/config";

export function getMimeType(mime) {
  if (mime.includes("image")) {
    return "image";
  }
  if (mime.includes("video")) {
    return "video";
  }
  if (mime.includes("application")) {
    return "file";
  }
}

export function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

  if (/android/i.test(userAgent)) {
    return "Android";
  }

  // iOS detection from: http://stackoverflow.com/a/9039885/177710
  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
    return "iOS";
  }

  return undefined;
}

export function getUrlQuery() {
  var pairs = window.location.search.substring(1).split("&"),
    obj = {},
    pair,
    i;

  for (i in pairs) {
    if (pairs[i] === "") continue;

    pair = pairs[i].split("=");
    obj[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
  }

  return obj;
}

export function setUrlQuery(key, value) {
  key = encodeURIComponent(key);
  value = encodeURIComponent(value);

  // kvp looks like ['key1=value1', 'key2=value2', ...]
  var kvp = document.location.search.substr(1).split("&");
  let i = 0;

  for (; i < kvp.length; i++) {
    if (kvp[i].startsWith(key + "=")) {
      let pair = kvp[i].split("=");
      pair[1] = value;
      kvp[i] = pair.join("=");
      break;
    }
  }

  if (i >= kvp.length) {
    kvp[kvp.length] = [key, value].join("=");
  }
  // can return this or...
  let params = kvp.filter((item) => item !== "").join("&");

  // reload page with new params
  return params;
}

export function useGeoLocation() {
  const isMounted = useIsMounted();
  const [Location, setLocation] = React.useState();
  const [LocationInfo, setLocationInfo] = React.useState();

  React.useEffect(() => {
    if (navigator?.geolocation) {
      navigator.geolocation.getCurrentPosition((pos) => {
        const { coords } = pos;
        setLocation({
          latitude: coords.latitude,
          longitude: coords.longitude,
        });
      });
    }
  }, []);

  React.useEffect(() => {
    if (Location) {
      let url = `https://api.mapbox.com/geocoding/v5/mapbox.places/${Location.longitude},${Location.latitude}.json?access_token=${MAP_BOX_TOKEN}&limit=1&country=AU&types=locality`;
      Axios.get(url)
        .then((payload) => {
          if (
            isMounted &&
            payload?.data?.features &&
            payload.data.features[0]
          ) {
            let pos = payload?.data?.features[0];
            setLocationInfo({
              text: pos.text,
              longitude: pos.center[0],
              latitude: pos.center[1],
            });
          }
        })
        .catch();
    }
  }, [Location]);

  return {
    setLocation: (lat, lng) => setLocation({ latitude: lat, longitude: lng }),
    getLocationInfo: () => {
      return LocationInfo;
    },
  };
}

export function getLevelIconUrl(tierId) {
  switch (tierId) {
    case 1:
      return "/icons/level/bronze.png";
    case 2:
      return "/icons/level/silver.png";
    case 3:
      return "/icons/level/gold.png";
    case 4:
      return "/icons/level/medal_default.png";
    default:
      return undefined;
  }
}
export const approximateCoordinate = (coordinate) => {
  const amount = Math.random() / 60 - Math.random() / 60;

  return Number(coordinate) + amount;
};
export const isDevelopment =
  BASE_URL.includes("dev.jobtick.com") || BASE_URL.includes("localhost");
