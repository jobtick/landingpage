export function getLineHeight(element){
    return parseInt(document.defaultView.getComputedStyle(element, null).getPropertyValue("line-height"))
}

export function getLineCounts(element){
    return element.offsetHeight/getLineHeight(element)
}