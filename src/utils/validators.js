import { getExtension } from "./typeTools";

export const PostJobRequirementVerify = (value) => /^(\w|\d)(\w|\d|[!-/:-@]|\s)*$/.test(value);

export const RegularTextVerify = (value) => /^((\w)(\w|[!-/:-@]|\s)*)?$/.test(value);

export const PriceVerify = (value) => /^\d{0,4}$/.test(value);

export const isImage = (filename) => {
    switch (getExtension(filename).toLowerCase()) {
        case 'jpg':
        case 'jpeg':
        case 'gif':
        case 'bmp':
        case 'png':
            return true;
    }
    return false;
}