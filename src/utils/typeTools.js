export function ValidateEmail(email) {
    if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email)) {
        return (true)
    }
    return (false)
}


/**
 * Decimal adjustment of a number.
 *
 * @param {String}  type  The type of adjustment.
 * @param {Number}  value The number.
 * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
 * @returns {Number} The adjusted value.
 */

function decimalAdjust(type, value, exp) {
    // If the exp is undefined or zero...
    if (typeof exp === 'undefined' || +exp === 0) {
        return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // If the value is not a number or the exp is not an integer...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
        return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
}

// Decimal round
export const round10 = function (value, exp) {
    return decimalAdjust('round', value, exp);
}
// Decimal floor
export const floor10 = function (value, exp) {
    return decimalAdjust('floor', value, exp);
}
// Decimal ceil
export const ceil10 = (value, exp) => {
    return decimalAdjust('ceil', value, exp);
}

export function getAustraliaShortState(state) {
    switch (state) {
        case 'New South Wales':
            return 'NSW'
        case 'Queensland':
            return 'QLD'
        case 'South Australia':
            return 'SA'
        case 'Tasmania':
            return 'TAS'
        case 'Victoria':
            return 'VIC'
        case 'Western Australia':
            return 'WA'
        case 'Australian Capital Territory':
            return 'ACT'
        case 'Jervis Bay Territory':
            return 'ACT'
        case 'Northern Territory':
            return 'NT'
        default:
            return state
    }
}

export function formatPlaceName(placename) {
    let locationArray = placename?.split(', ') || []
    return locationArray[0] + ', ' + getAustraliaShortState(locationArray[1])
}

export function truncateText(text, len) {
    if (text.length > len)
        return text.substr(0, len) + ' ...'
    return text
}

export const slugGenerator = (text) => {
    return text.toLowerCase().replace(/[&]/g, 'and').replace(/[,?!()./%$']/g, '').trim().replace(/\s/g, '-')
}


export function getMimeType(mime) {
    if (mime.includes("image")) {
        return "image"
    }
    if (mime.includes("video")) {
        return "video"
    }
    if (mime.includes("application")) {
        return "file"
    }
}

export function range(size, startAt = 0) {
    return [...Array(size).keys()].map((i) => i + startAt);
}

export const getExtension = (filename) => {
    var parts = filename.split('.');
    return parts[parts.length - 1];
}