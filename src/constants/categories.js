const categories = [
  "Gardening",
  "Handyperson",
  "Cleaning",
  "Removals",
  "Design & Marketing",
  "Tech & Computer",
  "Event",
  "Anything else",
];
export default categories;
