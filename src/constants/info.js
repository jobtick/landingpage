export const Socials = {
    linkedin :'https://www.linkedin.com/company/jobtick/',
    twitter:'https://twitter.com/jobtick',
    instagram: 'https://www.instagram.com/jobtick.au/',
    pinterest:'https://www.pinterest.com/jobtick/',
    facebook:'https://www.facebook.com/jobtick',
    youtube:'https://www.youtube.com/channel/UCRXoJTnMT-4gGtr_BkElWRw?fbclid=IwAR3TSyZbXik-FVL_Kl9JUIO5zm16Rixt9sIVhkQyP-KnQZB5l_IBJKnSwZo'
}

export const AppStores = {
    google: 'https://play.google.com/store/apps/details?id=com.jobtick.android',
    apple: 'https://apps.apple.com/au/app/jobtick/id1539848980'
}