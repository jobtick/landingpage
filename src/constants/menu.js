const MainMenu = [
  {
    label: "Post a job",
    className: "jt-postjob-header",
    link: "/post-job",
  },
  {
    label: "Categories",
    items: [
      {
        label: "Removals",
        link: "/removals",
      },
      {
        label: "Home services",
        link: "/home-services",
      },
      {
        label: "Home cleaning",
        link: "/home-cleaning",
      },
      {
        label: "Cleaning",
        link: "/cleaning",
      },
      {
        label: "Handyman",
        link: "/handyman",
      },
      {
        label: "Plumbers",
        link: "/plumbers",
      },
      {
        label: "Painters",
        link: "/painters",
      },
      {
        label: "Pest control",
        link: "/pest-control",
      },
      {
        label: "Delivery",
        link: "/delivery",
      },
      {
        label: "Computer and technology",
        link: "/computer-and-tech",
      },
      {
        label: "Graphic and design",
        link: "/graphic-and-design",
      },
      {
        label: "Computer repairs",
        link: "/computer-repairs",
      },
      {
        label: "Landscaper",
        link: "/landscaper",
      },
      {
        label: "Fencing",
        link: "/fencing",
      },
      {
        label: "Pet care",
        link: "/pet-care",
      },
      {
        label: "Dog grooming",
        link: "/dog-grooming",
      },
      {
        label: "Cat care",
        link: "/cat-care",
      },
      {
        label: "Auto repair",
        link: "/auto-repair",
      },
      {
        label: "Car wash",
        link: "/car-wash",
      },
      {
        label: "Bike repair",
        link: "/bike-repair",
      },
      {
        label: "Insulator",
        link: "/insulator",
      },
      {
        label: "Lawn care",
        link: "/lawn-care",
      },
      {
        label: "Gutter cleaning",
        link: "/gutter-cleaning",
      },
      {
        label: "Locksmith",
        link: "/locksmith",
      },
      {
        label: "Gardener",
        link: "/gardener",
      },
    ],
  },
  {
    label: "Explore jobs",
    className: "jt-explore-header",
    link: "/explore-jobs",
  },
];

export default MainMenu;
