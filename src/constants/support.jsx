const SupportData = [
    {
        id:1,
        category: 'Overview of Jobtick',
        question: 'How to get started on Jobtick?',
        answer: (
            <>
                <p>
                    Welcome to Jobtick, a platform to get help with your to-do list or to help others with theirs. Jobtick provides an online open marketplace connecting two groups of members - the Poster who needs to outsource jobs to be done and the ticker who wants to earn money by working on the jobs.
                </p>
                <p>
                    Signing up is free for everyone; our sign-up process is quick and easy. You can sign up using your email address or with a Facebook, Google or Apple account.
                </p>
            </>
        )
    },
    {
        id:2,
        category: 'Overview of Jobtick',
        question: 'What is Jobtick?',
        answer: (
            <>
                <p>
                Jobtick, is the interactive on-demand home service platforms with tremendous value to service professionals and customers, a cutting edge on-demand home service apps far superior to anything in the marketplace; you can post the Job need it to be done, we put you in touch with verified service provider "TICKER" in your neighbourhoods who are ready to get the Job done for you. Browse and filter the offers and select your preferred Ticker to complete the Job.
                </p>
            </>
        )
    },
    {
        id:3,
        category: 'Overview of Jobtick',
        question: 'Is Jobtick accessible in every country?',
        answer: <><p>No, Jobtick is only accessible in Australia</p></>
    },
    {
        id:4,
        category: 'Overview of Jobtick',
        question: 'What do I need to sign-up for a free account?',
        answer: (

            <>
                <p>
                    You will be able to sign-up on our website, within a very easy process. It is important to provide all your details such as your bank account for the payment transaction, correctly. In the case of any problem, please ask for help from our support center. On our website your all information is secure and confidential.
                </p>
            </>
        )
    },
    {
        id:5,
        category: 'Overview of Jobtick',
        question: 'What kind of services and jobs can I offer and receive in Jobtick? ',
        answer: (
            <>
                <p>
                    Jobtick is a versatile platform to offer and receive a wide variety of legal services (in person and/or remotely), such as household, office work, digital works etc. All users have an excellent opportunity to work and receive various kinds of legal services in this platform.
                </p>
            </>
        )
    },
    {
        id:6,
        category: 'Overview of Jobtick',
        question: 'Who is a Poster? ',
        answer: (
            <>
                <p>
                    Poster refers to a community of people who want to get some jobs done and/or receive some services.  Posters sign-up for the Jobtick account and post the exact details of the jobs/services they want to get completed/receive together with the price they wish to pay. Once the job is completed, posters pay the amount through the secure Jobtick payment gateway to the tickers.
                </p>
                <p>
                    The poster needs to complete 18 years of age to sign-up on Jobtick.
                </p>
            </>
        )
    },
    {
        id:7,
        category: 'Overview of Jobtick',
        question: 'Who is a Ticker?',
        answer: (
            <>
                <p>
                    To be a ticker in Jobtick, you should meet our eligibility criteria, i.e.  you must be older than 18 years old and have working right in Australia. The ticker refers to a person who provides the services to the prescribed lists of jobs on the Jobtick platform.
                </p>
                <p>
                    Posters publish a wide variety of jobs/tasks on the Jobtick platform. When you sign-up as a ticker, you can apply for the numerous jobs/task you feel you can do your best for. When a poster selects you to do the jobs/tasks as ticker, you can communicate for further details and unit requirements. You as the ticker must complete the job in order to get paid.
                </p>
            </>
        )
    },
    {
        id:8,
        category: 'Account Administration and Profile Insights',
        question: 'How to get started with the Jobtick account (Sign-up)?',
        answer: (
            <>
                <p>
                    You just need to be over 18 to be as a user on Jobtick. If you are a ticker, you will also need to have legal working rights in Australia. Jobtick has a set of Community Guidelines for its posters and tickers, all guidelines have been created in line with the Terms and Conditions, and are in place to help all members to have a fair, enjoyable and safe experience on the platform.
                </p>
                <p>
                    To become a Jobtick community member, an individual can sign-up using an e-mail address, Facebook, or any Google account.
                </p>
                <p>
                    As soon as sign-up completes, the user needs to complete some credentials in his/her profile, including his/her personal information and skills layout. You might want to upload any documents such as trade certificate, licenses, achievement, organization details and other portfolio features. An excellent and updated profile helps in obtaining the jobs, efficiently.
                </p>
                <p>
                    Make sure that all information provided are correct, and accurate.
                </p>
            </>
        )
    },
    {
        id:9,
        category: 'Account Administration and Profile Insights',
        question: 'How can I update my account setup and settings?',
        answer: (
            <>
                <p>
                    To update your account, simply follow these steps:
                </p>
                <ul className="none">
                    <li>Step1: Sign in to your Jobtick Account</li>
                    <li>Step2: Click on ‘menu’ icon</li>
                    <li>Step3: Click on ‘Settings.’ from the extended menu</li>
                    <li>Step4: Click on ‘Settings.’ from the extended menu</li>
                    <li>Step5: Click on ‘Save’ button to save all account details that you have modified.</li>
                </ul>
            </>
        )
    },
    {
        id:10,
        category: 'Account Administration and Profile Insights',
        question: 'How to set up your profile on Jobtick?',
        answer: (
            <>
                <p>
                    Jobtick is a secure and verified platform established to encourage the marketplace. It is essential to complete the necessary profile details to participate in the jobs actively.
                </p>
                <p>Few Profile parameters which are essential to be provided are as follows:</p>
                <ul className="numbered">
                    <li>Date of Birth: This proves the fact of eligibility for the platform.</li>
                    <li>Mobile Number: This is required to verify the user isn’t a robot or a bot.</li>
                    <li>Bank Account details: For the secured gateway of payments from Jobtick. </li>
                    <li>Address</li>
                    <li>Profile Picture</li>
                </ul>
                <p>Make sure all details provided are correct and valid. All your personal information is secure in Jobtick platform.</p>
            </>
        )
    },
    {
        id:11,
        category: 'Account Administration and Profile Insights',
        question: 'How do I set alerts and notification preferences?',
        answer: (
            <>
                <p>
                    The alerts are generated to keep you active, helping you in accessing the opportunities in your fields.
                </p>
                <p>
                    The preferences are calculated by considering the profile details. The skill set, your activity on the site, together with other information decide the alerts suitable for you. To never miss any job, it is recommended to keep your alerts and notifications working.
                </p>
                <p>Steps to turn on your alerts:</p>
                <ul>
                    <li>Log in to your Jobtick account</li>
                    <li>Visit the ‘Menu’ option</li>
                    <li>Hit on “Job Alerts” and turn it on.</li>
                </ul>
                <h3>Alert Optimization</h3>
                <p>
                    Furthermore, you can also optimize your alerts. Alert Optimization includes setting up the preferences for the subjected skill or keyword.
                </p>
                <p>
                    Optimization includes the filters and segregation of alerts depending on the location and the skill set.
                </p>
                <p>
                    In the jobs alert setting page, you can keep the preferences related to the alerts and your interest notification.
                </p>
            </>
        )
    },
    {
        id:12,
        category: 'Account Administration and Profile Insights',
        question: 'Is the service for SMS notification available at Jobtick?',
        answer: (
            <>
                <p>
                    At Jobtick, you can get the SMS notification on the verified number in your profile. The user needs to verify the number by entering the correct one-time password (OTP) for the service. Meanwhile, after you log in to the Jobtick account, make sure you have enabled the SMS alerts.
                </p>
                <p>
                    To enable the SMS alerts, simply follows these steps:
                </p>
                <ul className="none">
                    <li>Step1: Visit your ‘Profile’</li>
                    <li>Step2: go to 'Edit Profile'</li>
                    <li>Step3: In the ‘Private Information’ section, verify your phone number to receive the SMS alerts.</li>
                </ul>
            </>
        )
    },
    {
        id:13,
        category: 'Account Administration and Profile Insights',
        question: 'How can I modify my notification settings?',
        answer: (
            <>
                <p>
                    As soon as you sign-up with the Jobtick account, the user is considered for email notifications and some predefined alerts by default. If you would like modify your notification settings, simply follow these steps:
                </p>
                <ul className="numbered">
                    <li>Login to the Jobtick Account</li>
                    <li>Click on the ‘Menu’ icon</li>
                    <li>Click on ‘Setting’</li>
                    <li>Select ‘Notification Settings’</li>
                    <li>Here you can edit the preferences and schedule related to the keywords and skill set.</li>
                </ul>
            </>
        )
    },
    {
        id:14,
        category: 'Account Administration and Profile Insights',
        question: 'I want to deactivate my account permanently, How can I do that?',
        answer: (
            <>
                <p>
                    Well, Jobtick isn't happy to see you go. We always try to improve the users experience and services with every review and suggestion. However, if you are still not satisfied with the Jobtick services and would like to delete your account permanently, please contact our support center. If you would like to deactivate your account:
                </p>
                <ul className="none">
                    <li>Visit the Jobtick.com</li>
                    <li>Log in to your account with your username and password </li>
                    <li>Click on the “Setting”</li>
                    <li>Select “Edit account”</li>
                    <li>Click on the “Deactivate my Account”</li>
                </ul>
                <p>
                    This process will hide your profile on the site. If you want to continue your JobTick account again, then you will have to log in again with your credentials. The profile will be visible again.
                </p>
            </>
        )
    },
    {
        id:15,
        category: 'Posting a job',
        question: 'How to post a job on Jobtick? ',
        answer: (
            <>
                <p>
                    A job posting is the actual work of the poster. The poster defines any services he/she would like  to be accomplished by a ticker or tickers. The procedure for posting a job is very easy and includes no extra charges.
                </p>
                <p>
                    Furthermore, the content and explanation of the job completely depend on the poster. Meanwhile, he or she needs to follow the guidelines issued by Jobtick.
                </p>
                <p>
                    The most important aspects to follow while posting a job are the time duration (i.e. the deadline for job completion), amount offered (i.e. stipend which should be in AUD and proper to the complication level of the job) and the specifications. The contact details and other communication protocols will be followed later after selecting the ticker. The job details should be correctly explained.
                </p>
            </>
        )
    },
    {
        id:16,
        category: 'Posting a job',
        question: 'How to repost the expired job?',
        answer: (
            <>
                <p>
                    Generally, the jobs will be automatically removed from the listing after the deadline date (which is the expiration date for the job) is reached. This happens in situations when the job is not assigned to any ticker. Therefore, in such scenarios, you can repost the job with a new deadline from desktop or applications.
                </p>
            </>
        )
    },
    {
        id:17,
        category: 'Posting a job',
        question: 'What to do next after I post a job?',
        answer: (
            <>
                <p>
                    After you post the job with all details, you are expected to wait until the tickers make offers for your job. Posting jobs make your job visible on the job feed page.
                </p>
                <p>
                    Once tickers begin making offers, you should choose the best ticker to complete your job and accept his/her request. You can communicate within the private chat for any further details.
                </p>
                <p>
                    As soon as the ticker is selected, the allocated amount will be deducted from your enrolled bank account. The amount will be held by Jobtick pay platform.  As soon as the job is accomplished the amount will be transferred to the ticker’s account.
                </p>
            </>
        )
    },
    {
        id:18,
        category: 'Posting a job',
        question: 'When my jobs get expired?',
        answer: (
            <>
                <p>
                    You post a job with a due date time considered as the final call for the job expiration. If no tickers make an offer on your job and/or you don’t accept any offer before this deadline, the job will be expired. However, you can repost the expired job as mentioned above.
                </p>
            </>
        )
    },
    {
        id:19,
        category: 'Posting a job',
        question: 'How payment is ensured after job completion?',
        answer: (
            <>
                <p>
                    Jobtick provides a trusted bank account. After posting a job by poster, when he/she accepts a particular ticker’s offer for the job, the allocated amount for this job will be deducted from the poster’s bank account and held in Jobtick account. As soon as the job is completed, the ticker will receive this amount after poster’s confirmation. Make sure you complete the basic details of the profile including the bank account details and billing address, correctly. The entire transaction is smooth and secure.
                </p>
            </>
        )
    },
    {
        id:20,
        category: 'Making an offer',
        question: 'How can I make an offer?',
        answer: (
            <>
                <p>
                    Offer is the action to alert the poster, explaining that you are interested in the job completion offered by him/her. The offer is made by a ticker. When a job is posted, the ticker could go through the “Explore”, find all details and click on the “Make an Offer” button to make the offer by text. The ticker can also make the offer by a live video by following the Jobtick guidelines for live video, to increase his/her chance to get the job.
                </p>
            </>
        )
    },
    {
        id:21,
        category: 'Making an offer',
        question: 'Can I have a negotiation with the poster for more details? ',
        answer: (
            <>
                <p>
                    Jobtick provides an access to a private chat channel after the job is assigned. There is also a public comment section provided in the “Job details” where the ticker and poster could ask for any further clarification. Any sharing of personal details is prohibited in this section, by Jobtick.
                </p>
            </>
        )
    },
    {
        id:22,
        category: 'Making an offer',
        question: 'What things do I need to ensure before making an offer? ',
        answer: (
            <>
                <p>
                    Prior to make any offer, make sure you have provided all of the essential documents/requirements asked by Jobtick (e.g. profile photo, bank account, billing address, date of birth, mobile number etc.). Please read the entire job details, and make sure the location and job’s due date meet your availability. To protect yourself from suspicious jobs, do abide the entire protocols of Jobtick. The sharing of the personal details in the public comment section is not allowed. Once you make an offer, stay calm, not be overwhelmed. There can be many offers on a single job; it will be solely the poster's decision for the hiring purpose.
                </p>
            </>
        )
    },
    {
        id:23,
        category: 'Making an offer',
        question: 'I made the offer before, but now I have some emergency. What should I do now',
        answer: (
            <>
                <p>
                    Jobtick community believes in reliable tickers. If your offer has not been accepted by any poster yet, simply remove it without any extra charges or penalties. If the poster has already accepted your offer, you have two alternatives, either ask the poster to reschedule it or if the poster is not satisfied with rescheduling, cancel the job. In this case you may be charged and asked to pay penalty.
                </p>
            </>
        )
    },

    {
        id:24,
        category: 'Communication on Jobtick',
        question: 'What kind of communication is expected on the Jobtick?',
        answer: (
            <>
                <p>
                    Jobtick believes in sharing and addressing the problems. It abides the fact that communication solves the issues. The main idea of the platform is sharing the need and availability to help each other. The level of communication is always monitored by the Jobtick community. All users are expected to behave based on the rules on this platform and only address the issues related to the jobs posted, making the Jobtick community a trustworthy and efficient community.
                </p>
            </>
        )
    },
    {
        id:25,
        category: 'Communication on Jobtick',
        question: 'Can I post phone numbers and email addresss in the comment box?',
        answer: (
            <>
                <p>
                    Jobtick doesn’t allow direct communication between users until the confirmation of the deal. Therefore, all users are not allowed to provide any private details such as phone number, email address etc. and/or offer any price to complete the job in the comment section as it is public.
                </p>
            </>
        )
    },
    {
        id:26,
        category: 'Communication on Jobtick',
        question: 'How can I discuss any doubts regarding jobs?',
        answer: (
            <>
                <p>
                    For any enquiries, there is a “question box” provided to ask for any clarifications related to the jobs posted.
                </p>
            </>
        )
    },
    {
        id:27,
        category: 'Communication on Jobtick',
        question: 'What should I do if a ticker is not responding after I have accepted his/her offer 24 hours ago?',
        answer: (
            <>
                <p>
                    There are cases that ticker may forget the job deadline or get offline without any certain notice. Wait for 48 hours. In case of not getting any reply after 48 hours, Jobtick will alert the ticker and if he/she doesn’t reply, the offer automatically will be cancelled.  The amount deducted from the poster account will be full refunded.
                </p>
            </>
        )
    },
    {
        id:28,
        category: 'Payments and billings',
        question: 'How to modify the payment settings in Jobtick? ',
        answer: (
            <>
                <p>
                    Posters and Tickers will be asked to provide their payment details prior to making any offer and/or accepting any offer (Post a job is completely free and no payment details are required for it), as required in our gateway payment (Stripe). Valid banking details initiate smoother transactions. Once the offer is accepted by the poster, the offer amount will be deducted from his/her bank account. This amount will be held over the Jobtick payment gateway until the job completion. Follow the steps below to change/modify the payment settings and account details:
                </p>
                <ul>
                    <li>Login to the Jobtick account.</li>
                    <li>Select "Payment"</li>
                    <li>Click on the “Setting icon”</li>
                </ul>
                <p>and change/modify the details and priorities related to the payment.</p>
            </>
        )
    },
    {
        id:29,
        category: 'Payments and billings',
        question: 'How to access payment history?',
        answer: (
            <>
                <p>
                    Payment history comprises of all the pricing and billing transactions. It holds the record of every transfer you have done such as failed, in-process and successful payments. The “Payment” tab in the “Menu” provides you the payment history.
                </p>
            </>
        )
    },
    {
        id:30,
        category: 'Payments and billings',
        question: 'Is Jobtick secure and reliable for Payment?',
        answer: (
            <>
                <p>
                    The payment process on Jobtick is entirely secure and encrypted. It doesn't allow any third-party to have access to your payment details and profile. It is initiated after the entire review process. No bugs and system errors have been encountered in the payment process so far.
                </p>
            </>
        )
    },
    {
        id:31,
        category: 'Payments and billings',
        question: 'How and when is the ticker paid?',
        answer: (
            <>
                <p>
                    The ticker Payment Procedure happens as follows:
                </p>
                <p>
                    Firstly, after the ticker makes an offer, the poster accepts it and assigns the job. Then, the offer amount will be deducted from the poster's account and held in the Jobtick secure account.	Ticker now needs to complete the job. After completion of the job, the ticker needs to hit on the “Ask to release money” tab on his/her job details screen. As soon as the poster confirms the completion of the job, the money will be released and the ticker will have it in his/her account.
                </p>
            </>
        )
    },
    {
        id:32,
        category: 'Suggestions and Tips',
        question: 'What do I need to consider to get the best deals on Jobtick?',
        answer: (
            <>
                <p>
                    Well, for the better experience and high success rates, we do provide the best suggestions here. Check out the below points to experience the advantages of Jobtick.
                </p>
                <p>
                    <b>Design and update the Profile:</b> On any digital platform, the profile speaks up everything about you. A completed filled, and updated profile impresses the posters. Update of a profile picture, badges, portfolio, and revised skillset increases the chances of getting hired. The profile is the first and last impression, so try making it as much informative as you can.
                </p>
                <p>
                    <b>Keep your alert and notifications on:</b> Every second, lots of jobs are being posted, and lots of offers are being approved in the Jobtick platform. To avoid missing any opportunity and staying updated, set your alert option on. Then, the filtered jobs belonging to your profession and skill set will be notified to you on a priority basis.
                </p>
                <p>
                    <b>Complete the banking details:</b>Post a job is completely free in JobTick, but without the payment details, the users cannot accept and/or make an offer. Therefore, make sure you have provided your bank details correctly.
                </p>
                <p>
                    <b>Rules and Regulation:</b> Make sure you follow all the community guidelines offered by Jobtick. Jobtick doesn't appreciate the violence of any law. Maintaining the professionalism and etiquette of the platform is its ultimate aim.
                </p>
            </>
        )
    },
    {
        id:33,
        category: 'Suggestions and Tips',
        question: 'How to make sure whether to place an offer or not?',
        answer: (
            <>
                <p>
                    Before making an offer, kindly check the details, description, and location of the job. Check out the timeline and schedule of the job details. Verify whether you are available on the dates or not. Analyze the pricing scenario sufficiency to the job's complexity if you think that the requirements and details match your expectations, and you can go for the offer placing procedure.
                </p>
            </>
        )
    },
    {
        id:34,
        category: 'Suggestions and Tips',
        question: 'What do I need to do if payment is not in my account after finishing a job?',
        answer: (
            <>
                <p>
                    After completing the job, make sure you hit on the button of 'Ask to release'. Without this, no payment is initiated from Jobtick. If the problem still exists, please contact our support center.
                </p>
            </>
        )
    },
    {
        id:35,
        category: 'Suggestions and Tips',
        question: 'What does an excellent job description signify?  ',
        answer: (
            <>
                <p>
                    Job description is the first impression of what the tickers will get about the job. It should meet every expectation you have from the ticker since it can affect the kind of services offered to you.
                </p>
                <p>
                    A good job description should have the features and content values as below:
                </p>
                <ul>
                    <li>Exact details of the job (work): All the minute requirements should be included.</li>
                    <li>The details regarding the location, time, and pricing should be included.</li>
                    <li>
                        The starting time and deadline for the job should be mentioned.
                    </li>
                    <li>The required tools and accessories should be mentioned. It should be specified whether the ticker needs to bring the required tools for the job or be provided.</li>
                    <li>The precautions and measures ensured while arriving at the job location should be specified well.</li>
                </ul>
            </>
        )
    },
    {
        id:36,
        category: 'Suggestions and Tips',
        question: 'How to identify the right ticker?',
        answer: (
            <>
                <p>
                    JWell, in the Jobtick platform, the profile speaks much about the ticker and the skills set acquired by him/her.
                </p>
                <p>
                    To select the appropriate ticker for your job, follow the below steps:
                </p>
                <ul>
                    <li>Analyze the offers received.</li>
                    <li>Visit every ticker's profile and check for the badges and portfolio inputs related to the skill set you require. Badges can be helpful to decide if the ticker is trustworthy or not.</li>
                    <li>
                        Check out the reviews for his/her previous jobs.
                    </li>
                    <li>Communicate the expectations in the comment box (personal details sharing are not allowed) </li>
                </ul>
                <p>Jobtick does not force you to select any particular ticker. If you are not confident enough, you can keep the search going. Surely you will get the desired ticker!</p>
            </>
        )
    },
    {
        id:37,
        category: 'Tax Details',
        question: 'Do I need to pay GST if my annual business turnover is $85,000?',
        answer: (
            <>
                <ul>
                    <li>Any business with annual turnover over $75,000 will need to register for GST.</li>
                    <li>You will have a choice to register or not If your annual turnover is less than $75,000</li>
                    <li>If you are not register for GST and your turnover exceed the threshold of $75,000 you must register for GST within 21 days from reaching the $75,000 threshold.</li>
                </ul>
            </>
        )
    },
    {
        id:38,
        category: 'Tax Details',
        question: 'What is GST?',
        answer: (
            <>
                <p>
                    Goods and services tax (GST) is a broad-based tax of 10% on most goods, when you charge $100 for your services, your customer will be charged $110. The $10 is GST which you must pay to ATO usually quarterly or monthly.
                </p>
            </>
        )
    },
    {
        id:39,
        category: 'Tax Details',
        question: 'Do I need to pay taxes on my income on Jobtick?',
        answer: (
            <>
                <p>YES. you pay tax on every dollar of income you earn in Australia</p>
            </>
        )
    },
    {
        id:40,
        category: 'Tax Details',
        question: 'How can I extract the payment invoice on Jobtick?',
        answer: (
            <>
                <p>
                    Jobtick records and keeps any transactions made by its users in the ‘Payment’ part of the ‘Menu’ accessible to the users only. The payment tab has the forms of paid and earned money to save and print.
                </p>
            </>
        )
    },
    {
        id:41,
        category: 'Charges and Cancellations',
        question: 'What happens when I cancel the job before completion?',
        answer: (
            <>
                <p>
                    Upon your cancelation request, the Ticker will be notified, and the Ticker has the option of accepting your cancelation request or reject the request within 48 Hours.You must have a solid reason why you want to cancel the job before completion.
                </p>
                <p>
                    If the Ticker accepts the cancelation, the job will be automatically canceled, and Jobtick will process the refund to your wallet immediately.
                </p>
                <p>
                    If the Ticker rejects the cancellation request, we will invite you and the Ticker to resolve the issue and reach an agreement on who is responsible for the fees within 72 hours. If the issue is not resolved within the time frame, Jobtick is committed to resolving disputes that arise from unfinished Jobs. Jobtick decision will be final, although you or the Ticker will have the option to take legal action should it be necessary.
                </p>
            </>
        )
    },
    {
        id:42,
        category: 'Charges and Cancellations',
        question: 'Who will pay the in-process job cancellation charges?',
        answer: (
            <>
                <p>
                    Both ticker and poster may be asked to pay the cancellation fee if:
                </p>
                <ul className="numbered">
                    <li>
                        Ticker is not responding to the private messages after the offer approval: Ticker will pay the cancellation fees.
                    </li>
                    <li>
                        Due date is over: Ticker will pay the cancellation fees.
                    </li>
                    <li>
                        Ticker is not interested to do the job: Ticker will pay the cancellation fees.
                    </li>
                    <li>
                        Ticker is not able to complete the job: Ticker will pay the cancellation fees
                    </li>
                    <li>
                        Poster is not interested to get the job completed anymore: Poster will pay the cancellation fees.
                    </li>
                    <li>
                        Poster is not responding to the private messages after the offer approval or present in the job location: Poster will pay the cancellation fees.
                    </li>
                    <li>
                        Poster requests additional works not agreed before: Poster will pay the cancellation fees.
                    </li>
                </ul>
            </>
        )
    },
    {
        id:43,
        category: 'Charges and Cancellations',
        question: 'How to reassign the job to somebody else?',
        answer: (
            <>
                <p>
                    Jobtick doesn't allow any reassignment. The option given in such scenarios where the ticker cannot complete the job is to cancel. During the phase before the offer is approved by the poster, the ticker can offer withdrawal. But after the offer acceptance ticker needs to abide the commitment and visit the offer cancellation in emergencies. The ticker is responsible for the job cancellation and needs to pay the penalty charges. The poster can post the job again, following the same procedure as before.
                </p>
            </>
        )
    },
    {
        id:44,
        category: 'Charges and Cancellations',
        question: 'Can I change the requirements and edit my job further?',
        answer: (
            <>
                <p>
                    Yes, you can do so. Jobtick suggests all its users to be sure about the requirements of their job before posting it. In the case of any alteration of details at the last moment, you are offered the option of job editing. To edit and/or update the job, visit the “My Jobs” tab and select the “edit job” option. However, if you commented on the queries regarding to the previous job details in the comment box, make sure your comments meet the updated requirements.
                </p>
            </>
        )
    },
    {
        id:45,
        category: 'Charges and Cancellations',
        question: 'I think I wouldn\'t be able to complete my job in the given interval. What should I do now?',
        answer: (
            <>
                <p>
                    Well, in this scenario, you need to consult the poster for incrementing the deadline date. The agreement between both members is necessary. You can convince the mate by delivering the cause of the extension. If you both agree with the norm, you can reschedule the job details in “My job”, “Reschedule time” section. You need to have the poster approval for this reschedule after submitting it.
                </p>
            </>
        )
    },
    {
        id:46,
        category: 'Reviews on Jobtick',
        question: 'What is the importance of reviews?',
        answer: (
            <>
                <p>
                    Users reviews are essential to make a trust between the users and Jobtick. Reviews provide an idea about the users’ background in the Jobtick as well as the quality of the services provided by them.
                </p>
            </>
        )
    },
    {
        id:47,
        category: 'Reviews on Jobtick',
        question: 'What does the success rate indicate?',
        answer: (
            <>
                <p>
                    The success rate signifies the percentage of the jobs completed by ticker successfully among the jobs assigned to him/her. The success rate becomes available on the ticker profile after completion of each job.
                </p>
            </>
        )
    },
    {
        id:48,
        category: 'Reviews on Jobtick',
        question: 'The ticker has done a phenomenal work. How can I add a review to his/her profile?',
        answer: (
            <>
                <p>
                    That’s great you would like to appreciate someone for his/her work. To leave a review, visit “My job” and hit on the option of “leave review”. Make sure you think well before posting your review since it cannot be changed anymore and can affect the possibility of getting any job by the ticker in the future.
                </p>
            </>
        )
    },
    {
        id:49,
        category: 'Reviews on Jobtick',
        question: 'Can I change the reviews on my profile?',
        answer: (
            <>
                <p>
                    Reviews are the consequences of the service qualities you have provided. Jobtick is a reliable and secure platform. The deliberate praises and approvals are not entertained in Jobtick at all. Jobtick doesn’t allow the personal deletion of any review on its platform. However, you can ask the respective review giver to give you any clarification about their review.
                </p>
            </>
        )
    },
    {
        id:50,
        category: 'Safety tips',
        question: 'What are the safety guidelines of Jobtick?',
        answer: (
            <>
                <p>
                    To stay away from any suspicious incidents, all users must avoid sharing their personal details, business contact number, mobile number, image and access to the social media account in the comment box of the jobs posted on the Jobtick platform. Once the deal finalizes, a permit will be granted to the users to have a one-on-one conversation with the defined client. All tickers must be sure they are capable to do the job before making any offer and are able to accomplish the job assigned on time. After approval of the offer, both ticker and poster should behave professionally and maintain decent contact.
                </p>
            </>
        )
    },
    {
        id:51,
        category: 'Safety tips',
        question: 'What should I do if I\'m not feeling safe while working?',
        answer: (
            <>
                <p>
                    If you are feeling unfamiliar vibes like violating, threatening, and abusing, then please report the matter to us, instantly. If it's urgent or an emergency, call your local emergency line. If the poster has threatened you, please get in touch with us to let us know. Jobtick team offers guidance and support immediately.
                </p>
            </>
        )
    },
    {
        id:52,
        category: 'Safety tips',
        question: 'What should tickers consider before making an offer?',
        answer: (
            <>
                <p>
                    Before you put an offer on any job on the Jobtick platform, you need to be sure about the poster. The poster working method should be learned. Check out the poster profile in details before you apply for the job completion. Consider the location details before accepting the job proposal. Furthermore, check the badges and reviews of the poster. Observe his/her way of communication and the objective behind the work.
                </p>
            </>
        )
    },
    {
        id:53,
        category: 'Safety tips',
        question: 'How does Jobtick help to resolve a dispute between the ticker and poster?',
        answer: (
            <>
                <p>
                    Dispute may arise due to the miscommunication between the clients. Jobtick understands the situation as every person caries his/her perception of things. In the case of any dispute related to the payments and services, discuss them with the Jobtick platform. Jobtick will review both sides, the details, the situations and available facts; and makes a conclusion. If the poster and ticker point of view doesn't match, the final decision by the Jobtick committee is compulsory to be followed. Threatening, Abusing, and not cooperating with the Jobtick executive involved in the process of the dispute is strictly prohibited. Jobtick has a well-reputed staff with highly qualified members. The platform doesn't entertain any harsh behavior and believes in professionalism.
                </p>
            </>
        )
    }
]
export default SupportData;