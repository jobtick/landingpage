export const Status = {
    pending : "pending",
    draft : "draft",
    open : "open",
    assigned: "assigned",
    complete: "completed",
    overdue: "Overdue",
    cancelled: "cancelled",
    closed: "closed"
}

export const RequestStatus = {
    pending: "pending",
    accepted: "accepted",
    declined: "declined"
}
