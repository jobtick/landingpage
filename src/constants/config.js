export const BASE_URL = process.env.NEXT_PUBLIC_APP_BASE_URL;
export const APP_BASE_URL = process.env.NEXT_PUBLIC_APP_DASHBOARD_BASE;
export const APP_API_URL = process.env.NEXT_PUBLIC_APP_API_BASE;
export const BLOG_URL = 'https://www.jobtick.com/blog';

export const GOOGLE_AUTH_CLIENT_ID = "582071119574-2d8rcnd0serh1eam9bjr26pcj41emr0i.apps.googleusercontent.com";
export const FACEBOOK_AUTH_CLIENT_ID = "1147513929013619";
export const APPLE_AUTH_CLIENT_ID = "com.jobtick.service";

export const MAP_BOX_TOKEN = "pk.eyJ1Ijoiam9idGljayIsImEiOiJja2pzMWYycWEza3RwMnpxb3BzYm1rZ3plIn0.q7vS4z3Ffxuyy0QwnOX8BA";

export const LIVECHAT_LICENCE_ID = "12468333";

export const PUSHER_CONFIG = {
    key: "31c5e7256697a01d331a",
    cluster: "us2",
    auth: "https://dev.jobtick.com/api/v1/broadcasting/auth"
}