import React from "react";
import Link from "next/link";
import {
  Container,
  Navbar,
  NavbarToggler,
  Collapse,
  Nav,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavItem,
  Button,
} from "reactstrap";
import Logo from "assets/vector/logo.svg";
import Typography from "components/Typography";
import { useRouter } from "next/router";
import { AccountContext } from "../index";
import AuthModal from "components/AuthModal";
import { logout } from "api/user";
import { APP_BASE_URL } from "constants/config";
import classes from "./style.module.scss";
import { getUrlQuery } from "utils";

export default function Header() {
  const router = useRouter();
  const account = React.useContext(AccountContext);

  const [ShowSignIn, setShowSignIn] = React.useState(0);
  const [Collapsed, setCollapsed] = React.useState(false);
  const [Scolled, setScolled] = React.useState(false);

  const handleBodyScroll = () => {
    if (window.scrollY > 10) setScolled(true);
    else setScolled(false);
  };

  React.useEffect(() => {
    window.addEventListener("scroll", handleBodyScroll);

    return () => {
      setScolled(false);
      window.removeEventListener("scroll", handleBodyScroll);
    };
  }, []);

  React.useEffect(() => {
    if (Collapsed)
      document.body.style.overflow = Collapsed ? "hidden" : undefined;
    else document.body.style.removeProperty("overflow");
  }, [Collapsed]);

  React.useEffect(() => {
    router.events.on("routeChangeStart", () => setCollapsed(false));
  }, [router]);

  React.useEffect(() => {
    if (account) return undefined;
    let { auth } = getUrlQuery();
    if (auth === "login") setShowSignIn(1);
    if (auth === "register" || auth === "invite") setShowSignIn(2);
  }, []);

  const headerRef = React.useRef();

  React.useEffect(() => {
    if (Scolled) headerRef.current?.classList.add("scrolled");
    else headerRef.current?.classList.remove("scrolled");
  }, [Scolled]);

  React.useEffect(() => {
    if (Collapsed) headerRef.current?.classList.add("collapsed");
    else headerRef.current?.classList.remove("collapsed");
  }, [Collapsed]);

  return (
    <header className={classes.header} ref={headerRef}>
      <Navbar expand="lg">
        <Container>
          <Link href="/">
            <a className="navbar-brand">
              <Logo />
            </a>
          </Link>

          <div className="navbar-right">
            {account ? (
              <>
                <Button color="link" size="lg" onClick={logout}>
                  <Typography component="span" variant="sub-heading1">
                    Log out
                  </Typography>
                </Button>
                <a href={APP_BASE_URL}>
                  <Button color="primary" outline size="lg">
                    <Typography component="span" variant="sub-heading1">
                      Hello <b>{account.name}</b>
                    </Typography>
                  </Button>
                </a>
              </>
            ) : (
              <>
                <Button
                  className="jt-login-header"
                  color="link"
                  size="lg"
                  onClick={() => setShowSignIn(1)}
                >
                  <Typography
                    component="span"
                    variant="sub-heading2"
                    className="login"
                  >
                    Log in to Jobtick
                  </Typography>
                </Button>
              </>
            )}
          </div>
        </Container>
      </Navbar>

      <AuthModal
        open={ShowSignIn > 0 ? true : false}
        onClose={() => setShowSignIn(0)}
        SignUp={ShowSignIn === 2}
      />
      <div className="overlay" />
    </header>
  );
}
