import React from "react";
import DownloadAppPopup from "./DownloadAppPopup";
import Footer from "./Footer";
import Header from "./Header";
import Cookies from "js-cookie";
import { fetchAccount } from "api/user";
import useIsMounted from "react-is-mounted-hook";
import { APP_BASE_URL, BASE_URL } from "constants/config";
import { QueryClient, QueryClientProvider } from "react-query";
import { useSelector } from "react-redux";
import DiscountBanner from "containers/home/components/DiscountBanner";

export const AccountContext = React.createContext({});

export default function LandingLayout({ children }) {
  const isMounted = useIsMounted();
  const footerState = useSelector((state) => state.layout["FOOTER_VISIBLE"]);

  const [Account, setAccount] = React.useState();

  const getAccount = () => {
    if (Cookies.get("token")) {
      fetchAccount()
        .then((payload) => {
          if (isMounted) {
            if (payload) {
              if (process.env.NODE_ENV === "production") {
                if (
                  [BASE_URL, BASE_URL.substr(0, BASE_URL.length - 1)].includes(
                    window.location.href
                  )
                )
                  window.location = APP_BASE_URL;
                if (window.location.pathname.startsWith("/explore-jobs")) {
                  let slug = window.location.pathname.split("/explore-jobs")[1];
                  window.location =
                    APP_BASE_URL + "explore-jobs" + (slug || "");
                }
              }
              setAccount(payload.data);
            }
          }
        })
        .catch((reason) => {
          if (isMounted && reason.response) {
            if (reason.response.status === 401) {
              Cookies.remove("token");
              return false;
            }
            return { error: true, response: reason.response };
          }
        });
    }
  };

  // eslint-disable-next-line
  React.useEffect(getAccount, []);

  const queryClient = new QueryClient();

  return (
    <AccountContext.Provider value={Account}>
      <QueryClientProvider client={queryClient}>
        <noscript>
          <iframe
            src="https://www.googletagmanager.com/ns.html?id=GTM-WK73Z5J"
            height="0"
            width="0"
            style={{ display: "none", visibility: "hidden" }}
          ></iframe>
        </noscript>
        {/* <DiscountBanner /> */}
        <Header />
        <main>{children}</main>
        {footerState && <Footer />}
        <DownloadAppPopup />
      </QueryClientProvider>
    </AccountContext.Provider>
  );
}
