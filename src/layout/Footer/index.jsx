import React from "react";
import { Container, Col } from "reactstrap";
import { FiMail } from "react-icons/fi";
import {
  FaTwitter,
  FaInstagram,
  FaPinterest,
  FaFacebook,
  FaLinkedin,
} from "react-icons/fa";
import Typography from "components/Typography";
import { AppStores, Socials } from "constants/info";
import Image from "next/image";
import Link from "next/link";
import { BLOG_URL } from "constants/config";
import classes from "./style.module.scss";

export default function Footer() {
  const menuObject = [
    {
      title: "Support",
      items: [
        {
          title: "Help & Support",
          url: "/support",
        },
        {
          title: "Contact Us",
          url: "/contact",
        },
        {
          title: "Blog",
          url: BLOG_URL,
          external: true,
        },
      ],
    },
    {
      title: "Explore",
      items: [
        {
          title: "Explore Jobs",
          url: "/explore-jobs",
          class: "jt-explore-footer",
        },
        {
          title: "Post a job",
          url: "/post-job",
          class: "jt-post-job-footer",
        },
      ],
    },
    {
      title: "More",
      items: [
        {
          title: "About us",
          url: "/about",
        },
        {
          title: "Terms & Conditions",
          url: "/terms",
        },
        {
          title: "Privacy Policy",
          url: "/privacy",
        },
      ],
    },
  ];

  return (
    <footer className={classes.footer}>
      <Container>
        <div className="items">
          <Col lg={2} md={6} xs={12} className="info">
            <Image
              src={"/vector/logo-white.svg"}
              alt="Jobtick"
              width="36px"
              height="36px"
              layout="fixed"
              loading="eager"
            />
            <Typography component="h2" variant="sub-heading3">
              In It Together
            </Typography>
            <div className="email">
              <FiMail />
              <Typography
                component="a"
                className="jt-mail"
                href="mailto:info@jobtick.com"
                target="_blank"
                variant="sub-heading3"
              >
                info@jobtick.com
              </Typography>
            </div>
            {/*<div className='contact'>
                            <FaPhoneAlt />
                            <Typography component='a' variant='sub-heading2'>08503046554</Typography>
                        </div>*/}
          </Col>
          <Col md={12} lg={7} className="menus">
            {menuObject.map((menu, index) => (
              <Col md={4} key={index}>
                <Typography component="h4" variant="sub-heading1">
                  {menu.title}
                </Typography>
                <ul>
                  {menu.items.map((item, index) => (
                    <li key={index} className={item.class}>
                      {item.external ? (
                        <a
                          className={item.className}
                          href={item.url}
                          target="_blank"
                        >
                          <Typography variant="body2">{item.title}</Typography>
                        </a>
                      ) : (
                        <Link href={item.url}>
                          <a className={item.className}>
                            <Typography variant="body2">
                              {item.title}
                            </Typography>
                          </a>
                        </Link>
                      )}
                    </li>
                  ))}
                </ul>
              </Col>
            ))}
          </Col>
          <Col lg={3} md={6} className="connection">
            <Typography component="h2" variant="sub-heading1">
              Connect with us
            </Typography>
            <div className="social">
              <a
                href={Socials.facebook}
                className="jt-social jt-facebook"
                target="_blank"
                className="facebooek"
              >
                <FaFacebook />
              </a>
              <a
                href={Socials.pinterest}
                className="jt-social jt-social jt-pinterest"
                target="_blank"
                className="pinterest"
              >
                <FaPinterest />
              </a>
              <a
                href={Socials.linkedin}
                className="jt-social jt-linkedin"
                target="_blank"
                className="linkedin"
              >
                <FaLinkedin />
              </a>
              <a
                href={Socials.twitter}
                className="jt-social jt-twitter"
                target="_blank"
                className="twitter"
              >
                <FaTwitter />
              </a>
              <a
                href={Socials.instagram}
                className="jt-instagram jt-youtube"
                target="_blank"
                className="instagram"
              >
                <FaInstagram />
              </a>
            </div>
            <div className="stores">
              <a
                className="iosapp jt-iosapp-footer"
                href={AppStores.apple}
                target="_blank"
              >
                <Image
                  src={"/images/footer/apple-store.jpg"}
                  alt="Download app for IOS"
                  width="135"
                  height="40"
                  loading="eager"
                />
              </a>
              <a
                className="androidapp jt-androidapp-footer"
                href={AppStores.google}
                target="_blank"
              >
                <Image
                  src={"/images/footer/google-play.jpg"}
                  alt="Download app for Android"
                  width="135"
                  height="40"
                  loading="eager"
                />
              </a>
            </div>
          </Col>
        </div>
        <div className="copyright">
          <Typography variant="body3">Copyright © 2022</Typography>
        </div>
      </Container>
    </footer>
  );
}
