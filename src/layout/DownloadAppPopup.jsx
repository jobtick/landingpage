import React from "react";
import { getMobileOperatingSystem } from "utils";
import Typography from "components/Typography";
import { Button } from "reactstrap";
import { AppStores } from "constants/info";
import { FaStar } from "react-icons/fa";
import { FiX } from "react-icons/fi";
import Image from "next/image";
import { useRouter } from "next/router";

export default function DownloadAppPopup() {
  const [Agent, setAgent] = React.useState();
  const router = useRouter();
  React.useEffect(() => {
    setAgent(getMobileOperatingSystem());
  }, []);

  if (!Agent) return <></>;

  const handleOpen = () => {
    if (Agent === "Android")
      try {
        window.location =
          "intent://jobtick.com#Intent;scheme=http;package=com.jobtick.android;end";
      } catch (e) {}
    if (Agent === "iOS")
      try {
        window.location = AppStores.apple;
      } catch (e) {}
  };
  if (router.pathname !== "/post-job")
    return (
      <div className="download-app-popup">
        <Button color="link" onClick={() => setAgent(undefined)}>
          <FiX size="16px" />
        </Button>

        <Image
          src={"/images/logo.png"}
          alt="Jobtick"
          width="50"
          height="50"
          loading="eager"
        />
        <div className="message">
          <Typography>Jobtick for {Agent}</Typography>
          <div className="stars">
            {[1, 2, 3, 4, 5].map((number) => (
              <FaStar size="14px" key={number} />
            ))}
          </div>
        </div>
        <Button color="primary" onClick={handleOpen} outline>
          <Typography variant="body1">GET</Typography>
        </Button>
      </div>
    );
  return <></>;
}
