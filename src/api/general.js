import Axios from "axios";
import API from "./_config";

export function getBlogPosts(perpage = 9, page = 1) {
    return Axios.create().get(`https://www.jobtick.com/blog/wp-json/wp/v2/posts?per_page=${perpage}&categories=1&_embed&page=${page}`)
        .then(response => {
            return response.data
        }).catch(e => {
            throw (e)
        })
}

export function submitContact(data) {
    return API.post('contact', data)
        .then(response => {
            return response.data
        }).catch(e => {
            throw (e)
        })
}

export function fetchLandingData(category, location = '') {
    return API.get(`landing/${category}${location ? '/' + location : ''}`)
        .then(response => {
            return response.data
        }).catch(e => {
            throw (e)
        })
}