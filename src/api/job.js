import API, { APIV2 } from "./_config";

export function fetchTasks(searchFor, status, filter, pageNumber) {
  return APIV2.post(`public-jobs`, {
    search_query: searchFor,
    ...filter,
    distance: filter.task_type === "remote" ? undefined : filter.distance,
    location: undefined,
    current_lat:
      filter.task_type === "remote" || !filter.distance
        ? undefined
        : filter.current_lat,
    current_lng:
      filter.task_type === "remote" || !filter.distance
        ? undefined
        : filter.current_lng,
    page: pageNumber,
  })
    .then((response) => {
      return response.data;
    })
    .catch((e) => {
      throw e;
    });
}
export function fetchTask(slug) {
  return API.get(`public-tasks/${slug}`)
    .then((response) => {
      return response.data;
    })
    .catch((e) => {
      throw e;
    });
}
export function fetchCategories() {
  return APIV2.get(`category`)
    .then((response) => {
      return response.data;
    })
    .catch((e) => {
      throw e;
    });
}

export function submitTask(data) {
  return API.post(`tasks/create`, data)
    .then((response) => {
      return response.data;
    })
    .catch((e) => {
      throw e;
    });
}

export function editJob(taskSlug, data) {
  return API.put(`tasks/${taskSlug}`, data)
    .then((response) => {
      return response.data;
    })
    .catch((e) => {
      throw e;
    });
}

export function submitJob(data) {
  return API.post(`tasks/create`, {
    ...data,
    payment_type: data.payment_type || "fixed",
    task_type: data.type || data.location ? "physical" : "remote",
  })
    .then((response) => {
      return response.data;
    })
    .catch((e) => {
      throw e;
    });
}

export function deleteJobAttachment(taskSlug, mediaId) {
  return API.delete(`/tasks/${taskSlug}/attachment?media=${mediaId}`)
    .then((response) => {
      return response.data;
    })
    .catch((e) => {
      throw e;
    });
}

export function uploadTemporaryAttachment(file) {
  let formData = new FormData();
  formData.append("media", file, file.fileName);
  return API.post(`/media/temp-attachment`, formData, {
    headers: {
      accept: "application/json",
      "Accept-Language": "en-US,en;q=0.8",
      "Content-Type": `multipart/form-data; boundary=${formData._boundary}`,
    },
  })
    .then((response) => {
      return response.data;
    })
    .catch((e) => {
      throw e;
    });
}
