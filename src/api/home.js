import API, { APIV2 } from "./_config";
export function fetchSkills() {
  return APIV2.get(`landing/skills`)
    .then((response) => {
      return response.data;
    })
    .catch((e) => {
      throw e;
    });
}
export function submitEmail(data) {
  return APIV2.post(`landing/alert`, data)
    .then((response) => {
      return response.data;
    })
    .catch((e) => {
      throw e;
    });
}
