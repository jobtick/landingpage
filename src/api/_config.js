import axios from 'axios'
import Cookies from 'js-cookie'

const API = axios.create({
	baseURL: process.env.NEXT_PUBLIC_APP_API_BASE + 'api/v1/'
})
API.interceptors.request.use(
	config => {
		const token = Cookies.get("token")
		if (!token) {
			return config
		}
		config.headers.Authorization = `Bearer ${token}`
		config.headers.Version = process.env.NEXT_PUBLIC_APP_VERSION
		return config
	},
	err => Promise.reject(err)
)
API.interceptors.response.use(
	response => {
		return response
	},
	error => {
		return Promise.reject(error)
	}
)

export const APIV2 = axios.create({
	baseURL: process.env.NEXT_PUBLIC_APP_API_BASE + 'api/v2/'
})
APIV2.interceptors.request.use(
	config => {
		const token = Cookies.get("token")
		if (!token) {
			return config
		}
		config.headers.Authorization = `Bearer ${token}`
		config.headers.Version = process.env.NEXT_PUBLIC_APP_VERSION
		return config
	},
	err => Promise.reject(err)
)
APIV2.interceptors.response.use(
	response => {
		return response
	},
	error => {
		return Promise.reject(error)
	}
)

export default API;