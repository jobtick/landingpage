import API from "./_config"
import Cookies from 'js-cookie';

export function login(credentials) {
	return API.post(`/signin`, { ...credentials, device_type: 'Web' })
		.then(response => {
			return response.data
		})
		.catch(e => {
			throw (e)
		})
}
export function logout() {
	return API.get(`/logout`)
		.then(response => {
			return response.data
		})
		.catch(e => {
			throw (e)
		})
		.finally(() => {
			Cookies.remove("token");
			window.location.pathname = '/'
		})
}
export function googleSignIn(access_token, fname, lname, referrer_code) {
	return API.post(`google/signin`, {
		access_token,
		fname,
		lname,
		referrer_code,
		device_type: 'Web',
		location: 'no-location'
	})
		.then(response => {
			return response.data
		})
		.catch(e => {
			throw (e)
		})
}

export function facebookSignIn(access_token, fname, lname, referrer_code) {
	return API.post(`facebook/signin`, {
		access_token,
		fname,
		lname,
		referrer_code,
		device_type: 'Web',
		location: 'no-location'
	})
		.then(response => {
			return response.data
		})
		.catch(e => {
			throw (e)
		})
}
export function appleSignIn(access_token, fname, lname, referrer_code) {
	return API.post(`apple/signin`, {
		access_token,
		fname,
		lname,
		referrer_code,
		device_type: 'Web',
		location: 'no-location'
	})
		.then(response => {
			return response.data
		})
		.catch(e => {
			throw (e)
		})
}


export function signup(credentials) {
	return API.post(`/signup`, { ...credentials, device_type: 'Web' })
		.then(response => {
			return response.data
		})
		.catch(e => {
			throw (e)
		})
}

export function changePassword(data) {
	return API.post(`account/password/reset`, data)
		.then(response => {
			return response.data
		})
		.catch(e => {
			throw (e)
		})
}

export function requestSendEmailOtp(email) {
	return API.post(`email-verification/resent-otp`, {
		email
	})
		.then(response => {
			return response.data
		})
		.catch(e => {
			throw (e)
		})
}

export function verifyEmailOtp(email, otp, forgotPassword) {
	return API.post(forgotPassword ? `reset-password/otp-verify` : `email-verification`, {
		email,
		otp
	})
		.then(response => {
			return response.data
		})
		.catch(e => {
			throw (e)
		})
}

export function requestForgotPassword(email) {
	return API.post(`reset-password`, {
		email
	})
		.then(response => {
			return response.data
		})
		.catch(e => {
			throw (e)
		})
}

export function verifyForgotPassword(data) {
	return API.post(`/reset-password/verify`, data)
		.then(response => {
			return response.data
		})
		.catch(e => {
			throw (e)
		})
}

export function fetchAccount() {
	return API.get(`account`).then(response => {
		return response.data
	}).catch(e => {
		throw (e)
	})
}

export function fetchProfile(userId) {
	return API.get(`public-profile/${userId}`)
		.then(response => {
			return response.data
		}).catch(e => {
			throw (e)
		})
}

export function editProfile(data) {
	return API.post(`profile/info`, data)
		.then(response => {
			return response.data
		}).catch(e => {
			throw (e)
		})
}

export function fetchReviews(userId, rateeType) {
	return API.get(`profile/${userId}/reviews/${rateeType}`)
		.then(response => {
			return response.data
		}).catch(e => {
			throw (e)
		})
}