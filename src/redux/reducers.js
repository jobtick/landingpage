import { combineReducers } from 'redux'
import layoutReducer from './reducers/layout'

const reducers = combineReducers({
    layout : layoutReducer
})

export default reducers