const initialValues = {
    FOOTER_VISIBLE : true
}

const layoutReducer = (state = initialValues, action) => {
    switch (action.type) {
        case 'FOOTER_VISIBLE':
            return {...state, 'FOOTER_VISIBLE': action.payload}
        default:
            return state
    }
}

export default layoutReducer