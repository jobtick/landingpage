import { APP_API_URL, BASE_URL } from "constants/config";
import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import reducers from "./reducers";

const reduxStore = createStore(
    reducers,
    APP_API_URL !== 'https://api.jobtick.com/' && composeWithDevTools()
)

export default reduxStore