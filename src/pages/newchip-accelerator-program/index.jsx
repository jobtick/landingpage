import React from 'react';
import Typography from 'components/Typography';
import classes from './style.module.scss';
import { Container } from 'reactstrap';

function Newchip() {
    React.useEffect(() => {
        document.body.classList.add('triangle-backround');
        document.body.classList.add('newchip-page');

        return (() => {
            document.body.classList.remove('triangle-backround');
            document.body.classList.remove('newchip-page');
        });
    }, [])
    const NewchipLn = () => <a href="https://newchip.com/" target="_blank">Newchip</a>
    const JobtickLn = () => <a href="https://jobtick.com/" target="_blank">Jobtick</a>
    return (
        <Container className={classes["newchip"]}>
            <div className="paper">
                <Typography  component="h3" variant="heading3">Jobtick Chosen for Newchip's Intensive Global Pre-Seed Accelerator Program</Typography>
                <Typography component="p" variant="body1">
                    <JobtickLn/> is proud to be among the top 10% of applicants selected for <NewchipLn/>'s renowned global accelerator program designed to provide all the skills and tools which founders need to fund, build and scale their companies rapidly.
                    Newchip's has helped over 1,000 founders from 35 countries raise over $300 million in funding.
                </Typography>
                <Typography component="p" variant="body1">
                    <b>Jobtick</b> is a scalable community marketplace based on machine learning that connects real people seeking to outsource everyday jobs and reliable service providers who can complete those jobs and tick them off the to-do list.
                </Typography>
                <Typography component="p" variant="body1">
                    <i>
                        "Newchip evaluates a vast number of companies from across the globe, selecting less than 10% to be part of our accelerator. This strict selection process makes us an ideal partner for investors looking for promising startups. On-Demand home service app companies like <b>Jobtick</b> can scale quickly with proper funding and guidance. We are excited for <b>Jobtick</b> and believe they will do well at Newchip." - Armando Vera Carvajal, Accelerator Director at Newchip.
                    </i>
                </Typography>
                <Typography component="p" variant="body1">
                    <i>
                        "After being accepted into the Newchip Accelerator, we are excited to begin the startup accelerator journey, as part of our focus on capital raising, business development, scaling of the company, and global expansion." Sajjad Bozorgi, CoFounder of <b>Jobtick</b>“
                    </i>
                </Typography>
            </div>
        </Container>

    )
}
export default Newchip;