import React from 'react'
import { useRouter } from 'next/router';
import Typography from 'components/Typography';
import HeaderTags from 'components/HeaderTags';
import { Container } from 'reactstrap';
import { BASE_URL } from 'constants/config';
import content from 'content/privacy/privacy.json'
import classes from './style.module.scss'

export default function About() {
    React.useEffect(() => {
        document.body.classList.add('triangle-backround');
        document.body.classList.add(classes['policy-page']);
        return (() => {
            document.body.classList.remove('triangle-backround');
            document.body.classList.remove(classes['policy-page']);
        });
    }, []);
    const router = useRouter()
    return (
        <>
            <HeaderTags
                title="Jobtick | Privacy policy"
                description="This privacy policy explains how the personal information of the Jobtick’s users are handled."
                image={BASE_URL.substr(0, BASE_URL.length - 1) + "/logo192.png"}
                url={router.asPath}
            />

            <Container className="policy-container">
                <Typography component="h2" variant='heading2'>{content.title}</Typography>
                {content.description.map((item, index) =>
                    <Typography component="p" variant="body1" key={index}>{item}</Typography>
                )}

                {content.sections.map((section) =>
                    <>
                        <Typography component="h3" variant='heading1'>{section.title}</Typography>
                        {section.description.map((item, index) =>
                            item.type === 'paragraph' ?
                                <Typography component="p" variant="body1" key={index}>{item.content}</Typography>
                                :
                                <ul key={index}>
                                    {item.content.map((listItem, index) =>
                                        <Typography component='li' variant='body1' key={index}>{listItem}</Typography>
                                    )}
                                </ul>
                        )}
                    </>
                )}

            </Container>
        </>
    )
}
