import React from "react";
import Hero from "components/PageComponents/about/Hero";
import classes from "./style.module.scss";
import Family from "components/PageComponents/about/Family";
import Value from "components/PageComponents/about/Value";
import TimeLine from "components/PageComponents/about/TimeLine";
import Vision from "components/PageComponents/about/Vision";

function About() {
  if (process.browser) document.body.classList.add(classes["about-page"]);
  React.useEffect(() => {
    return () => {
      document.body.classList.remove(classes["about-page"]);
    };
  }, []);

  const staticData = require(`content/about/about-us.json`);

  return (
    <>
      <Hero />
      {/* <TimeLine staticData={staticData.timeline}/> */}
      <Vision />
      <Family staticData={staticData.family} />
      <Value staticData={staticData.value} />
    </>
  );
}

export default About;
