import React from 'react'
import TermsAndCondition from "../../components/PageComponents/terms/TermsAndCondition";
import Definitions from "../../components/PageComponents/terms/Definitions";
import ThePosterObligation from "../../components/PageComponents/terms/ThePosterObligation";
import TheTickerObligation from "../../components/PageComponents/terms/TheTickerObligation";
import Fee from "../../components/PageComponents/terms/Fee";
import CancellationAndDisputes from "../../components/PageComponents/terms/CancellationAndDisputes";
import OnlinePaymentServiceProvider from "../../components/PageComponents/terms/OnlinePaymentServiceProvider";
import BackgroundsChecks from "../../components/PageComponents/terms/BackgroundsChecks";
import Feedback from "../../components/PageComponents/terms/Feedback";
import Insurance from "../../components/PageComponents/terms/Insurance";
import Termination from "../../components/PageComponents/terms/Termination";
import { useRouter } from 'next/router';
import HeaderTags from 'components/HeaderTags';
import { Container } from 'reactstrap';
import { BASE_URL } from 'constants/config';


export default function TermAndConditions() {
    React.useEffect(() => {
        document.body.classList.add('triangle-backround');
        document.body.classList.add('term-page');

        return (() => {
            document.body.classList.remove('triangle-backround');
            document.body.classList.remove('term-page');
        });
    }, [])

    const router = useRouter()
    return (
        <>
            <HeaderTags
                title="Jobtick | Terms and conditions"
                description="This privacy policy explains how the personal information of the Jobtick’s users are handled."
                image={BASE_URL.substr(0,BASE_URL.length-1) +"/logo192.png"}
                url={router.asPath}
            />
            <Container>
                <div className="paper">
                    <h3>The Use of Website and Mobile App</h3>
                    <TermsAndCondition />

                    <Definitions />

                    <ThePosterObligation />

                    <TheTickerObligation />

                    <Fee />

                    <CancellationAndDisputes />

                    <OnlinePaymentServiceProvider />

                    <Feedback />

                    <Insurance />

                    <Termination />
                </div>
            </Container>
        </>

    )
}
