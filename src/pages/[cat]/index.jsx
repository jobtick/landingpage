import React from 'react'
import Hero from 'components/PageComponents/location-category/Hero';
import TopExpert from 'components/PageComponents/location-category/TopExpert';
import ReviewsBlock from 'components/PageComponents/location-category/Reviews';
import WhatInclude from 'components/PageComponents/location-category/WhatInclude';
import RecentJobs from 'components/PageComponents/location-category/RecentJobs';
import RelatedLink from 'components/PageComponents/location-category/RelatedLink';
import WhyUsingJobtick from 'components/PageComponents/location-category/WhyUsingJobtick';
import FAQ from 'components/PageComponents/location-category/FAQ';
import classes from './style.module.scss'
import { fetchLandingData } from 'api/general';
import DefaultErrorPage from 'next/error';
import HeaderTags from 'components/HeaderTags';
import { BASE_URL } from 'constants/config';

function Category({ cat,data: serverData , staticData }) {
    if (!staticData)
        return <DefaultErrorPage statusCode={404} />

    if (process.browser)
        document.body.classList.add(classes['category-page']);
    React.useEffect(() => {
        return (() => {
            document.body.classList.remove(classes['category-page']);
        });
    }, []);

    return (
        <>
            <HeaderTags
                title={staticData.meta.title}
                description={staticData.meta.description}
                image={BASE_URL.substr(0,BASE_URL.length-1) + staticData.meta.image}
                url={'/'+cat+'/'}
                schema={staticData.schema}
            />
            <Hero type='category' title={staticData.title} cat={cat}/>
            <TopExpert category={cat}  staticData={staticData.topExpert}/>
            <ReviewsBlock title={staticData.title} staticData={staticData.reviews} />
            <WhatInclude type='category' staticData={staticData.whatInclude}/>
            <WhyUsingJobtick staticData={staticData.whyUsingJobtick}/>
            {serverData &&
                <RecentJobs data={serverData} staticData={staticData.recentJobs} />
            }
            <FAQ staticData={staticData.FAQ}/>
            <RelatedLink title={staticData.title} staticData={staticData.related}/>
        </>
    )
}
export async function getServerSideProps(ctx) {
    try {
        const { cat } = ctx.query;
        const staticData = require(`content/categories/${cat}.json`)
        let res = null
        if(staticData.hasServer)
            res = await fetchLandingData(cat);
        
        return { props: { cat, data: res?res.data:null , staticData } };
    } catch {
        return { props: { cat:null, data: null, staticData: null } };
    }
};


export default Category;