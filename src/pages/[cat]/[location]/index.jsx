import React from 'react'
import Hero from 'components/PageComponents/location-category/Hero';
import TopExpert from 'components/PageComponents/location-category/TopExpert';
import ReviewsBlock from 'components/PageComponents/location-category/Reviews';
import WhatInclude from 'components/PageComponents/location-category/WhatInclude';
import RecentJobs from 'components/PageComponents/location-category/RecentJobs';
import RelatedLink from 'components/PageComponents/location-category/RelatedLink';
import classes from './style.module.scss'
import { fetchLandingData } from 'api/general';
import DefaultErrorPage from 'next/error';
import HeaderTags from 'components/HeaderTags';
import { BASE_URL } from 'constants/config';

function Location({ cat, data: serverData, staticData, location }) {
    if (!staticData)
        return <DefaultErrorPage statusCode={404} />
    if (process.browser)
        document.body.classList.add(classes['location-page']);
    React.useEffect(() => {
        return (() => {
            document.body.classList.remove(classes['location-page']);
        });
    }, []);

    return (
        <>
            <HeaderTags
                title={staticData.meta.title}
                description={staticData.meta.description}
                image={BASE_URL.substr(0,BASE_URL.length-1) + staticData.meta.image}
                url={'/'+cat+'/'+location+'/'}
                schema={staticData.schema}
            />
            <Hero title={staticData.title} location={location} cat={cat}/>
            <TopExpert category={cat} staticData={staticData.topExpert} />
            <ReviewsBlock title={staticData.title} location={location} staticData={staticData.reviews} />
            <WhatInclude staticData={staticData.whatInclude} />
            {serverData &&
                <RecentJobs data={serverData} location={location} staticData={staticData.recentJobs} />
            }
            <RelatedLink title={staticData.title} staticData={staticData.related} />
        </>
    )
}
export async function getServerSideProps(ctx) {
    const { cat, location } = ctx.query;
    try {
        const staticData = require(`content/categories/${cat}.json`)
        let res = null
        if (staticData.hasServer)
            res = await fetchLandingData(cat, location);

        return { props: { cat, location, data: res ? res.data : null, staticData } };
    } catch {
        return { props: { cat, location, data: null, staticData: null } };
    }
};


export default Location;