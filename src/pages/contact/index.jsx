import React from 'react';
import { Row, Col, Button, Container } from "reactstrap";
import Lottie from "react-lottie";
import vector from 'assets/vector/animations/No Message.json';
import { useRouter } from 'next/router';
import HeaderTags from 'components/HeaderTags';
import Typography from 'components/Typography';
import Input from 'components/Input';
import { getUrlQuery } from 'utils';
import useIsMounted from 'react-is-mounted-hook';
import useToast from 'components/Toast/Toast';
import Spinner from 'components/Spinner';
import { submitContact } from 'api/general';
import { BASE_URL } from 'constants/config';

export default function Contact() {
    const isMounted = useIsMounted()
    const router = useRouter()
    const toast = useToast()

    const [IsSubmiting, setIsSubmiting] = React.useState(false)
    const [InputFields, setInputFields] = React.useState({})
    const [Errors, setErrors] = React.useState({})

    React.useEffect(() => {
        const { email, type } = getUrlQuery();
        setInputFields({ email, type })
    }, [])

    const handleChange = (e) => {
        setInputFields(inputFields => {
            return {
                ...inputFields,
                [e.target.name]: e.target.value
            }
        })

        setErrors(errors => {
            return {
                ...errors,
                [e.target.name]: undefined
            }
        })
    }
    const handleSubmit = (e) => {
        e.preventDefault()
        if (IsSubmiting)
            return;
        setIsSubmiting(true)
        submitContact(InputFields)
            .then(payload => {
                if (isMounted && payload.success) {
                    toast.showMessage("success", "Your message sent successfuly")
                    setInputFields({})
                }
            })
            .catch(reason => {
                if (isMounted && reason.response) {
                    const errorData = reason.response.data ? reason.response.data.error : {};
                    if (errorData.errors) setErrors(errorData.errors)
                    toast.showMessage('danger', errorData.message || "Something went wrong")
                }
            })
            .finally(() => {
                if (isMounted)
                    setIsSubmiting(false)
            })
    }
    return (
        <>
            <HeaderTags
                title="Jobtick | Contact"
                description="Contact with Jobtick"
                image={BASE_URL.substr(0,BASE_URL.length-1) +"/logo192.png"}
                url={router.asPath}
            />
            <Container>
                <Row className="contact">
                    <Col md={6}>
                        <form onSubmit={handleSubmit}>
                            <div className="box">
                                <Typography
                                    variant="body1"
                                   
                                    component="label"
                                    htmlFor="name"
                                >
                                    Full name
                                <sup>*</sup>
                                </Typography>
                                <Input
                                    name="name"
                                    type="text"
                                    value={InputFields.name}
                                    onChange={handleChange}
                                />
                                {Errors.name && <span className="input-error">{Errors.name[0]}</span>}
                            </div>

                            <div className="box">
                                <Typography
                                    variant="body1"
                                   
                                    component="label"
                                    htmlFor="email"
                                >
                                    Your email address
                                <sup>*</sup>
                                </Typography>
                                <Input
                                    type="email"
                                    name="email"
                                    value={InputFields.email}
                                    onChange={handleChange}
                                />
                                {Errors.email && <span className="input-error">{Errors.email[0]}</span>}
                            </div>

                            <div className="box">
                                <Typography
                                    variant="body1"
                                   
                                    component="label"
                                    htmlFor="subject"
                                >
                                    Subject
                                <sup>*</sup>
                                </Typography>
                                <Input
                                    type="text"
                                    name="subject"
                                    value={InputFields.subject}
                                    onChange={handleChange}
                                />
                                {Errors.subject && <span className="input-error">{Errors.subject[0]}</span>}
                            </div>
                            <div className="box">
                                <Typography
                                    variant="body1"
                                   
                                    component="label"
                                    htmlFor="description"
                                >
                                    Description
                                <sup>*</sup>
                                </Typography>
                                <Input
                                    type="textarea"
                                    name="description"
                                    value={InputFields.description}
                                    onChange={handleChange}
                                />
                                {Errors.description && <span className="input-error">{Errors.description[0]}</span>}
                            </div>
                            <div className="box actions">
                                {/*<Col>
                                <Button className="attach" size="lg" block>Attach file</Button>
                            </Col>*/}
                                <Col>
                                    <Button color="primary" size="lg" block>
                                        {IsSubmiting && <Spinner className="mr-3" type="button" />}
                                    Submit
                                </Button>
                                </Col>
                            </div>
                        </form>
                    </Col>
                    <Col md={6} className="animation">
                        <Lottie
                            width="auto"
                            height="auto"
                            options={
                                {
                                    loop: true,
                                    autoplay: true,
                                    animationData: vector,
                                    rendererSettings: {
                                        preserveAspectRatio: 'xMidYMid slice'
                                    }
                                }
                            }
                            isClickToPauseDisabled={true}
                        />
                    </Col>
                </Row>
            </Container>
        </>
    );
}
