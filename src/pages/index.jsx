import Home from "containers/home";
import { fetchCategories } from "api/job";
import { getBlogPosts } from "api/general";
// import categories from "constants/categories";

export async function getStaticProps() {
  // let categories = []
  let blogPosts = [];
  // try {
  //   const res = await fetchCategories();
  //   categories = res.data
  // } catch { }
  try {
    blogPosts = await getBlogPosts(12);
  } catch {}
  return { props: { blogPosts }, revalidate: 60 * 60 * 24 };
}

export default ({ blogPosts }) => <Home blogPosts={blogPosts} />;
