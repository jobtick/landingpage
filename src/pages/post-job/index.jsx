import PostJob from "components/PostJob";
import React from "react";
import HeaderTags from "components/HeaderTags";
import { BASE_URL } from "constants/config";

function CreateJob() {
  return (
    <>
      <HeaderTags
        title="Find home & office services or earn more - Jobtick"
        description="Visit jobtick.com for a handyman, cleaning, removals, delivery, computer & tech services and anything else in Australia"
        image={BASE_URL.substr(0, BASE_URL.length - 1) + "/logo192.png"}
        url={"/post-job"}
      >
        <meta name="viewport" content="width=device-width, user-scalable=no" />
      </HeaderTags>
      <PostJob />
    </>
  );
}

export default CreateJob;
