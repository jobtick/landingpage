import React from 'react';
import SupportData from 'constants/support';
import { useRouter } from 'next/router';
import { Col, Collapse, Container, Row } from 'reactstrap';
import Typography from 'components/Typography';
import { FiChevronRight } from 'react-icons/fi';
import Link from 'next/link';
import HeaderTags from 'components/HeaderTags';
import classes from './style.module.scss'
import DefaultErrorPage from 'next/error'
import { BASE_URL } from 'constants/config';

const Support = ({ category, question }) => {

    const slugGenerator = (text) => {
        return text.toLowerCase().replace(/[,?!()./%$']/g, '').trim().replace(/\s/g, '-')
    }

    const categories = SupportData.map(item => item.category).filter((value, index, self) => self.indexOf(value) === index)
    const ActiveIndex = categories.findIndex(item => slugGenerator(item) === category)

    const [Data, setData] = React.useState()

    React.useEffect(() => {
        if (question)
            setData(SupportData.find(item => slugGenerator(item.question) === question))
    }, [question])

    React.useEffect(() => {
        document.body.classList.add(classes['support-page']);
        return (() => {
            document.body.classList.remove(classes['support-page']);
        });
    }, []);

    const router = useRouter()

    if ((category && ActiveIndex === -1) || (question && !Data))
        return <DefaultErrorPage statusCode={404} />

    return (
        <>
            <HeaderTags
                title={`Jobtick | Support${Data ? " | " + Data.question : ''}`}
                description={Data ? Data.question.split(0, 100) : 'Get answer of your questions about Jobtick'}
                image={BASE_URL.substr(0,BASE_URL.length-1) +"/logo192.png"}
                url={router.asPath}
            />
            <Container>
                <div className="support">
                    <Row>
                        <Col md="5">

                            {categories.map((category, index) =>
                                <div className={`box${ActiveIndex === index ? ' active' : ''}`} key={index}>
                                    <Link href={ActiveIndex === index ? `/support` : `/support/${slugGenerator(category)}`}>
                                        <a className="category-header">
                                            <Typography component="h3" variant="body1">{category}</Typography>
                                            <span className="toggle-icon"><FiChevronRight /></span>
                                        </a>
                                    </Link>
                                    <Collapse isOpen={ActiveIndex === index}>
                                        {SupportData.filter(item => item.category === category).map((item) =>
                                            <Link href={`/support/${slugGenerator(item.category)}/${slugGenerator(item.question)}`} key={item.id}>
                                                <a>
                                                    <Typography component="span" variant="body3"
                                                        className={Data?.id === item.id ? "active topic" : "topic"}
                                                    >
                                                        {item.question}
                                                    </Typography>
                                                </a>
                                            </Link>
                                        )
                                        }
                                    </Collapse>
                                </div>
                            )}
                        </Col>
                        <Col md="7">
                            {Data &&
                                <div className="box ">
                                    <Typography component="h2" variant="sub-heading1" className="question">{Data.question}</Typography>
                                    <Typography component="div" variant="body1" className="answer">{Data.answer}</Typography>
                                </div>
                            }
                        </Col>
                    </Row>
                </div>
            </Container>
        </>
    );
};

export async function getServerSideProps(ctx) {
    const { category, question } = ctx.query;
    return { props: { category, question } };
};
export default Support;
