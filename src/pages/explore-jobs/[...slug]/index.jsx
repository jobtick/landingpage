import React from "react";
import FilterMenu from "components/PageComponents/explore-jobs/TaskFilter";
import TasksList from "components/PageComponents/explore-jobs/TasksList";
import TaskHandler from "components/PageComponents/explore-jobs/TaskHandler";
import useIsMounted from "react-is-mounted-hook";
import { fetchTask, fetchTasks } from "api/job";
import { FiArrowLeft, FiBriefcase, FiMapPin } from "react-icons/fi";
import TaskSearch from "components/PageComponents/explore-jobs/TaskSearch/TaskSearch";
import MapView from "components/PageComponents/explore-jobs/MapView";
import { useRouter } from "next/router";
import HeaderTags from "components/HeaderTags";
import { approximateCoordinate, useGeoLocation } from "utils";
import NoContent from "components/NoContent";
import { Button, Container } from "reactstrap";
import Cookies from "js-cookie";
import { BASE_URL } from "constants/config";
import useToast from "components/Toast/Toast";

const Tasks = ({ slug, TaskData, search }) => {
  const isMounted = useIsMounted();
  const toast = useToast();
  const router = useRouter();
  const geoLocation = useGeoLocation();
  const [InitailLocation, setInitailLocation] = React.useState(false);

  const taskList = React.createRef();
  const [IsLoading, setIsLoading] = React.useState(false);
  const [Tasks, setTasks] = React.useState([]);
  const [SearchFor, setSearchFor] = React.useState(search || "");
  const [Filter, setFilter] = React.useState({
    task_type: "all",
    min_price: 5,
    max_price: 9999,
    distance: 100,
    location: "Sydney",
    current_lng: 151.2093,
    current_lat: -33.8688,
  });
  const [PageNumber, setPageNumber] = React.useState(0);
  const [PageMeta, setPageMeta] = React.useState();

  const [Toggled, setToggled] = React.useState(slug ? true : false);
  const [Scrolled, setScrolled] = React.useState(false);

  if (process.browser) document.body.classList.add("explore-page");
  React.useEffect(() => {
    return () => {
      document.body.classList.remove("explore-page");
    };
  }, []);

  // React.useEffect(() => {
  //     Cookies.set('explore-filter', JSON.stringify(Filter))
  // }, [Filter])

  React.useEffect(() => {
    if (InitailLocation) return undefined;
    const locationInfo = geoLocation.getLocationInfo();
    if (locationInfo && !Filter.location) {
      setFilter({
        ...Filter,
        location: locationInfo.text,
        current_lng: locationInfo.longitude,
        current_lat: locationInfo.latitude,
        distance: 100,
      });
      setInitailLocation(true);
    }
  }, [geoLocation]);

  const getData = () => {
    if (PageNumber > 0) {
      setIsLoading(true);
      fetchTasks(SearchFor, undefined, Filter, PageNumber)
        .then((payload) => {
          if (isMounted) {
            setTasks([...Tasks, ...payload.data]);
            setPageMeta({
              pagesCount: payload.last_page,
              from: payload.from,
              to: payload.to,
              total: payload.total,
            });
          }
        })
        .catch((error) => {})
        .finally(() => {
          setIsLoading(false);
        });
    } else setPageNumber(1);
  };

  const handleScroll = React.useCallback(
    (el) => {
      if (PageNumber < PageMeta.pagesCount)
        if (
          el.target.scrollTop + el.target.clientHeight >=
          el.target.scrollHeight
        )
          setPageNumber(PageNumber + 1);

      if (document.body.offsetWidth < 860)
        setScrolled(el.target.scrollTop > 120);
      else setScrolled(false);
    },
    [PageNumber, PageMeta]
  );

  React.useEffect(() => {
    setTasks([]);
    setPageNumber(0);
  }, [Filter, SearchFor]);

  // eslint-disable-next-line
  React.useEffect(getData, [PageNumber]);

  React.useEffect(() => {
    let taskListRef = taskList.current;
    if (taskListRef)
      taskListRef.addEventListener("scroll", handleScroll, false);
    return () => {
      if (taskListRef)
        taskListRef.removeEventListener("scroll", handleScroll, false);
    };
    // eslint-disable-next-line
  }, [taskList, PageNumber, PageMeta]);

  const [IsMobile, setIsMobile] = React.useState(false);

  React.useEffect(() => {
    setToggled(slug || window.innerWidth > 992 ? true : false);
  }, [slug]);

  React.useEffect(() => {
    setIsMobile(slug || window.innerWidth < 992 ? true : false);
  }, [slug]);

  return (
    <>
      <HeaderTags
        title={
          TaskData?.data
            ? " Jobtick | Explore | " + TaskData.data.title
            : "Explore jobs that you are interested in, Complete them to get paid."
        }
        description={
          TaskData?.data
            ? TaskData.data.description
            : "Use your skills to earn money by completing jobs among thousands of tasks posted daily."
        }
        image={
          TaskData?.data?.attachments?.length > 0
            ? TaskData.data.attachments[0].url
            : BASE_URL.substr(0, BASE_URL.length - 1) +
              (TaskData?.data
                ? TaskData?.data?.task_type === "physical"
                  ? "/images/Offline.jpg"
                  : "/images/Online.jpg"
                : "/logo192.png")
        }
        url={router.asPath}
      />
      <Container>
        <div
          className={`tasks-toolbar${Toggled ? " toggled" : ""}${
            Scrolled ? " scrolled" : ""
          }`}
        >
          <FilterMenu data={Filter} onUpdate={setFilter} />

          {/* <TaskSearch
            styles={{}}
            value={SearchFor}
            onChange={(text) => setSearchFor(text)}
            searchToggle={SearchToggle}
            setSearchToggle={setSearchToggle}
          /> */}
        </div>

        <div
          className={`tasks-wrapper${Toggled ? " toggled" : ""}${
            Scrolled ? " scrolled" : ""
          }`}
        >
          <div className="tasks-sidebar">
            <TaskSearch
              styles={{}}
              value={SearchFor}
              onChange={(text) => setSearchFor(text)}
              mobile={IsMobile}
            />
            <div className="sidebar-header-filter">
              <FilterMenu data={Filter} onUpdate={setFilter} />
            </div>
            {IsLoading || Tasks.length > 0 ? (
              <TasksList
                forwardRef={taskList}
                selected={slug}
                isLoading={IsLoading}
                tasks={Tasks}
              />
            ) : (
              <NoContent icon={<FiBriefcase />} label="No job found" />
            )}
            <Button
              color="primary"
              className="show-map"
              onClick={() => setToggled(true)}
            >
              <FiMapPin size="20" />
            </Button>
          </div>
          <div className="task-container">
            {Toggled && (
              <div
                className="back-to-list"
                onClick={() => {
                  setToggled(false);
                  router.push("/explore-jobs");
                }}
              >
                <span>
                  <FiArrowLeft />
                </span>
              </div>
            )}
            {slug ? (
              <TaskHandler TaskData={TaskData} />
            ) : (
              Toggled && (
                <div className="map-container">
                  <MapView
                    center={
                      Filter.location
                        ? [Filter.current_lng, Filter.current_lat]
                        : undefined
                    }
                    toggleView={() => {}}
                    taskUrl={`/jobs/`}
                    mode="tasks"
                    data={Tasks.map((task) => ({
                      ...task,
                      latitude: approximateCoordinate(task.latitude),
                      longitude: approximateCoordinate(task.longitude),
                    }))}
                  />
                </div>
              )
            )}
          </div>
        </div>
      </Container>
    </>
  );
};

export async function getServerSideProps(ctx) {
  const { slug } = ctx.query;
  try {
    const res = await fetchTask(slug);
    return { props: { slug, TaskData: res } };
  } catch {
    return { props: { slug, TaskData: "not Valid" } };
  }
}
export default Tasks;
