import Tasks from "./[...slug]"
export async function getServerSideProps(ctx) {
    const { search } = ctx.query;
    return { props: { search: search || '' } };
};
export default Tasks;
