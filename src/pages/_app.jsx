import React from "react";
import "../sass/bundle.scss";
import Layout from "../layout";
import { useRouter } from "next/router";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import ReactNotification from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import CookieConsent from "react-cookie-consent";
import { Button } from "reactstrap";
import Typography from "components/Typography";
import { initializeApp } from "firebase/app";
import "firebase/analytics";

import { Provider } from "react-redux";
import reduxStore from "redux/store";
import { isDevelopment } from "utils";
import { firebase_config } from "utils/firebase";

NProgress.configure({
  minimum: 0.3,
  easing: "ease",
  speed: 800,
  showSpinner: true,
});

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  React.useEffect(() => {
    router.events.on("routeChangeStart", () => NProgress.start());
    router.events.on("routeChangeComplete", () => NProgress.done());
    router.events.on("routeChangeError", () => NProgress.done());
    return () => {
      router.events.off("routeChangeStart", () => NProgress.start());
      router.events.off("routeChangeComplete", () => NProgress.done());
      router.events.off("routeChangeError", () => NProgress.done());
    };
  }, []);

  React.useEffect(() => {
    //if (!isDevelopment) {
    const _app = initializeApp(firebase_config);
    //}
  }, []);

  return (
    <>
      <Provider store={reduxStore}>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </Provider>
      <ReactNotification />
      {router.pathname !== "/post-job" && (
        <CookieConsent
          disableStyles
          containerClasses="enable-cookie"
          enableDeclineButton
          buttonText="Accept"
          declineButtonText="Decline"
          declineButtonClasses="btn-outline-secondary"
          ButtonComponent={(props) => (
            <Button color="primary" outline {...props} />
          )}
        >
          <Typography>
            This website uses cookies to enhance the user experience. Please
            accept cookies from this site.
          </Typography>
        </CookieConsent>
      )}
      {/*typeof window !== 'undefined' && <LiveChat license={LIVECHAT_LICENCE_ID} />*/}
    </>
  );
}

export default MyApp;
