import React from 'react';
import { Col, Container, Row } from 'reactstrap';
import { fetchProfile } from 'api/user';
import ProfileMainInformation from 'components/PageComponents/profile/ProfileMainInformation';
import ProfileLevel from 'components/PageComponents/profile/ProfileLevel';
import ProfileReviews from 'components/PageComponents/profile/ProfileReviews';
import ProfileAboutMe from 'components/PageComponents/profile/ProfileAboutMe';
import ProfilePortfo from 'components/PageComponents/profile/ProfilePortfo';
import ProfileSkills from 'components/PageComponents/profile/ProfileSkills';
import NoContent from 'components/NoContent';
import { FiUser } from 'react-icons/fi';
import { useRouter } from 'next/router';
import HeaderTags from 'components/HeaderTags';
import { BASE_URL } from 'constants/config';

function Profile({ profile }) {
    const router = useRouter();

    if (process.browser)
        document.body.classList.add('profile-page');
    React.useEffect(() => {
        return (() => {
            document.body.classList.remove('profile-page');
        });
    }, []);

    return (
        <>
            <HeaderTags
                title={`Jobtick | Profile | ${profile?.name || 'Not found'}`}
                description={profile?.about || 'User not found'}
                image={BASE_URL.substr(0,BASE_URL.length-1) +(profile?.avatar?.thumb_url || "/logo192.png")}
                url={router.asPath}
            />
            <Container>
                {profile ?
                    <Row>
                        <Col lg={3}>
                            <ProfileMainInformation profile={profile} />
                            <ProfileLevel profile={profile} />
                            <ProfileReviews profile={profile} />
                        </Col>

                        <Col lg={6}>
                            <ProfileAboutMe profile={profile} />
                            <ProfilePortfo profile={profile} />
                        </Col>

                        <Col lg={3}>
                            <ProfileSkills profile={profile} />
                        </Col>
                    </Row>
                    :
                    <NoContent
                        icon={<FiUser />}
                        label="User not found"
                    />
                }
            </Container>
        </>
    )
}

export async function getServerSideProps(ctx) {
    const { id } = ctx.query;
    try {
        const res = await fetchProfile(id);
        return { props: { id, profile: res.data } };
    } catch {
        return { props: { id, profile: null } };
    }
};
export default Profile;