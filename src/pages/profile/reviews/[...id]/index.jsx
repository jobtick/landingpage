import React from 'react'
import Typography from 'components/Typography';
import { Button, Col, Container, Row } from 'reactstrap';
import ProfileAvatar from 'components/ProfileAvatar';
import StarRating from 'components/StarRating';
import moment from 'moment'
import { getLevelIconUrl, setUrlQuery } from 'utils';
import ButtonSelect from 'components/ButtonSelect';
import { round10 } from 'utils/typeTools';
import Pagination from 'components/Pagination/Pagination';
import NoContent from 'components/NoContent';
import { FiFrown, FiThumbsDown, FiThumbsUp } from 'react-icons/fi';
import { fetchProfile, fetchReviews } from 'api/user';
import Link from 'next/link';
import { useRouter } from 'next/router';
import HeaderTags from 'components/HeaderTags';
import { BASE_URL } from 'constants/config';

function Reviews({ profileRes, reviewsRes, role }) { // Role => 0: Ticker , 1: Poster
    const router = useRouter()

    const reviews = reviewsRes?.data || []
    const currentPage = reviewsRes?.meta.current_page
    const totalPages = reviewsRes?.meta.last_page

    const profile = profileRes?.data
    const ratings = role === 0 ? profile?.worker_ratings : profile?.poster_ratings

    const setRole = (value) => {
        router.push(window.location.pathname + '?' + setUrlQuery('role', value === 0 ? 'ticker' : 'poster'))
    }

    const setCurrentPage = (value) => {
        router.push(window.location.pathname + '?' + setUrlQuery('page', value))
    }

    const RatingValueCounts = ({ number, count, total }) => {
        return (
            <div className="rating-count-item">
                <Typography component="span" variant="body1" className="rating-number">{number}</Typography>
                <div className="percent-veiw">
                    <span style={{ width: count > 0 ? (count / total) * 100 + '%' : '0%' }} />
                </div>
                <Typography component="span" variant="body1" className="rating-count">({count})</Typography>
            </div>
        )
    }

    return (
        <>
            <HeaderTags
                title={`Jobtick | Reviews | ${profile?.name || 'Not found'}`}
                description={profile?.about || 'User not found'}
                image={BASE_URL.substr(0,BASE_URL.length-1) +(profile?.avatar?.thumb_url || "/logo192.png")}
                url={router.asPath}
            />
            <Container>
                <div className="content-wrapper reviews-wapper">
                    <Typography component="h2" variant="dsp-medium" className="page-title">Reviews</Typography>

                    <div className="tabs">
                        {['As Ticker', 'As Poster'].map((label, index) =>
                            <Button component="Button" key={index} className={`tab${role === index ? ' active' : ''}`} color="link" onClick={() => setRole(index)}>
                                <Typography component="span" variant="caption1">
                                    {label}
                                </Typography>
                            </Button>
                        )}
                    </div>
                    <Row>
                        <Col xs={12} md={5} lg={3}>
                            {profile &&
                                <div className="ratings-container">
                                    <ProfileAvatar profile={profile} />
                                    <Typography component="span" variant="sub-heading1" className="user-name">{profile.name}</Typography>
                                    <Typography component="span" variant="caption1" className="user-location">{profile.location}</Typography>
                                    <span className="user-level" >
                                        <img src={getLevelIconUrl(role === 0 ? profile.worker_tier?.id : profile.poster_tier?.id)} alt={`Level ${(role === 0 ? profile.worker_tier?.id : profile.poster_tier?.id) || "..."}`} />
                                    </span>
                                    <ButtonSelect
                                        value={role}
                                        onChange={(value) => setRole(value)}
                                        items={
                                            [
                                                {
                                                    label: 'As a Ticker',
                                                    value: 0
                                                },
                                                {
                                                    label: 'As a Poster',
                                                    value: 1
                                                }
                                            ]
                                        } />
                                    <div className="average-rating">
                                        {ratings ?
                                            <>
                                                <StarRating value={ratings.avg_rating || 0} disabled />
                                                <Typography component="span" variant="body1" className="user-avg-rating">{round10(ratings.avg_rating || 0)}</Typography>
                                                <Typography component="span" variant="caption1" className="user-ratings-received">{ratings.received_reviews || 0} Reviews</Typography>

                                            </>
                                            :
                                            <Typography component="span" variant="body1" className="user-avg-rating">No review</Typography>
                                        }
                                    </div>
                                    <div className="rating-counts">
                                        {[5, 4, 3, 2, 1].map(number =>
                                            <RatingValueCounts
                                                key={number}
                                                number={number}
                                                count={ratings?.rating_breakdown[number] || 0}
                                                total={ratings?.received_reviews || 0}
                                            />
                                        )}
                                    </div>
                                </div>
                            }
                            {!profile &&
                                <NoContent icon={<FiFrown />} label="User not found" />
                            }
                        </Col>
                        <Col xs={12} md={7} lg={9} className="reviews-container">

                            {reviews.map((item, index) =>
                                <div key={index} className="review-item">
                                    <div className="review-header flex-row">
                                        <Link href={`/profile/${item.rater.id}`}>
                                            <a target="_blank">
                                                <ProfileAvatar
                                                    profile={item.rater}
                                                />
                                            </a>
                                        </Link>
                                        <div className="job-info app-flex-column">
                                            <Link href={`/explore-jobs/${item.task.slug}`} >
                                                <a target="_blank">
                                                    <Typography component="span" variant="body3" className="job-title">{item.task.title}</Typography>
                                                </a>
                                            </Link>
                                            <Typography component="span" variant="caption1" className="user-role">{item.ratee_type === 'poster' ? 'Poster' : 'Ticker'}</Typography>
                                        </div>
                                        <div className="rate-info app-flex-column">
                                            <StarRating value={item.rating} disabled />
                                            <Typography component="span" variant="caption1" className="creation-date">
                                                {moment(item.created_at).format('DD-MM-YYYY HH:mm:ss')}
                                            </Typography>
                                        </div>
                                    </div>
                                    <Typography component="p" variant="body3" className="content">{item.message}</Typography>
                                </div>
                            )}
                            {currentPage && totalPages > 1 &&
                                <Pagination
                                    active={currentPage}
                                    count={totalPages}
                                    onChange={setCurrentPage}
                                />
                            }
                            {reviews.length === 0 &&
                                <NoContent icon={<><FiThumbsUp /><FiThumbsDown /></>} label="No reviews" />
                            }
                        </Col>
                    </Row>
                </div>
            </Container>
        </>
    )
}

export async function getServerSideProps(ctx) {
    const { id, role, page } = ctx.query;
    try {
        const profileRes = await fetchProfile(id);
        const reviewsRes = await fetchReviews(id, role === 'poster' ? 'poster' : 'worker');
        return { props: { role: role === 'poster' ? 1 : 0, profileRes, reviewsRes } };
    } catch {
        return { props: { role: null, profileRes: null, reviewsRes: null } };
    }
};

export default Reviews