import React from "react";
import Typography from "components/Typography";
import { Button, Container } from "reactstrap";
import Link from "next/link";
import classes from "./style.module.scss";
import { useRouter } from "next/router";

function DiscountBanner() {
  const { asPath } = useRouter();
  const discountBannerData = require("./discountBanner.json");
  const [Close, setClose] = React.useState(false);

  if (asPath !== "/") return <></>;
  return (
    <div
      className={[
        classes["discount-banner"],
        Close ? classes["closed"] : "",
      ].join(" ")}
    >
      <Container>
        <div className="discount-content">
          <Typography variant="body2" className="blue-days">
            {discountBannerData.title}{" "}
          </Typography>
          <div className="discount-content-center">
            <div className="discount-content-center-top">
              <Typography variant="heading2">
                {discountBannerData.titleP1}
              </Typography>
              <Typography variant="body2">
                {discountBannerData.titleP2}{" "}
              </Typography>
              <Typography variant="body2" className="Code">
                {discountBannerData.titleP3}{" "}
              </Typography>
            </div>
            <Typography variant="body2">
              {discountBannerData.titleP4}{" "}
            </Typography>
          </div>
          <div>
            <Link href="/post-job">
              <a>
                <Button color="primary" size="lg" className="getStarted">
                  <Typography variant="sub-heading1">
                    {discountBannerData.btnLabel}
                  </Typography>
                </Button>
              </a>
            </Link>
          </div>
        </div>
        <div className="discount-content mobile">
          <Typography variant="body2" className="blue-days">
            {discountBannerData.title}{" "}
          </Typography>
          <div className="discount-content-center ">
            <div className="discount-content-center-top">
              <div className="discount-content-center-top-left">
                <Typography variant="heading2">
                  {discountBannerData.titleP1}
                </Typography>
                <Typography variant="body2">
                  {discountBannerData.titleP2}{" "}
                </Typography>
              </div>
              <div className="discount-content-center-top-right">
                <Typography variant="body2" className="Code">
                  {discountBannerData.titleP3}{" "}
                </Typography>
              </div>
            </div>
            <div className="discount-content-bottom">
              <Typography variant="body2" className="content">
                {discountBannerData.titleP4}{" "}
              </Typography>
              <div className="discount-content-btn">
                <Link href="/post-job">
                  <a>
                    <Button color="primary" size="lg" className="getStarted">
                      <Typography variant="sub-heading1">
                        {discountBannerData.btnLabel}
                      </Typography>
                    </Button>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </Container>
      <Button close onClick={setClose} className="btn-close" />
    </div>
  );
}
export default DiscountBanner;
