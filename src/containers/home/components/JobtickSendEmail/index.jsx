import Typography from "components/Typography";
import React, { useEffect, useState } from "react";
import Lottie from "react-lottie";
import ReactMultiSelectCheckboxes from "react-multiselect-checkboxes";
import useIsMounted from "react-is-mounted-hook";
import useToast from "components/Toast/Toast";
import Spinner from "components/Spinner";
import { FormGroup, Button, Container } from "reactstrap";
import Input from "components/Input";
import classes from "./style.module.scss";
import { fetchSkills, submitEmail } from "api/home";
import { formatPlaceName } from "utils/typeTools";
import LocationInput from "components/LocationInput";
import { ValidateEmail } from "utils/typeTools";
import Image from 'components/Image';
// import Image from "next/image";

function JobtickSendEmail () {
  const isMounted = useIsMounted();
  const toast = useToast();
  const sendEmailData = require("./jobtickSendEmail.json");
  const [IsProcessing, setIsProcessing] = React.useState(false);
  const [SkillsString, setSkillsString] = React.useState("");
  const [InputFields, setInputFields] = React.useState({
    email: "",
    skills: "",
    location: "",
  });
  const [Errors, setErrors] = React.useState({});
  const [skillItems, setSkillItems] = useState([]);
  const [selectedOptions, setSelectedOptions] = useState([]);
  const [elements, setElements] = useState([]);
  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      borderBottom: "1px dotted pink",
      color: "#2b3240!important",
      padding: "12px 26px 12px 0",
      display: "flex",
      justifyContent: "space-between",
      direction: "rtl",
      background: "#fff !important",
      fontWeight: "400",
      fontSize: "14px",
      lineHeight: "18px",
    }),
    control: () => ({
      // none of react-select's styles are passed to <Control />
      display: "none",
    }),
    menu: () => ({
      // none of react-select's styles are passed to <Control />
      boxShadow: "none !important",
    }),

    singleValue: (provided, state) => {
      const opacity = state.isDisabled ? 0.5 : 1;
      const transition = "opacity 300ms";
      const background = "green";

      return { ...provided, opacity, transition, background };
    },
  };
  useEffect(() => {
    fillSillItems();
    setElements(Array.from(document.getElementsByClassName("css-1v99tuv")));
  }, []);

  useEffect(() => {
    setInputFields({ ...InputFields, skills: SkillsString });
  }, [SkillsString]);

  const fillSillItems = async () => {
    const { data } = await fetchSkills();
    const newSkills = [];
    data?.map((item) => {
      return newSkills.push({
        value: item.title,
        label: item.title,
      });
    });
    setSkillItems(newSkills);
  };

  const handleChange = (e) => {
    setInputFields({ ...InputFields, [e.target.name]: e.target.value });
    setErrors((value) => {
      return {
        ...value,
        [e.target.name]: undefined,
      };
    });
  };
  function getDropdownButtonLabel ({ placeholderButtonLabel, value }) {
    let newLabelDropDown = placeholderButtonLabel;
    let labelDropDown = "";

    const copySelected = [...selectedOptions];
    copySelected.map((option) => {
      labelDropDown += " " + option.value + ",";
      return labelDropDown;
    });
    if (copySelected.length == 0) {
      elements.map((element) => {
        element.style.cssText = "color:#8993A4 !important";
      });
      setSkillsString("");
      return `${newLabelDropDown}`;
    } else {
      elements.map((element) => {
        element.style.cssText = "color:#2B3240 !important";
      });
      setSkillsString(labelDropDown);
      return `${labelDropDown}`;
    }
  }
  function onChange (value, event) {
    if (event.action === "select-option" && event.option.value === "*") {
      this.setState(skillItems);
    } else if (
      event.action === "deselect-option" &&
      event.option.value === "*"
    ) {
      this.setState([]);
    } else if (event.action === "deselect-option") {
      this.setState(value.filter((o) => o.value !== "*"));
    } else if (value.length === this.options.length - 1) {
      this.setState(skillItems);
    } else {
      this.setState(value);
    }
  }
  const handleLocation = (location) => {
    if (!location.id) return undefined;
    const { coordinates } = location.geometry;

    if (Errors.location)
      setErrors((errors) => {
        return { ...errors, location: undefined };
      });
    setInputFields((data) => {
      return {
        ...data,
        location: formatPlaceName(location.place_name),
        latitude: coordinates[1],
        longitude: coordinates[0],
      };
    });
  };
  const handleSubmit = (e) => {
    console.log(InputFields);
    if (e) {
      e.preventDefault();
      e.stopPropagation();
    }
    if (IsProcessing) return undefined;

    setIsProcessing(true);
    let verify = true;
    if (InputFields.email && InputFields.email !== "") {
      if (!ValidateEmail(InputFields.email)) {
        setErrors((errors) => {
          return {
            ...errors,
            email: ["Email address is not valid"],
          };
        });
        verify = false;
      }
    } else {
      setErrors((errors) => {
        return {
          ...errors,
          email: ["Email address is required"],
        };
      });
      verify = false;
    }
    if (!verify) {
      setIsProcessing(false);
      return undefined;
    }

    submitEmail({
      ...InputFields,
    })
      .then((payload) => {
        //  console.log(payload);
        if (isMounted && payload.success) {
          toast.showMessage("success", payload.message || "Thanks");
        }
      })
      .catch((reason) => {
        // console.log(isMounted);
        if (isMounted && reason.response) {
          const errorData = reason.response.data
            ? reason.response.data.error
            : {};
          console.log(reason.response.data);
          if (errorData?.errors) setErrors(errorData?.errors);
          toast.showMessage(
            "danger",
            errorData?.message || "Something went wrong"
          );
        }
      })
      .finally(() => {
        if (isMounted) setIsProcessing(false);
      });
  };
  return (
    <div className={classes["sendEmail"]}>
      <Container>
        <div className="sendEmail-image">
          {/* <Lottie
            className="animation"
            width="auto"
            height="490px"
            options={{
              loop: true,
              autoplay: true,
              animationData: require("assets/vector/animations/" +
                "No Notification.json"),
            }}
            isClickToPauseDisabled={true}
          /> */}
          <Image
            src={require("assets/vector/animations/gif/No Notification.gif")}
          />
        </div>
        <div className="sendEmail-content">
          <Typography variant="heading3" component="h3">
            {sendEmailData.title}
          </Typography>
          <form onSubmit={handleSubmit} className="sendEmail-form">
            <div className="sendEmail-formGroup-container">
              <FormGroup>
                <Input
                  type="email"
                  name="email"
                  value={InputFields.email || ""}
                  onChange={handleChange}
                  error={Errors.email && true}
                  placeholder={sendEmailData["email-placeholder"]}
                />
                {Errors.email && (
                  <span className="input-error">{Errors.email[0]}</span>
                )}
              </FormGroup>
              <FormGroup>
                <LocationInput
                  name="location"
                  placeholder="location"
                  error={
                    (Errors.location && Errors.location[0]) ||
                    (Errors.latitude && Errors.latitude[0]) ||
                    (Errors.longitude && Errors.longitude[0])
                  }
                  onChange={handleLocation}
                  value={InputFields.location || ""}
                  autoplay="false"
                />
              </FormGroup>
            </div>

            <FormGroup>
              <ReactMultiSelectCheckboxes
                styles={customStyles}
                options={[...skillItems]}
                placeholderButtonLabel={sendEmailData["dropdown-placeholder"]}
                getDropdownButtonLabel={getDropdownButtonLabel}
                value={selectedOptions}
                onChange={onChange}
                setState={setSelectedOptions}
                classNamePrefix="dropdown-container"
              />
            </FormGroup>
            <Button
              color="primary"
              className="send-mail-btn"
              type="submit"
              size="lg"
              block
            >
              {IsProcessing ? (
                <Spinner
                  type="button"
                  className="mr-3"
                  style={{ borderTopColor: "#fff" }}
                />
              ) : (
                <Typography variant="sub-heading1">
                  {sendEmailData["btn-label"]}
                </Typography>
              )}
            </Button>
          </form>
        </div>
      </Container>
    </div>
  );
}

export default JobtickSendEmail;
