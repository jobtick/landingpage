import Typography from "components/Typography";
import React from "react";
import { FiChevronLeft, FiChevronRight, FiClock, FiEye } from "react-icons/fi";
import { Button, Container } from "reactstrap";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import moment from "moment";
import Link from "next/link";
import { BLOG_URL } from "constants/config";
import classes from "./style.module.scss";
import Image from 'components/Image';
// import Image from "next/image";

function Blog ({ posts }) {
  const blogData = require("./blog.json");
  var settings = {
    arrows: true,
    centerMode: false,
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    initialSlide: 0,
    swipeToSlide: true,
    autoplay: false,
    autoplaySpeed: 2000,
    variableWidth: true,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          arrows: true,
          centerMode: false,
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 992,
        settings: {
          arrows: true,
          centerMode: false,
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToShow: 2,
          slidesToScroll: 1,
          centerMode: false,
          dots: false,
        },
      },
      {
        breakpoint: 576,
        settings: {
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: false,
          dots: false,
        },
      },
    ],
  };
  return (
    <div className={classes.blog}>
      <Container>
        <div className="header data-row">
          <div>
            <Typography component="h3" variant="heading3">
              {blogData.title}
            </Typography>
            <div className="btn-wrapper">
              <Typography component="span" variant="body1">
                {blogData.description}
              </Typography>
              <Link href={BLOG_URL}>
                <a target="_blank" className="see-more-responsive">
                  <Typography component="span" variant="sub-heading1">
                    {blogData.button}
                  </Typography>
                </a>
              </Link>
            </div>
          </div>

          <Link href={BLOG_URL}>
            <a target="_blank">
              <Button color="info" className="see-more">
                <Typography component="span" variant="sub-heading1">
                  {blogData.button}
                </Typography>
              </Button>
            </a>
          </Link>
        </div>
        <Slider {...settings}>
          {posts?.map((item, index) => (
            <Link href={item.link} key={index}>
              <a className="item" target="_blank">
                <div className="wrapper">
                  <div className="image-box">
                    <div className="post-image">
                      <Image
                        src={
                          item["_embedded"]["wp:featuredmedia"][0].source_url
                        }
                        alt={item.title.rendered}
                        width={284}
                        height={214}
                      />
                    </div>
                    {item.popular && (
                      <div className="popular">
                        <img src="/vector/home/popular.svg" />
                      </div>
                    )}
                  </div>
                  <div className="content ">
                    <div className="description">
                      <Typography
                        component="h4"
                        variant="sub-heading1"
                        dangerouslySetInnerHTML={{
                          __html: item.title.rendered,
                        }}
                      />
                      <Typography
                        component="div"
                        className="post-intro"
                        variant="body3"
                        dangerouslySetInnerHTML={{
                          __html: item.excerpt.rendered,
                        }}
                      />
                    </div>
                    <div className="info-box">
                      <img src={item._embedded.author[0].avatar_urls["96"]} />
                      <div className="account-info data-row">
                        <div className="user-info ">
                          <Typography component="span" variant="sub-heading2">
                            {item._embedded.author[0].name}
                          </Typography>
                          <Typography variant="caption1">
                            {moment(item.date).format("DD MMM YYYY")}
                          </Typography>
                        </div>
                        <div className="date ">
                          <div>
                            <FiEye />
                            <Typography variant="caption1">
                              {item.gillion_post_views}
                            </Typography>
                          </div>
                          <div>
                            <FiClock />
                            <Typography variant="caption1">
                              {
                                item[
                                "_yoast_wpseo_estimated-reading-time-minutes"
                                ]
                              }{" "}
                              min
                            </Typography>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </Link>
          ))}
        </Slider>
      </Container>
    </div>
  );
}

export default Blog;
function NextArrow (props) {
  const { className, onClick } = props;
  return (
    <div className={className} onClick={onClick}>
      <FiChevronRight />
    </div>
  );
}

function PrevArrow (props) {
  const { className, onClick } = props;
  return (
    <div className={className} onClick={onClick}>
      <FiChevronLeft />
    </div>
  );
}
