import React from "react";
import Typography from "components/Typography";
import { FiChevronDown, FiChevronUp } from "react-icons/fi";
import { Col, Row, Container } from "reactstrap";
import classes from "./style.module.scss";
import Image from 'components/Image';

function FAQ () {
  const [Active, setActive] = React.useState();
  const faqData = require("./faq.json");
  return (
    <div className={classes.faq}>
      <Container>
        <Typography component="h3" variant="heading3">
          {faqData.title}
        </Typography>
        <div className="questions">
          {faqData.items.map((item, index) => (
            <div
              className={`item-wrapper${index === Active ? " active" : ""}`}
              key={index}
            >
              <div
                className="toggler"
                onClick={() =>
                  setActive((value) => (value === index ? undefined : index))
                }
              >
                <Typography variant="sub-heading1" className="question-text">
                  {item.question}
                </Typography>
                <span className="icon">
                  {index === Active ? (
                    <FiChevronUp size="16" />
                  ) : (
                    <FiChevronDown size="16" />
                  )}
                </span>
              </div>
              <div className="answer-text">
                <Typography component="div" variant="sub-heading2">
                  {item.answer}
                </Typography>
                {item.id == "4" ? (
                  <Row className="answer-content">
                    {item.contents.map((contentItem) => (
                      <Col
                        lg={6}
                        md={6}
                        sm={12}
                        xs={12}
                        className="answer-content-item-container"
                      >
                        <div className={"answer-content-item "}>
                          {/* <img src={contentItem.img} /> */}
                          <Image src={contentItem.img} width="60" height="60" />
                          <div className="data-row">
                            <div className="level-info ">
                              <div className="title-container">
                                <Typography
                                  component="div"
                                  variant="sub-heading1"
                                >
                                  {contentItem.title}
                                </Typography>
                                <Typography component="span" variant="caption2">
                                  {contentItem.hint}
                                </Typography>
                              </div>

                              <Typography variant="body3">
                                {contentItem.description}
                              </Typography>
                            </div>
                          </div>
                        </div>
                      </Col>
                    ))}
                  </Row>
                ) : (
                  ""
                )}
              </div>
            </div>
          ))}
        </div>
      </Container>
    </div>
  );
}

export default FAQ;
