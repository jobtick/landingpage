import Typography from "components/Typography";
import React from "react";
import Lottie from "react-lottie";
import { Button, Col, Container } from "reactstrap";
import Poster from "./components/Poster";
import Ticker from "./components/Ticker";
import classes from "./style.module.scss";
import Link from "next/link";

function HowJobtickWork() {
  const howData = require("./howJobtickWork.json");
  return (
    <div className={classes["how-it-works"]}>
      <Container>
        <div className="header">
          <Typography variant="heading3" component="h3">
            {howData.title}
          </Typography>
          <Typography variant="body1" component="p">
            {howData.description}
          </Typography>
        </div>
        <Poster />
        <Ticker />
      </Container>
    </div>
  );
}

export default HowJobtickWork;
