import Typography from "components/Typography";
import React from "react";
import Lottie from "react-lottie";
import { Button, Col, Row, Container } from "reactstrap";
import classes from "./style.module.scss";
import Link from "next/link";
import Image from 'components/Image';
// import Image from "next/image";

function Poster () {
  const posterData = require("./poster.json");
  return (
    <div className={classes["poster"]}>
      <div className="poster-container">
        <div className="poster-header">
          <Typography variant="heading2" component="h3">
            As a <span>Poster</span>
          </Typography>
          <Link href="/post-job">
            <a>
              <Button color="primary" size="lg" className="Getstartednow">
                <Typography variant="heading4">{posterData.button}</Typography>
              </Button>
            </a>
          </Link>
        </div>
        <Typography variant="body1" className="subheading">
          {posterData.description}
        </Typography>
      </div>
      <div className="items">
        {posterData?.steps.map((step, index) => (
          <Col lg={4} md={6} sm={6} xs={12} className="step-item" key={index}>
            <div className="frame">
              {/* <Lottie
                className="animation"
                width="auto"
                height="280px"
                options={{
                  loop: true,
                  autoplay: true,
                  animationData: require("assets/vector/animations/" +
                    step.lottie +
                    ".json"),
                }}
                isClickToPauseDisabled={true}
              /> */}
              <Image
                src={require("assets/vector/animations/gif/" +
                  step.lottie +
                  ".gif")}
              />
            </div>
            <div className="description">
              <Typography component="h3" variant="heading3">
                {index + 1}.{" "}
                {index + 1 != 3 ? (
                  <>
                    {" "}
                    <span>{step.titleFw}</span>
                    {step.title}
                  </>
                ) : (
                  <>
                    {step.titleFw}
                    <span>{step.title}</span>
                  </>
                )}
              </Typography>
              <Typography component="span" variant="body1">
                {step.content}.{" "}
              </Typography>
            </div>
          </Col>
        ))}
      </div>
    </div>
  );
}

export default Poster;
