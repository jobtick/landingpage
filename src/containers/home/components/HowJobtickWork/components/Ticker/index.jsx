import Typography from "components/Typography";
import React from "react";
import Lottie from "react-lottie";
import { Button, Col, Row, Container } from "reactstrap";
import classes from "./style.module.scss";
import Link from "next/link";
import Image from 'components/Image';
// import Image from "next/image";

function Ticker () {
  const tickerData = require("./ticker.json");

  return (
    <div className={classes["ticker"]}>
      <div className="ticker-container">
        <div className="ticker-header">
          <Typography variant="heading2" component="h3">
            As a <span>Ticker</span>
          </Typography>
          <Link href="/explore-jobs">
            <a>
              <Button color="primary" size="lg" className="Getstartednow">
                <Typography variant="heading4">{tickerData.button}</Typography>
              </Button>
            </a>
          </Link>
        </div>
        <Typography variant="body1" className="subheading">
          {tickerData.description}
        </Typography>
      </div>
      <div className="items">
        {tickerData?.steps.map((step, index) => (
          <Col lg={4} md={6} sm={6} xs={12} className="step-item ">
            <div className="frame">
              {/* <Lottie
                className="animation"
                width="auto"
                height="280px"
                options={{
                  loop: true,
                  autoplay: true,
                  animationData: require("assets/vector/animations/" +
                    step.lottie +
                    ".json"),
                }}
                isClickToPauseDisabled={true}
              /> */}
              <Image
                src={require("assets/vector/animations/gif/" +
                  step.lottie +
                  ".gif")}
              />
            </div>
            <div className="description">
              <Typography component="h3" variant="heading3">
                {index + 1}.{" "}
                {index + 1 == 1 ? (
                  <>
                    <div className="bottom-line  ">
                      <span>{step.titleFw}</span>
                      <img src="/vector/home/how-it-work/line.svg" />
                    </div>
                    {step.title}
                  </>
                ) : (
                  <>
                    {step.titleFw}
                    <div className="bottom-line responsive-line ">
                      <span>{step.title}</span>
                      <img src="/vector/home/how-it-work/line.svg" />
                    </div>
                  </>
                )}
              </Typography>
              <Typography component="span" variant="body1">
                {step.content}.{" "}
              </Typography>
            </div>
          </Col>
        ))}
      </div>
    </div>
  );
}

export default Ticker;
