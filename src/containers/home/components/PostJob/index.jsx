import React from "react";
import { Col, Row, Container } from "reactstrap";
import Typography from "components/Typography";
import classes from "./style.module.scss";
import Link from "next/link";
import { getMobileOperatingSystem } from "utils";
import Image from 'components/Image';
function PostJob () {
  const postJobData = require("./postJob.json");

  return (
    <div className={classes["post-job"]}>
      <Container>
        <div className="header">
          <Typography component="h2" variant="heading3">
            {postJobData.title}
          </Typography>
        </div>
        <Row>
          {postJobData.item?.map(({ category, description }, index) => (
            <Col
              lg={3}
              md={3}
              sm={4}
              xs={6}
              className={"category-item jt-category-" + category}
            >
              <Link href="/post-job">
                <a className="link-container">
                  {/* <img
                    src={`/images/home/post-job-n/${category
                      .toLowerCase()
                      .replace(/\s/g, "-")
                      .replace(/[&]/g, "and")}.svg`}
                    alt={category}
                    // className="img-box"
                    width={56}
                    height={57}
                  /> */}
                  <Image src={`/images/home/post-job-n/${category
                    .toLowerCase()
                    .replace(/\s/g, "-")
                    .replace(/[&]/g, "and")}.svg`}
                    alt={category}
                    className="img-box"
                    width={56}
                    height={57} />
                  <Typography variant="sub-heading1" className="category-name">
                    {category}
                  </Typography>
                  <Typography
                    variant="caption1"
                    className="category-description"
                  >
                    {description}
                  </Typography>
                </a>
              </Link>
            </Col>
          ))}
        </Row>
      </Container>
    </div>
  );
}
export default PostJob;
