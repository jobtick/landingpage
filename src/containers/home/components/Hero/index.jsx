import React from "react";
import { Button, Container } from "reactstrap";
import Link from "next/link";
import Typography from "components/Typography";
import classes from "./style.module.scss";
export default function Hero() {
  const heroData = require("./hero.json");
  return (
    <div className={classes["hero"]}>
      <Container>
        <div className="hero-content">
          <Typography component="h2" variant="heading0">
            <span className="primary-text">{heroData.titleP1}</span>{" "}
            {heroData.titleP2}
            <br />
            <span className="warning-text">{heroData.titleP3}</span>{" "}
            {heroData.titleP4}
          </Typography>
          <div
            style={{ backgroundImage: `url(${heroData.mobileImage})` }}
            className="hero-image-mobile"
          />
          <Typography component="p" variant="body2">
            {heroData.description}
          </Typography>
          <div className="role-select">
            <Link href="/explore-jobs">
              <a>
                <Button color="primary" size="lg" className=" jt-explore-hero">
                  <Typography variant="heading4" className="btn-desktop">
                    {heroData.btnExplore}
                  </Typography>
                </Button>
              </a>
            </Link>
            <div className="or-part">
              <Typography variant="sub-heading2">or</Typography>
            </div>
            <Link href="/post-job">
              <a>
                <Button color="info" size="lg" className=" jt-post-job-hero">
                  <Typography variant="heading4" className="btn-desktop">
                    {heroData.btnPostJob}
                  </Typography>
                </Button>
              </a>
            </Link>
          </div>
        </div>
        <div
          style={{ backgroundImage: `url(${heroData.image})` }}
          className="hero-image"
        />
      </Container>
    </div>
  );
}
