import StarRating from "components/StarRating";
import Typography from "components/Typography";
import React from "react";
import { FaQuoteRight } from "react-icons/fa";
import { Button, Container } from "reactstrap";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import classes from "./style.module.scss";
import { FiChevronLeft, FiChevronRight } from "react-icons/fi";
// import Image from "next/image";
import Link from "next/link";
import Image from 'components/Image';

function Testimonials () {
  const reviews = require("content/home/testimonials.json");
  const [Active, setActive] = React.useState(0);
  var settings = {
    arrows: true,
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: false,
    focusOnSelect: false,
    autoplay: false,
    autoplaySpeed: 5000,
    variableWidth: true,
    afterChange: (current) => setActive(current),
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          nextArrow: <NextArrow />,
          prevArrow: <PrevArrow />,
        },
      },
      {
        breakpoint: 992,
        settings: {
          arrows: false,
          centerMode: true,
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  return (
    <div className={classes.testimonials}>
      <Container>
        <div className="header">
          <Typography variant="heading3" component="h3">
            Discover the community
          </Typography>
          <Typography variant="body3">
            Real stories from over 1000 happy Posters who met the Tickers.
          </Typography>
        </div>

        <ReviewItem review={reviews[Active]} className="responsive" />
      </Container>
      <Slider {...settings}>
        {reviews.map((review, index) => (
          <React.Fragment key={index}>
            <div className="avatar-slide">
              <Image
                key={index}
                src={review.avatar}
                alt={review.fname + " " + review.lname}
                width="360"
                height="450"
                quality={100}
              />
            </div>
            <ReviewItem review={review} className="responsive" />
          </React.Fragment>
        ))}
      </Slider>
      <div className={`${classes.started} btn-container`}>
        <Typography component="h2" variant="heading4">
          Have a job to get done?
        </Typography>
        <Link href={"/post-job"}>
          <Button
            color="primary"
            size="lg"
            className={classes.btn + " jt-post-job-testimonials"}
          >
            <Typography variant="heading4">Get started now</Typography>
          </Button>
        </Link>
      </div>
    </div>
  );
}
export default Testimonials;

function NextArrow (props) {
  const { className, onClick } = props;
  return (
    <div className={className} onClick={onClick}>
      <FiChevronRight />
    </div>
  );
}

function PrevArrow (props) {
  const { className, onClick } = props;
  return (
    <div className={className} onClick={onClick}>
      <FiChevronLeft />
    </div>
  );
}

function ReviewItem ({ review, className }) {
  if (!review) return <></>;
  return (
    <div className={`review${className ? " " + className : undefined}`}>
      <FaQuoteRight className="icon" />
      <Typography component="h4" variant="heading4">
        {review.title}
      </Typography>
      <Typography component="p" variant="body3">
        {review.message}
      </Typography>
      <div className="description">
        <div className="writer">
          <div className="avatar">
            <Image
              src={review.avatar}
              alt={review.fname + " " + review.lname}
              width={65}
              height={65}
            />
          </div>
          <div>
            <Typography component="span" variant="sub-heading2">
              {review.fname + " " + review.lname}
            </Typography>
            <Typography component="span" variant="caption2" className="role">
              {review.role}
            </Typography>
            <StarRating value={5} disabled />
          </div>
        </div>
        <div className="extra-description">
          <Typography component="span" variant="body3">
            {review.categories}
          </Typography>
          <div className="job-success">
            <Typography
              component="span"
              variant="sub-heading3"
              className="percentage"
            >
              {review.jobSuccess}%
            </Typography>
            <Typography
              component="span"
              variant="sub-heading3"
              className="text"
            >
              Job Success
            </Typography>
          </div>
          <Typography component="span" variant="sub-heading3">
            {review.reviews}
          </Typography>
        </div>
      </div>
    </div>
  );
}
