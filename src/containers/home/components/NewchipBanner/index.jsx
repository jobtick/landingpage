import React from "react";
import Typography from "components/Typography";
import { Button, Container } from "reactstrap";
import Link from "next/link";
import classes from "./style.module.scss";

function NewchipBanner() {
  const [Close, setClose] = React.useState(false);
  React.useEffect(() => {
    const header = document.querySelector("header");
    if (Close) header.classList.remove("newchip");
    else header.classList.add("newchip");
  }, [Close]);
  return (
    <div
      className={[
        classes["newchip-banner"],
        Close ? classes["closed"] : "",
      ].join(" ")}
    >
      <Container>
        <Link href="/newchip-accelerator-program">
          <a>
            <Typography variant="sub-heading1" className="color">
              Jobtick Chosen for Newchip's Intensive Global Pre-Seed Accelerator
              Program!
            </Typography>
            <Typography variant="sub-heading1"> Read More</Typography>
          </a>
        </Link>
      </Container>
      <Button close onClick={setClose} className="btn-close" />
    </div>
  );
}
export default NewchipBanner;
