import Typography from "components/Typography";
import React from "react";
import Link from "next/link";
import { Button, Col, Container } from "reactstrap";
import classes from "./style.module.scss";
import Image from 'components/Image';

function WhyJobtick () {
  const [Items, setItems] = React.useState([]);
  const getItems = () => {
    setItems([
      {
        image: "/vector/home/security.svg",
        title: "Payment Security",
        description:
          "Jobtick has a secure payment system, which protects both our Posters and Tickers. All payments through Jobtick are encrypted and secured.",
      },
      {
        image: "/vector/home/support.svg",
        title: "24hr Support",
        description:
          "Our friendly customer service team is on hand 24/7 to assist the Jobtick community. Experts at what they do, our support team are ready to aid you if needed.",
      },
      {
        image: "/vector/home/flexible.svg",
        title: "Flexible Work Opportunities",
        description:
          "Jobtick allows users to have access to a large pool of flexible work opportunities matched with their locations and skills. ",
      },
      {
        image: "/vector/home/community.svg",
        title: "Community-based",
        description:
          "We have founded a strong sense of community on Jobtick. Our Posters and Tickers are constantly coming back - and this is because of our unique community. ",
      },
    ]);
  };
  React.useEffect(getItems, []);
  return (
    <div className={classes["why-jobtick"]}>
      <Container>
        <div className="header">
          <Typography component="h2" variant="heading3">
            Why Choose Jobtick?
          </Typography>
          <Typography component="p" variant="body1">
            We give you a complete solution. whether you want to get the job off
            the to do list or you want to tick the job for our posters, we got
            you covered.
          </Typography>
        </div>
        <div className="items">
          {Items.map((item, index) => (
            <Col lg={3} md={6} key={index}>
              <div className="circle">
                {/* <img src={item.image} /> */}
                <Image src={item.image} width="62" height="62" />
              </div>
              <div className="content">
                <Typography component="h3" variant="sub-heading1">
                  {item.title}
                </Typography>
                <Typography component="span" variant="body3">
                  {item.description}
                </Typography>
              </div>
            </Col>
          ))}
        </div>
        {/* <div className={classes.started}>
          <Typography component="h2" variant="heading3">
            Get started by
          </Typography>
          <br />
          <div className={classes.row}>
            <Link href="/post-job">
              <a>
                <Button
                  color="primary"
                  className={[
                    classes.btn,
                    classes.poster,
                    "jt-post-job-why-choose",
                  ].join(" ")}
                >
                  <Typography variant="heading4">Post a job</Typography>
                </Button>
              </a>
            </Link>
            <Link href="/explore-jobs">
              <a>
                <Button
                  color="primary"
                  className={classes.btn + " jt-explore-why-choose"}
                >
                  <Typography variant="heading4">Become a Ticker</Typography>
                </Button>
              </a>
            </Link>
          </div>
        </div> */}
      </Container>
    </div>
  );
}
export default WhyJobtick;
