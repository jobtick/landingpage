import Typography from "components/Typography";
import React from "react";
import Lottie from "react-lottie";
import { Button, Col, Row, Container } from "reactstrap";
import classes from "./style.module.scss";
import { AppStores } from "constants/info";
import Link from "next/link";
import Image from 'components/Image';
// import Image from "next/image";

function AppStore () {
  const yourPocketData = require("./appStore.json");
  return (
    <div className={classes["yourPocket"]}>
      <Container>
        <div className="yourPocket-content">
          <Typography variant="heading2" component="h3">
            {yourPocketData.title}
          </Typography>
          <Typography variant="body1" className="app-description">
            {yourPocketData.description}
          </Typography>
          <div className="stores">
            <a
              className="iosapp jt-iosapp-app"
              href={AppStores.apple}
              target="_blank"
            >
              <Image
                src={yourPocketData.apple}
                alt="Download app for IOS"
                width="200"
                height="57.46"
                loading="eager"
              />
            </a>
            <a
              className="androidapp jt-androidapp-app"
              href={AppStores.google}
              target="_blank"
            >
              <Image
                src={yourPocketData.google}
                alt="Download app for Android"
                width="200"
                height="57.46"
                loading="eager"
              />
            </a>
          </div>
        </div>
        <div className="yourPocket-image">
          {/* <img src={yourPocketData.image} className="yourPocket-image" /> */}
          <Image src={yourPocketData.image} className="yourPocket-image" width="608" height="346" />

        </div>
      </Container>
    </div>
  );
}

export default AppStore;
