import React from "react";
import Modal from "components/Modal";
import { getMobileOperatingSystem } from "utils";
import classes from "./style.module.scss";
export default function DiscountPopup({ Open, onClose }) {
  const [DiscountOpen, setDiscountOpen] = React.useState(false);
  const [Agent, setAgent] = React.useState();

  React.useEffect(() => {
    setAgent(getMobileOperatingSystem());
  }, []);

  // React.useEffect(() => {
  //   setTimeout(() => setDiscountOpen(true), 5000);
  // }, []);
  return (
    <Modal open={Open} onClose={onClose} className={classes.discount}>
      <img
        src={
          Agent === "Android" || Agent === "iOS"
            ? "/images/discount-mobile.png"
            : "/images/discount.png"
        }
      />
    </Modal>
  );
}
