import React from "react";
import Typography from "components/Typography";
import Marquee from "react-easy-marquee";
import classes from "./style.module.scss";
export default function JobPool() {
  const jobPoolData = require("./jobPool.json");
  const mid = jobPoolData.jobs.length / 2;
  return (
    <div className={classes["job-pool"]}>
      <Typography component="h3" variant="heading3">
        {jobPoolData.title}
      </Typography>
      {[jobPoolData.jobs.slice(0, mid), jobPoolData.jobs.slice(mid)].map(
        (jobsSlice, sliceIndex) => (
          <Marquee
            key={sliceIndex}
            duration={120000}
            height="147px"
            width="100%"
            axis="X"
            align="center"
            pauseOnHover={true}
            reverse={sliceIndex % 2 === 0}
            className="job-wrapper"
          >
            {jobsSlice.map((job, index) => (
              <div key={index} className="job-item">
                <div
                  style={{
                    backgroundImage: `url(${jobPoolData?.background})`,
                    width: "94px",
                    height: "94px",
                  }}
                  className={[
                    "avatar-wrapper",
                    "avatar-" + job.tickers?.length,
                  ].join(" ")}
                >
                  <img src={job.posterImg} className="poster-img" />
                  {job.tickers.map((ticker, index) => (
                    <img
                      src={ticker.image}
                      key={index}
                      className={"ticker-" + (index + 1)}
                    />
                  ))}
                </div>

                <div className="job-info">
                  <Typography variant="sub-heading1" className="job-title">
                    {job.title}
                  </Typography>
                  <div className="job-details">
                    <Typography
                      variant="sub-heading2"
                      className={[
                        "status",
                        "status-" + job.status.toLowerCase(),
                      ].join(" ")}
                    >
                      {job.status}
                    </Typography>
                    <Typography variant="heading3" className="job-price">
                      ${job.price}
                    </Typography>
                  </div>
                </div>
              </div>
            ))}
          </Marquee>
        )
      )}
    </div>
  );
}
