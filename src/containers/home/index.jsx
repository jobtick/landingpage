import React from "react";
import HeaderTags from "components/HeaderTags";
import { BASE_URL } from "constants/config";
import Hero from "./components/Hero";
import JobPool from "./components/JobPool";
import PostJob from "./components/PostJob";
import HowJobtickWork from "./components/HowJobtickWork";
import AppStore from "./components/AppStore";
import JobtickSendEmail from "./components/JobtickSendEmail";
import WhyJobtick from "./components/WhyJobtick";
import Testimonials from "./components/Testimonials";
import FAQ from "./components/FAQ";
import Blog from "./components/Blog";
// import DiscountPopup from "./components/DiscountPopup";
import exitIntent from "exit-intent-mobile-bugfix-asc";
import classes from "./style.module.scss";
function Home({ categories, blogPosts }) {
  React.useEffect(() => {
    document.body.classList.add(classes["home-page"]);
    return () => {
      document.body.classList.remove(classes["home-page"]);
    };
  }, []);

  // const [Open, setOpen] = React.useState(false);
  // React.useEffect(() => {
  //   const removeExitIntent = exitIntent({
  //     threshold: 200,
  //     maxDisplays: 2, // default 99999
  //     eventThrottle: 100, // default 200
  //     showAfterInactiveSecondsDesktop: 60, // default 60
  //     showAfterInactiveSecondsMobile: 40, // default 40
  //     showAgainAfterSeconds: 60,
  //     scrollOffset: 200,
  //     onExitIntent: () => {
  //       setOpen(true);
  //     },
  //     debug: false,
  //   });
  //   return () => removeExitIntent();
  // }, []);
  return (
    <>
      <HeaderTags
        title="Find home & office services or earn more - Jobtick"
        description="Visit jobtick.com for a handyman, cleaning, removals, delivery, computer & tech services and anything else in Australia"
        image={BASE_URL.substr(0, BASE_URL.length - 1) + "/logo192.png"}
        url={"/"}
        schema={{
          "@context": "https://schema.org",
          "@type": "LocalBusiness",
          name: "Jobtick",
          image: "https://www.jobtick.com/images/logo.png",
          "@id": "https://www.jobtick.com/",
          url: "https://www.jobtick.com/contact/",
          telephone: "",
          address: {
            "@type": "PostalAddress",
            streetAddress: "",
            addressLocality: "",
            addressRegion: "NSW",
            postalCode: "",
            addressCountry: "AU",
          },
          openingHoursSpecification: {
            "@type": "OpeningHoursSpecification",
            dayOfWeek: [
              "Monday",
              "Tuesday",
              "Wednesday",
              "Thursday",
              "Friday",
              "Saturday",
              "Sunday",
            ],
            opens: "00:00",
            closes: "23:59",
          },
          sameAs: [
            "https://www.facebook.com/jobtick",
            "https://twitter.com/jobtick",
            "https://www.instagram.com/jobtick.au/",
            "https://www.pinterest.com.au/jobtick/",
            "https://www.linkedin.com/company/jobtick/",
            "https://www.jobtick.com/",
            "https://www.youtube.com/channel/UCRXoJTnMT-4gGtr_BkElWRw",
          ],
        }}
      />

      <Hero />
      {/* <JobPool /> */}
      <PostJob />
      <HowJobtickWork />
      <AppStore />
      <WhyJobtick />
      <JobtickSendEmail />
      <Testimonials />
      <FAQ />
      <Blog posts={blogPosts} />
      {/* <DiscountPopup Open={Open} onClose={() => setOpen(false)} /> */}
    </>
  );
}

export default Home;
