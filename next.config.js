const withReactSvg = require("next-react-svg");
const path = require("path");

const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
});

const config = {
  include: path.resolve(__dirname, "src/assets/vector"),
  trailingSlash: true,
  images: {
    domains: [
      "api-dev.jobtick.com",
      "api.jobtick.com",
      "jobtick-ap.s3.ap-southeast-2.amazonaws.com",
      "www.jobtick.com",
      "jobtick.com",
    ],
  },
  webpack(config, options) {
    return config;
  },
};

module.exports = withBundleAnalyzer(withReactSvg(config));
